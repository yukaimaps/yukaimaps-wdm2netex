# WDM2Netex

This is the tool that exports WDM data to NeTEx.

## Usage

### Install

Dependencies:

- Python >= 3.7

Launch following commands for running the converter:

```bash
# Retrieve submodules (Netex schema)
git submodule init
git submodule update

# Init Python environment
python -m venv ./env
source ./env/bin/activate
pip install -r requirements.txt
```

### Running


### Testing

```bash
pytest
```

## Useful links

* [WDM](https://gitlab.com/yukaimaps/yukaidi-tagging-schema/-/blob/main/doc/Walk_data_model.md): documentation of the Walk Data Model (WDM) used here
* [NeTEx XML schema](https://github.com/NeTEx-CEN/NeTEx)
