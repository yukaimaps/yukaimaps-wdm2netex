Export WDM vers Netex
=====================

Le document suivant décrit comment les données du projet Yukaimaps, au format [WDM](https://gitlab.com/yukaimaps/yukaidi-tagging-schema/-/blob/main/doc/Walk_data_model.md), sont converties en export Netex.

Les exports sont conformes au profil France documenté ici :
* https://normes.transport.data.gouv.fr/posts/accessibilit%C3%A9/
* et plus généralement ici https://normes.transport.data.gouv.fr/

**Table des matières :**

[[_TOC_]]

# Contenu de l'export

L'export est un archive au format `*.zip`.

Il contient un unique fichier `accessibilite.xml`.

# En-tête

Chaque fichier xml commence par un élément PublicationDelivery.

### PublicationDelivery/@version

PublicationDelivery/@version est de la forme **x.y:FR-NETEX_nnnn-a.b-c** avec :
  * `x.y`:  la version du XSD Netex : fixé à 1.09
  * `nnnn`: le profil : fixé à "ACCESSIBILITE"
  * `a.b`: la version du profil : fixé à 2.1 pour le profil ACCESSIBILITE,
  * `c` la version de l'implémentation courante : fixé à 0.0 dans la version courante

Les autres valeurs sont fixes:
```xml
<PublicationDelivery xmlns="http://www.netex.org.uk/netex" xmlns:core="http://www.govtalk.gov.uk/core" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:ifopt="http://www.ifopt.org.uk/ifopt" xmlns:siri="http://www.siri.org.uk/siri" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="x.y:FR-NETEX_nnnn-a.b-c" xsi:schemaLocation="http://www.netex.org.uk/netex">
    (...)
</PublicationDelivery>
```

### PublicationDelivery/PublicationTimestamp

PublicationDelivery/PublicationTimestamp est renseigné avec les date et heure de génération du fichier, au format ISO8601.

### PublicationDelivery/ParticipantRef

PublicationDelivery/ParticipantRef est renseigné avec une valeur fixe : "YukaimapsWDM2Netex".

### PublicationDelivery/Description

Si au moins un objet exporté comporte un tag WDM Ref:Import:Source=OpenStreetMap, PublicationDelivery/Description est renseigné avec une valeur fixe : "Export Yukaimaps des données d'accessibilité et de cheminement - Cet export contient des données issues d'OpenStreetMap (© contributeurs OpenStreetMap).".

Sinon, PublicationDelivery/Description est renseigné avec une valeur fixe : "Export Yukaimaps des données d'accessibilité et de cheminement".

### PublicationDelivery/dataObjects[]/GeneralFrame

Dans dataObjects, on retrouve un unique GeneralFrame:
* PublicationDelivery/dataObjects[]/GeneralFrame/`@version` : valeur fixe "any"
* PublicationDelivery/dataObjects[]/GeneralFrame/`@id` : valeur fixe "FR:GeneralFrame:NETEX_ACCESSIBILITE:"

### PublicationDelivery/dataObjects[]/GeneralFrame/FrameDefaults

contient `<DefaultLocationSystem>EPSG:4326</DefaultLocationSystem>`

### PublicationDelivery/dataObjects[]/GeneralFrame/members[]

Contient la liste des objets, dans l'ordre suivant :

* liste des objets Quay
* liste des objets StopPlace
* liste des objets PointOfInterest
* liste des objets Entrance
* liste des objets SitePathLink
* liste des équipements
* liste des objets AccessSpace
* liste des objets PointOfInterestSpace
* liste des objets ParkingBay
* liste des objets PathJunction
* liste des objets DataSource

# Quay

Un objet Quay Netex est créé pour chaque object avec Quay=*. Il peut s'agir de :
* ![node] Quay=* (ponctuel)
* ![area] InsideSpace=Quay et Quay=* (polygonal)

On utilise la règle de gestion suivante :
* Quay/`@version` : version du ![way] chemin ou de la ![area] zone
* Quay/`@id` : de la forme Yukaimaps:Quay:wAA:LOC ou Yukaimaps:Quay:nAA:LOC, avec AA l'identifiant du ![node] noeud ou de la ![area] zone

**Cas particuliers**
Si l'objet WDM a un tag Ref:Import:Source=Netex :
* Quay/`@id` est rempli avec la valeur du tag Ref:Import:Id
* Quay/`@version` : de la forme AA+BB avec AA la valeur du tag Ref:Import:Version et BB la version du ![node] noeud ou de la ![area] zone. Si Ref:Import:Version n'est pas renseigné, Quay/`@version` vaut la valeur fixe "any"

Si l'objet WDM a un tag Ref:Export:Id :
* Quay/`@id` est rempli avec la valeur du tag Ref:Export:Id
* Quay/`@version` : de la forme +BB avec BB la version du ![node] noeud ou de la ![area] zone.

### Quay/keyList[]

Quay/keyList[] est renseigné en suivant les règles communes :
- typeOfKey=WDM_tag pour les éventuels tags non convertis
- typeOfKey=WDM_id si l'identifiant de l'objet n'a pas été utilisé pour la création de Quay/`@id`

### Quay/Name

Quay/Name est rempli avec la valeur du tag Name.

### Quay/Description

Quay/Description est rempli avec la valeur du tag Description.

### Quay/Centroid/Location

L’élément Quay/Centroid n'est présent que pour les ![node] objets ponctuels.

Si le tag NonGeographicalLocation=Yes est présent, l’élément Quay/Centroid est absent.

Sinon, la géométrie du ![node] noeud est exportée en suivant les règles communes.

### Quay/gml:Polygon

L’élément Quay/gml:Polygon n'est présent que pour les ![area] InsideSpace=Quay (polygonaux).

Si le tag NonGeographicalLocation=Yes est présent, l’élément Quay/gml:Polygon est absent.

Sinon, la géométrie de la ![area] zone est exportée en suivant les règles communes.

L’identifiant de l'InsideSpace=Quay est repris dans gml:Polygon/`@gml:id`.

### Quay/AccessibilityAssessment

Quay/AccessibilityAssessment est rempli en suivant les règles communes.

Les limitations/AccessibilityLimitation présentes sont : 
* WheelchairAccess
* AudibleSignalsAvailable
* VisualSignsAvailable

### Quay/Covered

Quay/Covered est renseigné à partir du tag Outdoor :
* outdoors si Outdoor=Yes
* indoors si Outdoor=No
* covered si Outdoor=Covered
* non renseigné sinon

### Quay/Gated

Quay/Gated est renseigné à partir du tag Gated :

* gatedArea si Gated=Yes
* openArea si Gated=No
* non renseigné sinon

### Quay/Lighting

Quay/Lighting est renseigné à partir du tag Lighting :
* wellLit si Lighting=Yes
* unlit si Lighting=No
* poorlyLit si Lighting=Bad
* non renseigné sinon

### Quay/SiteRef

Quay/SiteRef référence l’objet Netex StopPlace qui comprend le quai.
La référence est de la forme `<SiteRef ref="Yukaimaps:StopPlace:w12:LOC" version="5"/>` avec:
* Quay/SiteRef/@ref : l’identifiant de l’objet Netex
* Quay/SiteRef/@version : la version de l’objet

L'objet englobant peut être :
- une ![area] zone PointOfInterest=StopPlace de même mode que le quai
- une ![relation] relation StopPlace=* de même mode que le quai
- un objet construit à l'export (voir paragraphe StopPlace)

### Quay/equipmentPlaces[]

Quay/equipmentPlaces[] contient une liste de références vers les équipements du quai ainsi que leurs positions respectives, en suivant les règles de gestion communes.

Les équipements concernés sont :

* ceux créés à partir des objets Amenity=* situés à l'intérieur de la ![area] zone du Quay, dans le cas d'un quai polygonal
* générés à partir des attributs Shelter, Seating, RubbishDisposal, TicketValidator et TicketVendingMachine du quai ponctuel

### Quay/TransportMode

Quay/TransportMode est rempli avec la valeur du tag Quay en camelCase.

Valeurs autorisées:
 <br> - bus <br> - coach  <br> - water  <br> - funicular <br> - trolleyBus <br> - cableway <br> - rail <br> - metro <br> - tram

Si la valeur finale n’est pas dans la liste ci-dessus, Quay/TransportMode prend la valeur “bus”.

Si le tag Quay n’est pas renseigné, Quay/TransportMode prend également la valeur “bus”.

### Quay/PublicCode

Quay/PublicCode est rempli avec la valeur du tag PublicCode.


# StopPlace

On crée les objets Netex StopPlace à partir les objets StopPlace=* et en analysant les quais associés : on crée autant d'objet Netex StopPlace qu'il y a de modes (tag Quay) de quais.
Dans le cas où l'objet WDM StopPlace contient plusieurs quais de modes différents, en complément des objets Netex StopPlace pour chaque mode, on crée un objet StopPlace multimodal englobant ces objets Netex StopPlace de chaque mode (cas du *StopPlace dédupliqué*)

Les objets WDM d'origine peuvent être :
* un ![node] noeud ou une ![area] zone PointOfInterest=StopPlace et StopPlace=*.
* une ![relation] relation StopPlace=*

Remarque : pour les PointOfInterest=StopPlace, un objet Netex StopPlace est créé en complément de l'objet Netex PointOfInterest.

On utilise la règle de gestion suivante :
* StopPlace/`@version` : version de la ![area] zone ou de la ![relation] relation
* StopPlace/`@id` : de la forme Yukaimaps:StopPlace_TT:wAA:LOC, avec AA l'identifiant de la ![area] zone et TT le mode ou bien Yukaimaps:StopPlace_TT:rAA:LOC, avec AA l'identifiant de la ![relation] relation et TT le mode

**Cas particulier**
Si l'objet WDM a un tag Ref:Import:Source=Netex :
* StopPlace/`@id` est rempli avec la valeur du tag Ref:Import:Id
* StopPlace/`@version` : de la forme AA+BB avec AA la valeur du tag Ref:Import:Version et BB la version de la ![area] zone ou de la ![relation] relation. Si Ref:Import:Version n'est pas renseigné, StopPlace/`@version` vaut la valeur fixe "any"

Si l'objet WDM a un tag Ref:Export:Id :
* Quay/`@id` est rempli avec la valeur du tag Ref:Export:Id
* Quay/`@version` : de la forme +BB avec BB la version du ![node] noeud ou de la ![area] zone.

Dans le cas du StopPlace dédupliqué, le StopPlace multimodal aura les attributs suivants :
* StopPlace/`@version` : version de la ![area] zone
* StopPlace/`@id` : de la forme Yukaimaps:StopPlace:wAA:LOC, avec AA l'identifiant de la ![area] zone 

En complément, pour chaque objet Netex Quay non englobé dans un objet Netex StopPlace et de même nom (tag Name) et mode (tag Quay), on génère un nouvel objet Netex StopPlace (cas du *StopPlace généré*) avec la règle de gestion suivante :
* StopPlace/`@id` : de la forme Yukaimaps:StopPlace:g_wAA_TT:LOC avec AA le plus petit des identifiants des quais et TT le mode
* StopPlace/`@version` : valeur fixe "any"

### StopPlace/keyList[]

StopPlace/keyList[] est renseigné en suivant les règles communes :
- typeOfKey=WDM_tag pour les éventuels tags non convertis
- typeOfKey=WDM_id si l'identifiant de l'objet n'a pas été utilisé pour la création de StopPlace/`@id`

### StopPlace/Name

StopPlace/Name est rempli avec la valeur du tag Name.

Dans le cas d'un StopPlace généré, il prendra la valeur du tag Name des quais.

### StopPlace/Description

StopPlace/Description est rempli avec la valeur du tag Description.

### StopPlace/Centroid/Location

L’élément StopPlace/Centroid n'est présent que pour les ![node] PointOfInterest=StopPlace (géométrie ponctuelle).

Si le tag NonGeographicalLocation=Yes est présent, l’élément StopPlace/Centroid est absent.

Sinon, la géométrie du barycentre du ![node] noeud est exportée en suivant les règles communes.

### StopPlace/gml:Polygon

L’élément StopPlace/gml:Polygon n’est présent que pour les ![area] zones PointOfInterest=StopPlace.

Si le tag NonGeographicalLocation=Yes est présent, l’élément StopPlace/gml:Polygon est absent.

Sinon, la géométrie de la ![area] zone est exportée en suivant les règles communes.

L’identifiant du PointOfInterest=StopPlace préfixé de "_1" est repris dans gml:Polygon/`@gml:id`.

### StopPlace/placeTypes

L’élément StopPlace/placeTypes contient un unique élément de la forme 
* `<TypeOfPlaceRef ref="monomodalStopPlace"/>` dans le cas général, pour tous les objets StopPlace contenant des quais de même mode
* `<TypeOfPlaceRef ref="multimodalStopPlace"/>` dans le cas du StopPlace dédupliqué, pour le StopPlace englobant les objets StopPlace de même mode

### StopPlace/AccessibilityAssessment

Les limitations/AccessibilityLimitation présentes sont :

* WheelchairAccess
* AudibleSignalsAvailable
* VisualSignsAvailable
* StepFreeAccess
* EscalatorFreeAccess
* LiftFreeAccess


#### StopPlace généré

Dans le cas d’un StopPlace généré, StopPlace/AccessibilityAssessment est rempli en suivant les règles communes, à partir des tags présents sur les quais du StopPlace.

Les règles à appliquer pour le tag WheelchairAccess sont les suivantes :

* false si au moins un quai a WheelchairAccess=No
* unknown si WheelchairAccess est manquant sur certains quais
* partial sinon, en ajoutant la phrase suivante au validityConditions/ValidityCondition/Description : "présence inconnue d'un cheminement pratiquable en fauteuil roulant entre les quais"

Les règles à appliquer pour les tags AudibleSignals et VisualSigns sont les suivantes :

* true si tous les tags des quais sont à Yes
* false si tous les tags des quais sont à No
* partial si les tags des quais n'ont pas la même valeur ou s'ils valent tous Limited
* unknown si certains tags sont manquants sur les quais

Si la valeur retenue est partial, on ajoutera une phrase au validityConditions/ValidityCondition/Description :

* Signalétique auditive dépendante du quai (pour AudibleSignals)
* Signalétique visuelle dépendante du quai (pour VisualSigns)

StepFreeAccess, EscalatorFreeAccess, LiftFreeAccess ont la valeur fixe "unknown".

#### StopPlace classique

Dans le cas d’un StopPlace classique (objet avec StopPlace=*), StopPlace/AccessibilityAssessment est rempli en suivant les règles communes, à partir des tags présents sur l'objet StopPlace, ou à défaut sur les quais du StopPlace.

**Cas particulier**: le tag AudioInformation est utilisé en priorité pour déterminer AudibleSignalsAvailable :
* true si AudioInformation=Yes
* false si AudioInformation=No
* partial si AudioInformation=Bad avec le message "Signalétique auditive non accessible malentendants" dans validityConditions/ValidityCondition/Description
* les règles du tag AudibleSignals si le tag est absent

Si un des tags WheelchairAccess, AudioInformation, AudibleSignals et VisualSigns est absent, la valeur est déterminée à partir des quais du StopPlace, en suivant les mêmes règles que pour le StopPlace généré.

StepFreeAccess, EscalatorFreeAccess, LiftFreeAccess sont renseignés à partir de la valeur des tags StepFreeAccess, EscalatorFreeAccess, LiftFreeAccess avec les règles suivantes :

* false si le tag vaut No
* true si le tag vaut Yes
* partial si le tag vaut Limited
* unknown dans tous les autres cas

#### StopPlace dédupliqués

Dans le cas des StopPlace dédupliqués, StopPlace/AccessibilityAssessment est rempli de manière identique sur les différents StopPlace, en suivant les règles communes, à partir des tags présents sur l'objet StopPlace.

**Cas particulier**: le tag AudioInformation est utilisé en priorité pour déterminer AudibleSignalsAvailable :
* true si AudioInformation=Yes
* false si AudioInformation=No
* partial si AudioInformation=Bad avec le message "Signalétique auditive non accessible malentendants" dans validityConditions/ValidityCondition/Description
* les règles du tag AudibleSignals si le tag est absent

StepFreeAccess, EscalatorFreeAccess, LiftFreeAccess sont renseignés à partir de la valeur des tags StepFreeAccess, EscalatorFreeAccess, LiftFreeAccess avec les règles suivantes :

* false si le tag vaut No
* true si le tag vaut Yes
* partial si le tag vaut Limited
* unknown dans tous les autres cas

### StopPlace/Lighting

StopPlace/Lighting est renseigné à partir du tag Lighting :
* wellLit si Lighting=Yes
* unlit si Lighting=No
* poorlyLit si Lighting=Bad
* non renseigné sinon

### StopPlace/facilities

StopPlace/facilities/SiteFacilitySet est rempli en suivant les règles communes.

Les éléments enfants présents sont :
* AccessibilityInfoFacilityList
* AssistanceFacilityList
* AccessibilityToolList
* FamilyFacilityList
* MedicalFacilityList
* MobilityFacilityList
* PassengerCommsFacilityList
* PassengerInformationEquipmentList
* SanitaryFacilityList
* TicketingFacilityList
* AccessFacilityList
* EmergencyServiceList
* LuggageLockerFacilityList
* LuggageServiceFacilityList
* ParkingFacilityList
* Staffing

### StopPlace/ParentSiteRef

StopPlace/ParentSiteRef est renseigné uniquement dans le cas du StopPlace dédupliqué, sur chacun des objets StopPlace mono-modaux ainsi générés.

StopPlace/ParentSiteRef référence l’objet Netex StopPlace multi-modal généré. La référence est de la forme <ParentSiteRef ref="Yukaimaps:StopPlace:w12:LOC" version="2"/> avec:
* Quay/SiteRef/@ref : l’identifiant de l’objet Netex
* Quay/SiteRef/@version : la version de l’objet

### StopPlace/entrances[]

StopPlace/entrances[] contient une liste de références vers les entrées de l'objet StopPlace, de la forme `<EntranceRef version="2" ref="Yukaimaps:Entrance:n546889:LOC"/>`.

Dans le cas du StopPlace dédupliqué, les entrées sont uniquement référencées sur le StopPlace multi-modal.

### StopPlace/equipmentPlaces[]

StopPlace/equipmentPlaces[] contient une liste de références vers les équipements du StopPlace ainsi que leurs positions respectives, en suivant les règles de gestion communes.

Les équipements concernés sont ceux créés à partir des objets Amenity=*.

Dans le cas du StopPlace dédupliqué, les équipements sont uniquement référencées sur le StopPlace multi-modal.

### StopPlace/placeEquipments[]

StopPlace/placeEquipments[] contient une liste de références vers les équipements du StopPlace, de la forme `<StaircaseEquipmentRef ref="Yukaimaps:StaircaseEquipment:w4521_n2_n502:LOC" version="2"/>`.

Les équipements concernés sont ceux créés à partir de SitePathLink=Ramp/Stairs/Escalator/Travelator/Elevator/Crossing/QueueManagement.

Dans le cas du StopPlace dédupliqué, les équipements sont uniquement référencées sur le StopPlace multi-modal.

### StopPlace/TransportMode

StopPlace/TransportMode est rempli avec la valeur du tag StopPlace.

| valeur du tag StopPlace | StopPlace/TransportMode |
| ----------------------- | ----------------------- |
| RailStation             | rail                    |
| MetroStation            | metro                   |
| BusStation              | bus                     |
| CoachStation            | coach                   |
| TramStation             | tram                    |
| FerryPort               | water                   |
| LiftStation             | cableway                |
| Airport                 | air                     |
| OnstreetBus             | bus                     |
| OnstreetTram            | tram                    |
| autre valeur            | bus                     |

Dans le cas d'un StopPlace généré, StopPlace/TransportMode est construit à partir de Quay/TransportMode des quais.

Dans le cas du StopPlace dédupliqué, pour les StopPlace mono-modaux StopPlace/TransportMode est construit à partir de Quay/TransportMode des quais. Pour le StopPlace multi-modal, on prend le premier mode présent sur les quais, en respectant l'ordre suivant :
* air
* water
* rail
* metro
* tram
* cableway
* bus
* coach
* trolleyBus


### StopPlace/OtherTransportModes

StopPlace/OtherTransportModes est renseigné uniquement dans le cas du StopPlace dédupliqué, sur le StopPlace multimodal.

Il comprend la liste des autres modes présents sur les quais (construit à partir de Quay/TransportMode)

### StopPlace/StopPlaceType

StopPlace/StopPlaceType est rempli avec la valeur du tag StopPlace, en camelCase.

Valeurs autorisées :
<br> - onstreetBus <br> - busStation <br> - coachStation <br> - onstreetTram <br> - tramStation <br> - railStation <br> - metroStation <br> - airport <br> - ferryPort <br> - liftStation

Si la valeur finale n’est pas dans la liste ci-dessus, StopPlace/StopPlaceType prend la valeur "other".

Si le tag StopPlace n’est pas renseigné, StopPlace/StopPlaceType prend également la valeur "other".

Dans le cas du StopPlace généré et du StopPlace dédupliqué, on renseigne StopPlace/StopPlaceType à partir du StopPlace/TransportMode

| StopPlace/TransportMode | StopPlace/StopPlaceType |
| ----------------------- | ----------------------- |
| bus                     | onstreetBus             |
| tram                    | onstreetTram            |
| rail                    | railStation             |
| metro                   | metroStation            |
| coach                   | coachStation            |
| water                   | ferryPort               |
| cableway                | liftStation             |
| air                     | airport                 |
| autre valeur            | other                   |

### StopPlace/quays[]

StopPlace/quays[] contient une liste de références vers les quais que l'objet StopPlace englobe, de la forme `<QuayRef version="2" ref="Yukaimaps:Quay:w889:LOC"/>`.

Dans le cas du StopPlace dédupliqué, l'élément est absent pour le StopPlace multi-modal.

### StopPlace/accessSpaces[]

StopPlace/accessSpaces[] contient une liste de références vers les AccessSpaces que l'objet StopPlace englobe, de la forme `<AccessSpaceRef version="2" ref="Yukaimaps:AccessSpace:w889:LOC"/>`.

Dans le cas du StopPlace dédupliqué, les AccessSpaces sont uniquement référencées sur le StopPlace multi-modal.

### StopPlace/pathLinks[]

StopPlace/pathLinks[] contient une liste de références vers les SitePathLink que l'objet StopPlace englobe, de la forme `<PathLinkRef version="2" ref="Yukaimaps:SitePathLink:w4521_n12_n2:LOC"/>`.

Dans le cas du StopPlace dédupliqué, les objets SitePathLink sont uniquement référencées sur le StopPlace multi-modal.

# PointOfInterest

Un objet PointOfInterest Netex est créé pour chaque objet PointOfInterest=* avec la règle de gestion suivante :
* PointOfInterest/`@version` : version de la ![area] zone
* PointOfInterest/`@id` : de la forme Yukaimaps:PointOfInterest:wAA:LOC, avec AA l'identifiant de la ![area] zone

Remarque : pour les PointOfInterest=StopPlace, un objet Netex PointOfInterest est créé en complément de l’objet Netex StopPlace.

### PointOfInterest/keyList[]

PointOfInterest/keyList[] est renseigné en suivant les règles communes. Seul des typeOfKey=WDM_tag peuvent être présents.

### PointOfInterest/Name

PointOfInterest/Name est rempli avec la valeur du tag Name.

### PointOfInterest/Description

PointOfInterest/Description est rempli avec la valeur du tag Description.

### PointOfInterest/Centroid/Location

L’élément PointOfInterest/Centroid n'est présent que pour les ![node] PointOfInterest=* (géométrie ponctuelle).

Si le tag NonGeographicalLocation=Yes est présent, l’élément PointOfInterest/Centroid est absent.

Sinon, la géométrie du barycentre du ![node] noeud est exportée en suivant les règles communes.

### PointOfInterest/gml:Polygon

Si le tag NonGeographicalLocation=Yes est présent, l’élément PointOfInterest/gml:Polygon est absent.

Sinon, la géométrie de la ![area] zone est exportée en suivant les règles communes.

L’identifiant du PointOfInterest=* est repris dans gml:Polygon/`@gml:id`.

### PointOfInterest/Url

PointOfInterest/Url est rempli avec la valeur du tag Website.

### PointOfInterest/Image

PointOfInterest/Image est rempli avec la valeur du tag Image.

### PointOfInterest/PostalAddress

PointOfInterest/PostalAddress est de la forme :
* PointOfInterest/PostalAddress/`@version` : valeur fixe "any"
* PointOfInterest/PostalAddress/`@id` : de la forme Yukaimaps:PostalAddress:AA, avec AA un identifiant auto-incrémenté sur tout l’export

Puis, l'élément peut contenir les éléments suivants:
* PointOfInterest/PostalAddress/AddressLine1, qui est rempli avec la valeur du tag AddressLine
* PointOfInterest/PostalAddress/PostCode, qui est rempli avec la valeur du tag PostCode

Si les tags PostCode et AddressLine sont tous les deux absents, l'élément PointOfInterest/PostalAddress est absent.

### PointOfInterest/AccessibilityAssessment

PointOfInterest/AccessibilityAssessment est rempli en suivant les règles communes.

Les limitations/AccessibilityLimitation présentes sont :
* WheelchairAccess
* AudibleSignalsAvailable
* VisualSignsAvailable

**Cas particulier** : si PointOfInterest=StopPlace, les règles utilisées pour créer l'élément Netex StopPlace/AccessibilityAssessment correspondant sont réutilisées ici.

### PointOfInterest/Covered

PointOfInterest/Covered est renseigné à partir du tag Outdoor :
* outdoors si Outdoor=Yes
* indoors si Outdoor=No
* covered si Outdoor=Covered
* non renseigné sinon

### PointOfInterest/Gated

PointOfInterest/Gated est renseigné à partir du tag Gated :

* gatedArea si Gated=Yes
* openArea si Gated=No
* non renseigné sinon


### PointOfInterest/Lighting

PointOfInterest/Lighting est renseigné à partir du tag Lighting :
* wellLit si Lighting=Yes
* unlit si Lighting=No
* poorlyLit si Lighting=Bad
* non renseigné sinon

### PointOfInterest/facilities

PointOfInterest/facilities/SiteFacilitySet est rempli en suivant les règles communes.

Les éléments enfants potentiellement présents sont :
* AssistanceFacilityList
* AccessibilityToolList
* FamilyFacilityList
* MedicalFacilityList
* MobilityFacilityList
* PassengerCommsFacilityList
* PassengerInformationEquipmentList
* SanitaryFacilityList
* TicketingFacilityList
* AccessFacilityList
* EmergencyServiceList
* LuggageLockerFacilityList
* LuggageServiceFacilityList
* ParkingFacilityList
* Staffing

### PointOfInterest/entrances[]

PointOfInterest/entrances[] contient une liste de références vers les entrées de l'objet PointOfInterest, de la forme `<EntranceRef version="2" ref="Yukaimaps:Entrance:n546889:LOC"/>`.

### PointOfInterest/equipmentPlaces[]

PointOfInterest/equipmentPlaces[] contient une liste de références vers les équipements du PointOfInterest ainsi que leurs positions respectives, en suivant les règles de gestion communes.

Les équipements concernés sont ceux créés à partir des objets Amenity=* et situés à l'intérieur de la ![area] zone du PointOfInterest.

### PointOfInterest/placeEquipments[]

PointOfInterest/placeEquipments[] contient une liste de références vers les équipements du PointOfInterest, de la forme `<StaircaseEquipmentRef ref="Yukaimaps:StaircaseEquipment:w4521_n2_n502:LOC" version="2"/>`.

Les équipements concernés sont ceux créés à partir de SitePathLink=Ramp/Stairs/Escalator/Travelator/Elevator/Crossing/QueueManagement et situés à l'intérieur de la ![area] zone du PointOfInterest.

### PointOfInterest/classifications[]

(...)

### PointOfInterest/spaces[]

PointOfInterest/spaces[] contient une liste de références vers les PointOfInterestSpaces que l'objet PointOfInterest englobe, de la forme `<PointOfInterestSpaceRef version="2" ref="Yukaimaps:PointOfInterestSpace:w8189:LOC" />`.

### PointOfInterest/pathLinks[]

PointOfInterest/pathLinks[] contient une liste de références vers les SitePathLink que l'objet PointOfInterest englobe, de la forme `<PathLinkRef version="2" ref="Yukaimaps:SitePathLink:w4521_n12_n2:LOC"/>`.

# Entrance et PointOfInterestEntrance

Un objet Netex PointOfInterestEntrance est créé pour chaque ![node] avec Entrance=*.

En complément, un objet Netex Entrance est créé pour chaque ![node] avec Entrance=* situé dans un PointOfInterest=StopPlace.

On applique la règle de gestion suivante :
* Entrance/`@version` ou PointOfInterestEntrance/`@version` : version du ![node] noeud
* Entrance/`@id` ou PointOfInterestEntrance/`@id` : de la forme Yukaimaps:Entrance:nAA:LOC, avec AA l'identifiant du ![node] noeud

**Cas particulier**
Si l'objet WDM a un tag Ref:Import:Source=Netex :
* Entrance/`@id` est rempli avec la valeur du tag Ref:Import:Id
* Entrance/`@version` : de la forme AA+BB avec AA la valeur du tag Ref:Import:Version et BB la version du ![node] noeud. Si Ref:Import:Version n'est pas renseigné, Entrance/`@version` vaut la valeur fixe "any"

Remarque : un équipement Netex EntranceEquipment est créé en complément.

Les éléments suivants sont valables aussi bien pour les PointOfInterestEntrance que pour les Entrance.

### Entrance/keyList[]

Entrance/keyList[] est renseigné en suivant les règles communes :
- typeOfKey=WDM_tag pour les éventuels tags non convertis
- typeOfKey=WDM_id si l'identifiant de l'objet n'a pas été utilisé pour la création de Entrance/`@id`

### Entrance/Description

Entrance/Description est rempli avec la valeur du tag Description.

### Entrance/Centroid/Location

La géométrie du ![node] noeud est exportée dans Entrance/Centroid/Location en suivant les règles communes.

### Entrance/PostalAddress

Entrance/PostalAddress est rempli en suivant les même règles que PointOfInterest/PostalAddress.

### Entrance/AccessibilityAssessment

Entrance/AccessibilityAssessment est rempli en suivant les règles communes.

Les limitations/AccessibilityLimitation présentes sont :

* WheelchairAccess
* StepFreeAccess

WheelchairAccess est renseigné en suivant les règles communes à partir du tag WheelchairAccess.

Si le tag WheelchairAccess n'est pas renseigné, WheelchairAccess est rempli à l'aide des règles suivantes :

* false si Width < 0.8 m
* false si  NecessaryForceToOpen > 50 N
* false si KerbHeight > à 0,04 m
* true si Width ≥ 0.8 m, KerbHeight < 0.02 m et NecessaryForceToOpen ≤ 50 N
* true si Width ≥ 0.8 m, KerbHeight < 0.02 m et Door=None
* true si Width ≥ 0.8 m, KerbHeight < 0.02 m et EntrancePassage=Opening

Enfin, si aucune des conditions n'est remplie, WheelchairAccess vaudra "unknown".

StepFreeAccess est rempli à partir du tag KerbHeight et KerbCut:Width avec les règles suivantes :

* false si KerbHeight est strictement supérieur à 0,04
* true si KerbHeight est inférieur ou égal à 0,02
* partial si KerbHeight est compris entre 0,02 et 0,04, en ajoutant "Présence d'une marche de moins de 4 cm" à la liste des phrases utilisées pour constituer AccessibilityAssessment/validityConditions/ValidityCondition/Description
* partial si KerbHeight n'est pas renseigné et KerbCut:Width est un décimal positif, en ajoutant "Présence d'une abaissé de trottoir de taille inconnue" à la liste des phrases utilisées pour constituer AccessibilityAssessment/validityConditions/ValidityCondition/Description
* unknown sinon

### Entrance/SiteRef

Entrance/SiteRef référence l’objet Netex dont c'est une entrée.

La référence est de la forme `<SiteRef ref="Yukaimaps:StopPlace:w12:LOC" version="5"/>` avec:
* Entrance/SiteRef/@ref : l’identifiant de l’objet Netex
* Entrance/SiteRef/@version : la version de l’objet

Si l'entrée est située dans un PointOfInterest=StopPlace, l'objet Netex référencé sera un StopPlace.

Si l'entrée est située dans un PointOfInterest!=StopPlace, l'objet Netex référencé sera un PointOfInterest.

Sinon, l'élément est absent.

### Entrance/placeEquipments[] 

Entrance/placeEquipments contient une référence vers l'EntranceEquipment créé en complément de l'objet Entrance.

La référence est de la forme `<EntranceEquipmentRef ref="Yukaimaps:EntranceEquipment:n54267:LOC" version="2"/>`.

### Entrance/PublicCode

Entrance/PublicCode est rempli avec la valeur du tag PublicCode.

### Entrance/Label

Entrance/Label est rempli avec la valeur du tag Name.

### Entrance/EntranceType

Entrance/EntranceType est rempli à partir des tags EntrancePassage, Door et AutomaticDoor :

| conditions                                | Entrance/EntranceType |
| ----------------------------------------- | --------------------- |
| EntrancePassage=Opening                   | opening               |
| EntrancePassage=Gate/Barrier              | gate                  |
| EntrancePassage=Door et AutomaticDoor=Yes | automaticDoor         |
| EntrancePassage=Door et Door=Revolving    | revolvingDoor         |
| EntrancePassage=Door et Door=Swing        | swingDoor             |
| EntrancePassage=Door                      | door                  |
| autres conditions                         | élément absent        |

### Entrance/IsEntry

Entrance/IsEntry est rempli à partir de la valeur du tag IsEntry :
* true si IsEntry=Yes
* false si IsEntry=No
* élément absent si IsEntry a une autre valeur
* true si le tag n'est pas renseigné

### Entrance/IsExit

Entrance/IsExit est rempli  de la valeur du tag IsExit :
* true si IsExit=Yes
* false si IsExit=No
* élément absent si IsExit a une autre valeur
* true si le tag n'est pas renseigné

### Entrance/Width

Entrance/Width est rempli avec la valeur du tag Width, en mètres arrondis au centimètre.

### Entrance/Height

Entrance/Height est rempli avec la valeur du tag Height, en mètres arrondis au centimètre.

# SitePathLink

Les objets SitePathLink Netex sont contruits à partir des ![way] chemins qui ont SitePathLink=*.

Ces chemins sont préalablement découpés :

* à tous les ![node] noeuds d'intersections avec d'autres SitePathLink
* à tous les ![node] noeuds Entrance=*
* à tous les ![node] noeuds SitePathLink=Elevator

Si le chemin a SitePathLink=Stairs/Elevator/Escalator/Ramp ou s'il n'a pas de tag Level, ou s'il a un tag Level comportant une valeur unique (en utilisant le séparateur ";") on utilise la règle de gestion suivante :

* SitePathLink/`@version` : version du ![way] chemin
* SitePathLink/`@id` : de la forme Yukaimaps:SitePathLink:wAA_nBB_nCC:LOC, avec AA l'identifiant du ![way] chemin, BB l'identifiant du ![node] noeud de début et CC l'identifiant du ![node] noeud de fin

Si le chemin a plusieurs valeurs de tag Level et SitePathLink!=Stairs/Elevator/Escalator/Ramp, un SitePathLink Netex est créé pour chaque étage, avec la règle de gestion suivante :

* SitePathLink/`@version` : version du ![way] chemin
* SitePathLink/`@id` : de la forme Yukaimaps:SitePathLink:wAA_nBB_nCC_levelDD:LOC, avec AA l'identifiant du ![way] chemin, BB l'identifiant du ![node] noeud de début, CC l'identifiant du ![node] noeud de fin et DD l'identifiant du niveau

Les ![node] noeuds SitePathLink=Elevator sont également utilisés pour construire les SitePathLink Netex.<br>
On s'appuie alors en complément sur le tag Level, indiquant les étages desservis, séparés par des ";". Si le tag Level est absent, on considère que Level=0;1.<br>
Les étages desservis sont triés dans l'ordre croissant, et un SitePathLink Netex est créé pour chaque couple d'étage desservis, avec la règle de gestion suivante :

* SitePathLink/`@version` : version du ![node] noeud
* SitePathLink/`@id` :  de la forme Yukaimaps:SitePathLink:nAA_levelBB_levelCC:LOC, avec AA l'identifiant du ![node], BB l'identifiant du premier niveau et CC l'identifiant du deuxième niveau

**Cas particulier**
Si l'objet WDM a un tag Ref:Import:Source=Netex et que le chemin n'a pas été découpé :
* SitePathLink/`@id` est rempli avec la valeur du tag Ref:Import:Id
* SitePathLink/`@version` : de la forme AA+BB avec AA la valeur du tag Ref:Import:Version et BB la version du ![way] chemin. Si Ref:Import:Version n'est pas renseigné, SitePathLink/`@version` vaut la valeur fixe "any"

### SitePathLink/keyList[]

SitePathLink/keyList[] est renseigné en suivant les règles communes :
- typeOfKey=WDM_tag pour les éventuels tags non convertis
- typeOfKey=WDM_id si l'identifiant de l'objet n'a pas été utilisé pour la création de SitePathLink/`@id`

### SitePathLink/Name

On utilisera le tag Name.

Si le tag n'est pas renseigné, l'élément sera absent.

### SitePathLink/Distance

L'élément est rempli avec la distance calculée du SitePathLink, en mètres arrondis au cm.

Dans le cas d'un SitePathLink créé à partir d'un ![node] noeud SitePathLink=Elevator, Distance vaut 0.

### SitePathLink/gml:LineString

Si le tag NonGeographicalLocation=Yes est présent, l'élément est absent. Dans le cas d'un SitePathLink créé à partir d'un ![node] noeud SitePathLink=Elevator, l'élément est également absent.

Sinon, la géométrie du chemin SitePathLink est exportée sous la forme d'une linestring en WGS 84 dans gml:LineString/gml:pos[]. Elle est reconstituée à partir de la position des ![node] noeuds constituant le ![way] chemin, dans l'ordre.

La partie de l'identifiant du SitePathLink reprenant les identifiants des ![way] chemin et ![node] noeuds de d'origine et de fin est renseignée dans gml:LineString/`@gml:id`.

```xml
<gml:LineString gml:id="w3453_n345_n34">
    <gml:pos>2.068554 48.764788</gml:pos>
    <gml:pos>2.066760 48.763866</gml:pos>
</gml:LineString>
```

### SitePathLink/From/PlaceRef

SitePathLink/From/PlaceRef référence l'objet Netex qui représente le ![node] noeud de départ du ![way] chemin SitePathLink=*.

La référence est de la forme `<PlaceRef ref="Yukaimaps:PathJunction:n12:LOC" version="5"/>` avec:
* SitePathLink/From/PlaceRef/@ref : l'identifiant de l'objet Netex
* SitePathLink/From /PlaceRef/@version : la version de l'objet

Il peut s'agir d'objet Netex de type :
* Entrance (dans ce cas un SitePathLink/From/EntranceRef est également créé)
* ParkingBay
* équipement
* PathJunction

### SitePathLink/From/EntranceRef

SitePathLink/From/EntranceRef référence l'objet Netex qui représente le ![node] noeud de départ du ![way] chemin `SitePathLink=*`, uniquement lorsqu'il s'agit d'un objet `Entrance=*`. Sinon, SitePathLink/EntranceRef est absent.

La référence est de la forme `<EntranceRef version="1" ref="Yukaimaps:Entrance:n502:LOC" />` sur le même modèle que SitePathLink/From/PlaceRef

### SitePathLink/To/PlaceRef

SitePathLink/To/PlaceRef est construit sur le même modèle que SitePathLink/From/PlaceRef, avec le ![node] noeud de fin du ![way] chemin SitePathLink=*.

### SitePathLink/To/EntranceRef

SitePathLink/To/EntranceRef est construit sur le même modèle que SitePathLink/From/EntranceRef, avec le ![node] noeud de fin du ![way] chemin `SitePathLink=*` lorsqu'il s'agit un objet `Entrance=*`.

### SitePathLink/Description

On utilisera le tag Description.

Si le tag n'est pas renseigné, l'élément sera absent.

### SitePathLink/AccessibilityAssessment

SitePathLink/AccessibilityAssessment est rempli en suivant les règles communes.

Les limitations/AccessibilityLimitation présents sont :
* WheelchairAccess
* StepFreeAccess
* EscalatorFreeAccess
* LiftFreeAccess
* AudibleSignalsAvailable (dans certains cas)
* VisualSignsAvailable (dans certains cas)


**validityConditions/ValidityCondition/Description**

L’élément validityConditions est construit en suivant les règles communes.

En complément des valeurs des tags WheelchairAccess:Description, AudibleSignals:Description et VisualSigns:Description des phrases peuvent être ajoutées dans le cas où SitePathLink=Crossing (cf plus bas).

**WheelchairAccess**

Si SitePathLink=Stairs/Escalator/Travelator, WheelchairAccess vaudra "false".

Sinon, WheelchairAccess est renseigné en suivant les règles communes à partir du tag WheelchairAccess.

Si le tag WheelchairAccess n'est pas renseigné et que SitePathLink=Ramp, WheelchairAccess est rempli à l'aide des règles suivantes :

- false si Slope > 5%
- false si Width < 1,4m
- true si Slope ≤ 5% et Width ≥ 1,4 m

Si le tag WheelchairAccess n'est pas renseigné et que SitePathLink=Elevator, WheelchairAccess est rempli à l'aide des règles suivantes :

- false si Depth < 1,25 m
- false si AutomaticDoor = No
- false si Width < 0.8m
- true si AutomaticDoor!=No, Depth ≥ 1,25 m et Width ≥ 0.8 m

Si le tag WheelchairAccess n'est pas renseigné et que SitePathLink!=Ramp/Elevator, WheelchairAccess est rempli à l'aide des règles suivantes :

- false si Tilt > 5%
- false si Slope > 8%
- false si Width < 0.9 m
- false si FlooringMaterial = Gravel/Sand/Uneven
- false si Flooring = Hazardous
- false si le ![way] chemin SitePathLink=* comprend un ![node] Obstacle=\*
- false si le ![way] chemin SitePathLink=* comprend un ![node] PathJunction=Crossing avec KerbHeight > 0.04 m
- false si le ![way] chemin SitePathLink=* comprend un ![node] PathJunction=Crossing avec KerbDesign = Raised
- false si le ![way] chemin SitePathLink=* comprend un ![node] PathJunction=Crossing avec KerbDesign et KerbHeight non renseignés
- true si toutes les conditions suivantes sont remplies :
	- Tilt ≤ 2%
	- Slope ≤ 5%
	- Width ≥ 1,4 m
	- FlooringMaterial != Gravel/Sand/Uneven
	- Flooring != Hazardous/Discomfortable
	- le ![way] chemin SitePathLink=* ne comprend aucun ![node] Obstacle=\*
	- le ![way] chemin SitePathLink=* ne comprend aucun ![node] PathJunction=Crossing avec KerbHeight ≥ 0.02 m
- sinon, partial si toutes les conditions suivantes sont remplies :
	- Tilt ≤ 5%
	- Slope ≤ 8%
	- Width ≥ 0.9 m
	- FlooringMaterial != Gravel/Sand/Uneven
	- Flooring != Hazardous
	- le ![way] chemin SitePathLink=\* ne comprend aucun ![node] Obstacle=\*
	- tous les ![node] PathJunction=Crossing présents sur le ![way] chemin SitePathLink=\* ont KerbHeight ≤ 0.04 m

Enfin, si aucune des conditions n'est remplie, WheelchairAccess vaudra "unknown".

**StepFreeAccess**

Si SitePathLink=Stairs, StepFreeAccess vaudra "false".

Si SitePathLink=Escalator/Travelator/Elevator/Ramp, StepFreeAccess vaudra "true".

Sinon, si le ![way] chemin SitePathLink=\* comprend au moins un ![node] Obstacle=Kerb ou PathJunction=Crossing, StepFreeAccess est rempli à l'aide des règles suivantes :
- false si le ![way] chemin SitePathLink=\* comprend un ![node] Obstacle=Kerb
- false si le ![way] chemin SitePathLink=\* comprend un ![node] PathJunction=Crossing avec KerbHeight > 0.04 m
- false si le ![way] chemin SitePathLink=\* comprend un ![node] PathJunction=Crossing avec KerbDesign = Raised
- false si le ![way] chemin SitePathLink=\* comprend un ![node] PathJunction=Crossing avec KerbDesign et KerbHeight non renseignés
- true si tous les ![node] PathJunction=Crossing présents sur le ![way] chemin SitePathLink=\* ont KerbHeight ≤ 0.02 m
- sinon partial si tous les ![node] PathJunction=Crossing présents sur le ![way] chemin SitePathLink=\* ont KerbHeight compris entre 0.04 m et 0.02 m, en ajoutant "Présence de marches de moins de 4 cm" à la liste des phrases utilisées pour constituer AccessibilityAssessment/validityConditions/ValidityCondition/Description
- unknown sinon

Enfin, si aucune des conditions n'est remplie, StepFreeAccess vaudra "true".

**EscalatorFreeAccess**

Si SitePathLink=Escalator, EscalatorFreeAccess vaudra "false".

Sinon, EscalatorFreeAccess sera "true".

**LiftFreeAccess**

Si SitePathLink=Elevator, LiftFreeAccess vaudra "false".

Sinon, LiftFreeAccess sera "true".

**VisualSignsAvailable**

Si SitePathLink!=Hall/Corridor/Crossing/Elevator, l'élément est absent.

Si SitePathLink=Hall/Corridor, VisualSignsAvailable est renseigné en suivant les règles communes à partir du tag VisualSigns.

Si SitePathLink=Elevator, alors VisualSignsAvailable est construit à partir du tag VisualDisplays avec la règle de gestion suivante :

* true si VisualDisplays=Yes
* false si VisualDisplays=No
* unknown sinon

Si SitePathLink=Crossing, alors VisualSignsAvailable est construit à partir du tag ZebraCrossing avec la règle de gestion suivante :

* false si ZebraCrossing=None
* true si ZebraCrossing=Good/Worn
* partial si le tag ZebraCrossing=Discomfortable. Dans ce cas, on ajoute à la liste des phrases utilisées pour constituer AccessibilityAssessment/validityConditions/ValidityCondition/Description la phrase suivante : "Marquage au sol avec dégradation entraînant une difficulté d’usage ou d’inconfort"
* partial si le tag ZebraCrossing=Hazardous. Dans ce cas, on ajoute à la liste des phrases utilisées pour constituer AccessibilityAssessment/validityConditions/ValidityCondition/Description la phrase suivante : "Marquage au sol avec dégradation entraînant un problème de sécurité immédiat"
* unknown si le tag ZebraCrossing est présent avec une autre valeur, ou s'il est absent

Enfin, si aucune des conditions n'est remplie, VisualSignsAvailable vaudra "unknown".

**AudibleSignalsAvailable**

Si SitePathLink!=Hall/Corridor/Crossing/Elevator, l'élément est absent.

Si SitePathLink=Hall/Corridor, AudibleSignalsAvailable est renseigné en suivant les règles communes à partir du tag AudibleSignals.

Si SitePathLink=Elevator, alors AudibleSignalsAvailable est construit à partir du tag AudioAnnouncements avec la règle de gestion suivante :

* true si AudioAnnouncements=Yes
* false si AudioAnnouncements=No
* unknown sinon

Si SitePathLink=Crossing, alors AudibleSignalsAvailable est construit à partir du tag AcousticCrossingAids avec la règle de gestion suivante :

* false si AcousticCrossingAids = None
* true si AcousticCrossingAids=Good/Worn
* partial si AcousticCrossingAids=Discomfortable. Dans ce cas, on ajoute à la liste des phrases utilisées pour constituer AccessibilityAssessment/validityConditions/ValidityCondition/Description la phrase suivante : "Répétiteurs sonores avec dégradatation entrainant une difficulté d'usage ou un inconfort".
* partial si AcousticCrossingAids=Hazardous. Dans ce cas, on ajoute à la liste des phrases utilisées pour constituer AccessibilityAssessment/validityConditions/ValidityCondition/Description la phrase suivante : "Répétiteurs sonores avec dégradatation entrainant un problème de sécurité immédiat".
* unknown si le tag AcousticCrossingAids est présent avec une autre valeur, ou s'il est absent

### SitePathLink/PublicUse

On utilisera une valeur fixe “all” car tous les cheminements décrits sont accessibles à tous

### SitePathLink/Covered

| Tag Outdoor       | SitePathLink/Covered |
| ----------------- | -------------------- |
| "Yes"             | "outdoors"           |
| "No"              | "indoors"            |
| "Covered"         | "covered"            |
| autres conditions | élément absent       |

### SitePathLink/Gated

SitePathLink/Gated est renseigné à partir du tag Gated :

* gatedArea si Gated=Yes
* openArea si Gated=No
* non renseigné sinon

### SitePathLink/Lighting

| Tag Lighting      | SitePathLink/Lighting |
| ----------------- | --------------------- |
| "Yes"             | "wellLit"             |
| "No"              | "unlit"               |
| "Bad"             | "poorlyLit"           |
| autres conditions | élément absent        |

### SitePathLink/AllAreasWheelchairAccessible
| Tag WheelchairAccess | SitePathLink/AllAreasWheelchairAccessible |
| -------------------- | ------------------------------- |
| "Yes"                | "true"                          |
| "No"                 | "false"                         |
| autres conditions    | élément absent                  |

### SitePathLink/NumberOfSteps

Si SitePathLink=Stairs, on utilisera le tag StepCount:

| Tag StepCount     | SitePathLink/NumberOfSteps |
| ----------------- | -------------------------- |
| entier positif    | valeur de StepCount        |
| autres conditions | élément absent             |

Si le ![way] chemin `SitePathLink=*` comprend des ![node] noeuds `Obstacle=Kerb` (y compris sur ses noeuds de départ ou d'arrivée), StepCount correspond au nombre de ces ![node] noeuds. Si un noeud `Obstacle=Kerb` sert de jonction entre deux chemins `SitePathLink=*`, le ressaut est donc techniquement compté deux fois (une fois sur chaque SitePathLink relié).

Sinon, SitePathLink/NumberOfSteps vaudra 0.

### SitePathLink/MinimumWidth

Si le tag Width est absent, l'élément SitePathLink/MinimumWidth est absent.

Si le tag Width est présent mais n'est pas un nombre positif, l'élément SitePathLink/MinimumWidth est absent.

Si le ![way] chemin SitePathLink ne comprend pas de ![node] noeuds avec Obstacle=*, l'élément SitePathLink/MinimumWidth est rempli avec la valeur du tag Width en mètres, arrondi au centimètre.

Si le ![way] chemin SitePathLink comprend des ![node] noeuds avec Obstacle=* ayant des tags RemainingWidth (nombres positifs), l'élément SitePathLink/MinimumWidth prendra la plus petite valeur du tag RemainingWidth des différents ![node] noeuds obstacles traversés. À noter que les obstacles ayant les tags ObstacleType=Overhanging et Height >= 2.20 ne sont pas à prendre en compte dans ce calcul.

Cas particulier : si SitePathLink=Crossing, on prendra le minimum des valeurs suivantes :

* Width sur le ![way] chemin SitePathLink
* KerbCut:Width sur les ![node] noeuds aux extrémités du ![way] chemin

### SitePathLink/AllowedUse

Si SitePathLink=Escalator ou Travelator, on utilisera le tag Conveying:

| Tag Conveying           | SitePathLink/AllowedUse |
| ----------------------- | ----------------------- |
| "Reversible"            | "twoWay"                |
| "Forward" ou "Backward" | "oneWay"                |
| autres conditions       | élément absent          |

Sinon, SitePathLink/AllowedUse est absent.

### SitePathLink/Transition

On utilise le tag Incline si présent.

| Tag Incline       | SitePathLink/Transition |
| ----------------- | ----------------------- |
| "Up"              | "up"                    |
| "Down"            | "down"                  |
| "No"              | "level"                 |
| autres conditions | élément absent          |

Sinon, SitePathLink/Transition est absent.

### SitePathLink/Gradient

On utilisera la valeur du tag Slope après conversion des pourcentages en degrés avec la formule suivante : `ceil(round(360 / (2 * PI) * 100 * arctan(Slope / 100)) / 100)`.

### SitePathLink/GradientType

SitePathLink/GradientType est rempli à partir la valeur du tag Slope, arrondi à l'entier supérieur et avec les règles de gestion suivantes :

| tag Slope              | SitePathLink/GradientType  |
| ---------------------- | -------------------------- |
| supérieur ou égal à 9% | "verySteep"                |
| entre 6 et 8%          | "steep"                    |
| égal à 5%              | "medium"                   |
| entre 1 et 4%          | "gentle"                   |
| 0                      | "level"                    |

### SitePathLink/TiltAngle

On utilisera la valeur du tag Tilt, après conversion des pourcentages en degrés avec la formule suivante : `ceil(round(360 / (2 * PI) * 100 * arctan(Tilt / 100)) / 100)`.

Si la valeur résultante n'est pas un entier, l’élément est absent.

### SitePathLink/TiltType

On utilisera les tags Tilt et TiltSide.

| TiltSide | TiltAngle     | SitePathLink/TiltType |
| -------- | ------------- | ---------------------- |
| Left     | ≥ 3% à gauche | strongLeftTilt         |
| Left     | < 3% à gauche | mediumLeftTilt         |
|          | < 2%          | nearlyFlat             |
| Right    | < 3% à droite | mediumRightTilt        |
| Right    | ≥ 3% à droite | strongRightTilt        |

Si TiltSide n'est pas renseigné et que Tilt est supérieur à 2%, l'attribut SitePathLink/TiltType est absent.

### SitePathLink/AccessFeatureType

On prendra la valeur du tag SitePathLink, en camelCase. Valeurs autorisées :
* footpath
* lift
* stairs
* seriesOfStairs
* escalator
* travelator
* ramp
* shuttle
* crossing
* barrier
* narrowEntrance
* hall
* openSpace
* street
* pavement
* concourse
* confinedSpace
* queueManagement

Si la valeur du tag SitePathLink n'est pas dans liste ci-dessus, SitePathLink/AccessFeatureType vaudra "unknown".

Pour les valeurs ci-dessous, une conversion sera réalisée :

| valeur de SitePathLink | SitePathLink/AccessFeatureType |
| ---------------------- | ------------------------------ |
| Quay                   | pavement                       |
| Elevator               | lift                           |
| Corridor               | hall                           |

### SitePathLink/PassageType

On prendra la valeur du tag StructureType, en minuscule. Valeurs autorisées :
* pathway
* corridor
* overpass
* underpass
* tunnel

Si la valeur du tag StructureType n'est pas dans liste ci-dessous ou si le tag n'est pas renseigné, si SitePathLink=Pavement/Footpath/Street/OpenSpace/Concourse/Passage, alors SitePathLink/PassageType vaudra “pathway”. Sinon, l'élément SitePathLink/PassageType est absent.


### SitePathLink/FlooringType

On prendra la valeur du tag FlooringMaterial, en camelCase.

Pour les valeurs ci-dessous, une conversion sera réalisée :

| valeur de FlooringMaterial | SitePathLink/FlooringType |
| -------------------------- | ------------------------- |
| PavingStones               | stone                     |

Valeurs autorisées :
* carpet
* concrete
* asphalt
* cork
* fibreglassGrating
* glazedCeramicTiles
* plasticMatting
* ceramicTiles
* rubber
* steelPlate
* vinyl
* wood
* stone
* grass
* earth
* gravel
* uneven

Si la valeur du tag FlooringMaterial n'est pas dans liste ci-dessus, SitePathLink/FlooringType vaudra "unknown"

Si le tag n'est pas renseigné, l'élément SitePathLink/FlooringType sera absent.

### SitePathLink/TactileWarningStrip

On utilise les tags WDM TactileWarningStrip des ![node] noeuds d'origine et de destination pour renseigner cet élément, avec les règles de gestion suivantes :

| Noeud de départ                                               | Noeud de destination                                          | SitePathLink/TactileWarningStrip |
| ------------------------------------------------------------- | ------------------------------------------------------------- | -------------------------------- |
| TactileWarningStrip=None                                      | TactileWarningStrip=None                                      | "noTactileStrip"                 |
| TactileWarningStrip!=None                                     | TactileWarningStrip!=None                                     | "tactileStripAtBothEnds"         |
| TactileWarningStrip!=None                                     | TactileWarningStrip=None ou TactileWarningStrip non renseigné | "tactileStripAtBeginning"        |
| TactileWarningStrip=None ou TactileWarningStrip non renseigné | TactileWarningStrip!=None                                     | "tactileStripAtEnd"              |
| TactileWarningStrip non renseigné                             | TactileWarningStrip non renseigné                             | "unknown"                        |

### SitePathLink/TactileGuidingStrip

SitePathLink/TactileGuidingStrip est rempli à partir de la valeur du tag TactileGuidingStrip :
* true si TactileGuidingStrip=Yes
* false si TactileGuidingStrip=No
* élément absent sinon

### SitePathLink/SiteRef

SitePathLink/SiteRef est une référence au StopPlace Netex ou PointOfInterest Netex qui contient l'objet SitePathLink.

La référence est de la forme:
* SitePathLink/SiteRef/`@ref` : l'identifiant de l'objet
* SitePathLink/SiteRef/`@version` : la version de l'objet

Si l'objet SitePathLink est entièrement à l'intérieur d'un ou plusieurs objets StopPlace ou PointOfInterest, on prend le plus petit de ces objets.

Sinon, l'élément est absent.

### SitePathLink/equipmentPlaces[]

SitePathLink/equipmentPlaces[] contient une liste de références vers les équipements du SitePathLink ainsi que leurs positions respectives, en suivant les règles de gestion communes.

Les équipements concernés sont :
* CrossingEquipment si le ![way] chemin contient au moins un ![node] noeud avec SitePathLink=Crossing
* EntranceEquipment si le ![way] chemin contient au moins un ![node] noeud avec Entrance=*
* StaircaseEquipment si le ![way] chemin contient au moins un ![node] noeud avec Obstable=Kerb
* ShelterEquipment si le ![way] chemin contient au moins un ![node] noeud avec Obstable=Shelter ou Amenity=Shelter
* SanitaryEquipment si le ![way] chemin contient au moins un ![node] noeud avec Obstable=Toilets ou Amenity=Toilets
* SeatingEquipment si le ![way] chemin contient au moins un ![node] noeud avec Obstable=Bench ou Amenity=Seating
* RoughSurface si le ![way] chemin contient au moins un ![node] noeud avec Obstable=RoughSurface
* GeneralSign si le ![way] chemin contient au moins un ![node] noeud avec Amenity=Sign
* TicketValidatorEquipment si le ![way] chemin contient au moins un ![node] noeud avec Amenity=TicketValidator
* LuggageLockerEquipment si le ![way] chemin contient au moins un ![node] noeud avec Amenity=LuggageLocker
* TrolleyStandEquipment si le ![way] chemin contient au moins un ![node] noeud avec Amenity=TrolleyStand
* MeetingpointService si le ![way] chemin contient au moins un ![node] noeud avec Amenity=MeetingPoint
* PassengerSafetyEquipment si le ![way] chemin contient au moins un ![node] noeud avec Amenity=EmergencyPhone
* RubbishDisposalEquipment si le ![way] chemin contient au moins un ![node] noeud avec Amenity=RubbishDisposal

### SitePathLink/placeEquipments[]

SitePathLink/placeEquipments[] contient une liste de références vers des équipements associés à l'objet SitePathLink de la forme `<StaircaseEquipmentRef ref="Yukaimaps:StaircaseEquipment:w4521_n2_n502:LOC" version="2"/>`.

Les équipements concernés sont :
* CrossingEquipment si SitePathLink=Crossing
* RampEquipment si SitePathLink=Ramp
* StaircaseEquipment si SitePathLink=Stairs
* EscalatorEquipment si SitePathLink=Escalator
* TravelatorEquipment si SitePathLink=Travelator
* LiftEquipment si SitePathLink=Elevator
* QueueingEquipment si SitePathLink=QueueManagement

# Les équipements

## CrossingEquipment

Un élément Netex CrossingEquipment est créé pour chaque ![way] chemin SitePathLink=Crossing (en complément de l'élément Netex SitePathLink) avec les règles de gestion suivantes :
* CrossingEquipment/`@version` : version du ![way] chemin
* CrossingEquipment/`@id` : de la forme Yukaimaps:CrossingEquipment:wAA_nBB_nCC:LOC, avec les mêmes règles de gestion que pour l'élément Netex SitePathLink

Un élément Netex CrossingEquipment est également créé pour chaque ![node] noeud SitePathLink=Crossing avec les règles de gestion suivantes :
* CrossingEquipment/`@version` : version du ![node] noeud
* CrossingEquipment/`@id` : de la forme Yukaimaps:CrossingEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### CrossingEquipment/Width

CrossingEquipment/Width est rempli avec la valeur du tag Width, en mètres arrondis au centimètre.

### CrossingEquipment/DirectionOfUse

CrossingEquipment/DirectionOfUse est rempli avec la valeur fixe “both”.

### CrossingEquipment/CrossingType

CrossingEquipment/CrossingType est rempli avec les règles de gestion suivantes :

* levelCrossing si Crossing=Rail et CrossingBarrier=Yes / CrossingBarrier non renseigné
* levelCrossing si Crossing=UrbanRail et CrossingBarrier=Yes
* barrowCrossing si Crossing=Rail et CrossingBarrier=No
* barrowCrossing si Crossing=UrbanRail et CrossingBarrier=No / CrossingBarrier non renseigné
* roadCrossing si Crossing=Road/CycleWay et CrossingIsland=No / CrossingIsland non renseigné
* roadCrossingWithIsland si Crossing=Road/CycleWay et CrossingIsland=Yes
* other si Crossing!=Rail/UrbanRail/Road/CycleWay
* élément absent sinon

### CrossingEquipment/ZebraCrossing

CrossingEquipment/ZebraCrossing est rempli à partir de la valeur du tag ZebraCrossing :

* true si ZebraCrossing=Good/Worn/Discomfortable/Hazardous
* false si ZebraCrossing=None
* false sinon

### CrossingEquipment/PedestrianLights

CrossingEquipment/PedestrianLights est rempli à partir de la valeur du tag PedestrianLights :
* true si PedestrianLights=Yes
* false si PedestrianLights=No
* élément absent sinon

### CrossingEquipment/AcousticCrossingAids

CrossingEquipment/AcousticCrossingAids est rempli à partir de la valeur du tag AcousticCrossingAids :

* true si AcousticCrossingAids=Good/Worn/Discomfortable/Hazardous
* false si AcousticCrossingAids=None
* false sinon

### CrossingEquipment/TactileGuidanceStrips

CrossingEquipment/TactileGuidanceStrips est construit à partir du tag WDM TactileGuidingStrip, avec les mêmes règles de gestion que pour l'élément SitePathLink.

### CrossingEquipment/TactileWarningStrip

On utilise les tags WDM TactileWarningStrip des ![node] noeuds d'origine et de destination pour renseigner cet élément, avec les mêmes règles de gestion que pour les SitePathLink.

### CrossingEquipment/VisualGuidanceBands

CrossingEquipment/VisualGuidanceBands est rempli à partir de la valeur du tag VisualGuidanceBands:
* true si VisualGuidanceBands=Yes
* false si VisualGuidanceBands=No
* élément absent sinon

### CrossingEquipment/DroppedKerb

On utilise les tags WDM des deux ![node] noeuds d'origine et de destination pour renseigner cet élément, avec les règles de gestion suivantes :

| Noeuds de départ/destination                         | CrossingEquipment/DroppedKerb |
| ---------------------------------------------------- | ----------------------------- |
| KerbHeight est un décimal inférieur ou égal à 0.02 m | "true"                        |
| KerbCut:Width est un décimal positif                 | "true"                        |
| KerbDesign = Lowered/KerbCut/Flush/None              | "true"                        |
| autres conditions                                    | "false"                       |

### CrossingEquipment/MarkingStatus

CrossingEquipment/MarkingStatus est rempli à partir de la valeur du tag ZebraCrossing avec les règles de gestion suivantes :

* good si ZebraCrossing=Good
* worn si ZebraCrossing=Worn/Disconfortable
* hazardous si ZebraCrossing=Hazardous
* None si ZebraCrossing=None
* unknown sinon

### CrossingEquipment/VibratingCrossingAids

CrossingEquipment/VibratingCrossingAids est rempli à partir de la valeur du tag VibratingCrossingAids :
* true si VibratingCrossingAids=Good/Worn/Discomfortable/Hazardous
* false si VibratingCrossingAids=None
* non renseigné sinon

### CrossingEquipment/BumpCrossing

CrossingEquipment/BumpCrossing est rempli à partir de la valeur du tag BumpCrossing :

* true si BumpCrossing=Yes
* false si BumpCrossing=No
* élément absent sinon

### CrossingEquipment/VisualObstacle

On utilise les tags WDM des deux ![node] noeuds d'origine et de destination pour renseigner cet élément, avec les règles de gestion suivantes :

* none si VisualObstacle=None sur les deux extrémités
* carParking si VisualObstacle=CarParking sur les deux extrémités
* vegetation si VisualObstacle=Vegetation sur les deux extrémités
* building si VisualObstacle=Building sur les deux extrémités
* streetFurniture si VisualObstacle=StreetFurniture sur les deux extrémités
* other si VisualObstacle!=None sur une des deux extrémités
* élément absent sinon

## RampEquipment

Un élément Netex RampEquipment est créé pour chaque ![way] chemin SitePathLink=Ramp (en complément de l'élément Netex SitePathLink) avec les règles de gestion suivantes :
* RampEquipment/`@version` : version du ![way] chemin
* RampEquipment/`@id` : de la forme Yukaimaps:RampEquipment:wAA_nBB_nCC:LOC, avec les mêmes règles de gestion que pour l'élément Netex SitePathLink

### RampEquipment/Width

RampEquipment/Width est rempli avec la valeur du tag Width, en mètres arrondis au centimètre.

### RampEquipment/Length

RampEquipment/Length est rempli avec la distance calculée du SitePathLink, en mètres arrondis au cm, sur le même modèle que l'élément SitePathLink/Distance.

### RampEquipment/MaximumLoad

RampEquipment/MaximumLoad est rempli avec la valeur du tag MaxWeight, en kilogrammes arrondis au kilogramme.

### RampEquipment/Gradient

RampEquipment/Gradient est rempli avec la valeur du tag Slope, en degrés, sur le même modèle que l'élément SitePathLink/Gradient.

### RampEquipment/GradientType

RampEquipment/GradientType est rempli à partir la valeur du tag Slope, arrondi à l'entier supérieur et avec les règles de gestion suivantes :

| tag Slope              | RampEquipment/GradientType |
| ---------------------- | -------------------------- |
| supérieur ou égal à 9% | "verySteep"                |
| entre 6 et 8%          | "steep"                    |
| égal à 5%              | "medium"                   |
| entre 1 et 4%          | "gentle"                   |
| 0                      | "level"                    |


### RampEquipment/HandrailType

| valeur du tag Handrail | RampEquipment/HandrailType |
| ---------------------- | -------------------------- |
| "Right"                | "oneSide"                  |
| "Left"                 | "oneSide"                  |
| "Center"               | "oneSide"                  |
| "Both"                 | "bothSides"                |
| "None"                 | "none"                     |
| autres conditions      | non renseigné              |

### RampEquipment/TactileGuidanceStrips

RampEquipment/TactileGuidanceStrips est construit à partir du tag WDM TactileGuidingStrip, avec les mêmes règles de gestion que pour l'élément SitePathLink.

### RampEquipment/VisualGuidanceBands

RampEquipment/VisualGuidanceBands est rempli à partir de la valeur du tag VisualGuidanceBands:
* true si VisualGuidanceBands=Yes
* false si VisualGuidanceBands=No
* élément absent sinon

### RampEquipment/Temporary

CrossingEquipment/Temporary est rempli avec la valeur fixe "false" car seules les rampes permanentes sont cartographiées.

### RampEquipment/RestStopDistance

RampEquipment/RestStopDistance est rempli avec la valeur de RestStopDistance, en mètres arrondis au centimètre.

### RampEquipment/SafetyEdge

RampEquipment/SafetyEdge est rempli à partir de SafetyEdge avec les règles de gestion suivantes :

* none si SafetyEdge=None
* oneSide si SafetyEdge=Right
* oneSide si SafetyEdge=Left
* bothSides si SafetyEdge=Both

### RampEquipment/TurningSpace

RampEquipment/TurningSpace est rempli à partir de TurningSpacePosition avec les règles de gestion suivantes :

* none si TurningSpacePosition=None
* bottom si TurningSpacePosition=Bottom
* top si TurningSpacePosition=Top
* topAndBottom si TurningSpacePosition=TopAndBottom

## EscalatorEquipment

Un élément Netex EscalatorEquipment est créé pour chaque ![way] chemin SitePathLink=Escalator (en complément de l'élément Netex SitePathLink) avec les règles de gestion suivantes :
* EscalatorEquipment/`@version` : version du ![way] chemin
* EscalatorEquipment/`@id` : de la forme Yukaimaps:EscalatorEquipment:wAA_nBB_nCC:LOC, avec les mêmes règles de gestion que pour l'élément Netex SitePathLink

### EscalatorEquipment/PublicCode

EscalatorEquipment/PublicCode est rempli avec la valeur du tag PublicCode.

### EscalatorEquipment/Width

EscalatorEquipment/Width est rempli avec la valeur du tag Width, en mètres arrondis au centimètre.

### EscalatorEquipment/DirectionOfUse

| valeur du tag Incline              | valeur du tag Conveying | EscalatorEquipment/DirectionOfUse |
| ---------------------------------- | ----------------------- | --------------------------------- |
| toutes valeurs / tag non renseigné | "Reversible"            | "both"                            |
| "Up"                               | "Forward"               | "up"                              |
| "Up"                               | "Backward"              | "down"                            |
| "Down"                             | "Forward"               | "down"                            |
| "Down"                             | "Backward"              | "up"                              |
| autres conditions                  | autres conditions       | non renseigné                     |

### EscalatorEquipment/TactileActuators

EscalatorEquipment/TactileActuators est rempli à partir de la valeur du tag StoppedIfUnused:
* true si StoppedIfUnused=Yes
* false si StoppedIfUnused=No
* élément absent sinon

### EscalatorEquipment/EnergySaving

EscalatorEquipment/EnergySaving est rempli à partir de la valeur du tag StoppedIfUnused:
* true si StoppedIfUnused=Yes
* false si StoppedIfUnused=No
* élément absent sinon

### EscalatorEquipment/MonitoringRemoteControl

EscalatorEquipment/MonitoringRemoteControl est rempli à partir de MonitoringRemoteControl :

* true si MonitoringRemoteControl=Yes
* false si MonitoringRemoteControl=No
* non renseigné sinon

## TravelatorEquipment

Un élément Netex TravelatorEquipment est créé pour chaque ![way] chemin SitePathLink=Travelator (en complément de l'élément Netex SitePathLink) avec les règles de gestion suivantes :
* TravelatorEquipment/`@version` : version du ![way] chemin
* TravelatorEquipment/`@id` : de la forme Yukaimaps:TravelatorEquipment:wAA_nBB_nCC:LOC, avec les mêmes règles de gestion que pour l'élément Netex SitePathLink

### TravelatorEquipment/PublicCode

TravelatorEquipment/PublicCode est rempli avec la valeur du tag PublicCode.

### TravelatorEquipment/Width

TravelatorEquipment/Width est rempli avec la valeur du tag Width, en mètres arrondis au centimètre.

### TravelatorEquipment/DirectionOfUse

TravelatorEquipment/DirectionOfUse est rempli à partir de la valeur du tag Conveying:
* both si Conveying=Reversible
* up si Conveying=Forward/Backward
* élément absent sinon

### TravelatorEquipment/TactileActuators

TravelatorEquipment/TactileActuators est rempli à partir de la valeur du tag StoppedIfUnused:
* true si StoppedIfUnused=Yes
* false si StoppedIfUnused=No
* élément absent sinon


### TravelatorEquipment/EnergySaving

TravelatorEquipment/EnergySaving est rempli à partir de la valeur du tag StoppedIfUnused:
* true si StoppedIfUnused=Yes
* false si StoppedIfUnused=No
* élément absent sinon

### TravelatorEquipment/Length

TravelatorEquipment/Length est rempli avec la distance calculée du SitePathLink, en mètres arrondis au cm, sur le même modèle que l'élément SitePathLink/Distance.

### TravelatorEquipment/Gradient

TravelatorEquipment/Gradient est rempli avec la valeur du tag Slope, en degrés, sur le même modèle que l'élément SitePathLink/Gradient.

## LiftEquipment

Un élément Netex LiftEquipment est créé pour chaque ![way] chemin SitePathLink=Elevator (en complément de l'élément Netex SitePathLink) avec les règles de gestion suivantes :
* LiftEquipment/`@version` : version du ![way] chemin
* LiftEquipment/`@id` : de la forme Yukaimaps:LiftEquipment:wAA_nBB_nCC:LOC, avec les mêmes règles de gestion que pour l'élément Netex SitePathLink

Un élément Netex LiftEquipment est également créé pour chaque ![node] noeud SitePathLink=Elevator avec les règles de gestion suivantes :
* LiftEquipment/`@version` : version du ![node] noeud
* LiftEquipment/`@id` : de la forme Yukaimaps:LiftEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### LiftEquipment/PublicCode

LiftEquipment/PublicCode est rempli avec la valeur du tag PublicCode.

### LiftEquipment/Depth

LiftEquipment/Depth est rempli avec la valeur du tag Depth, en mètres arrondis au centimètre.

### LiftEquipment/MaximumLoad

LiftEquipment/MaximumLoad est rempli avec la valeur du tag MaxWeight, en kilogrammes arrondis au kilogramme.

### LiftEquipment/WheelchairPassable

LiftEquipment/WheelchairPassable est rempli à partir de la valeur du tag WheelchairAccess :

* true si WheelchairAccess=Yes
* false sinon

### LiftEquipment/WheelchairTurningCircle

LiftEquipment/WheelchairTurningCircle est rempli avec la valeur du tag ManoeuvringDiameter, en mètres arrondis au centimètre.

### LiftEquipment/InternalWidth

LiftEquipment/InternalWidth est rempli avec la valeur du tag InternalWidth, en mètres arrondis au centimètre.

### LiftEquipment/HandrailType

| valeur du tag Handrail | LiftEquipment/HandrailType |
| ---------------------- | -------------------------- |
| "Right"                | "oneSide"                  |
| "Left"                 | "oneSide"                  |
| "Center"               | "oneSide"                  |
| "Both"                 | "bothSides"                |
| "None"                 | "none"                     |
| autres conditions      | non renseigné              |

### LiftEquipment/HandrailHeight

LiftEquipment/HandrailHeight est rempli avec la valeur du tag HandrailHeight, en mètres arrondis au centimètre.

### LiftEquipment/RaisedButtons

LiftEquipment/RaisedButtons est rempli à partir de la valeur du tag RaisedButtons:
* true si RaisedButtons!=None
* false si RaisedButtons=None
* élément absent sinon

### LiftEquipment/BrailleButtons

LiftEquipment/BrailleButtons est rempli à partir de la valeur du tag RaisedButtons:
* true si RaisedButtons=Braille
* false si RaisedButtons!=Braille
* élément absent sinon

### LiftEquipment/MirrorOnOppositeSide

LiftEquipment/MirrorOnOppositeSide est rempli à partir de la valeur du tag MirrorOnOppositeSide:
* true si MirrorOnOppositeSide=Yes
* false si MirrorOnOppositeSide=No
* élément absent sinon

### LiftEquipment/Attendant

LiftEquipment/Attendant est rempli à partir de la valeur du tag Attendant:
* true si Attendant=Yes
* false si Attendant=No
* élément absent sinon

### LiftEquipment/Automatic

LiftEquipment/Automatic est rempli à partir de la valeur du tag AutomaticDoor :

* true si AutomaticDoor=Yes
* false si AutomaticDoor=No
* élément absent sinon

### LiftEquipment/AlarmButton

LiftEquipment/AlarmButton est rempli à partir de la valeur du tag AlarmButton :

* true si AlarmButton=Yes
* false si AlarmButton=No
* élément absent sinon

### LiftEquipment/AudioAnnouncements

LiftEquipment/AudioAnnouncements est rempli à partir de la valeur du tag AudioAnnouncements :

* true si AudioAnnouncements=Yes
* false si AudioAnnouncements=No
* false sinon

### LiftEquipment/ReachedFloorAnnouncement

LiftEquipment/ReachedFloorAnnouncement est rempli à partir de AudioAnnouncements et VisualDisplays avec les règles de gestion suivantes :

* none si AudioAnnouncements=No et VisualDisplays=No
* visual si VisualDisplays=Yes et AudioAnnouncements=No ou n'est pas renseigné
* audio si AudioAnnouncements=Yes et VisualDisplays=No ou n'est pas renseigné
* visualAndAudio si AudioAnnouncements=Yes et VisualDisplays=Yes
* élément absent sinon

### LiftEquipment/MagneticInductionLoop

LiftEquipment/MagneticInductionLoop est rempli à partir de la valeur du tag MagneticInductionLoop:
* true si MagneticInductionLoop=Yes
* false si MagneticInductionLoop=No
* élément absent sinon

## QueueingEquipment

Un élément Netex QueueingEquipment est créé pour chaque ![way] chemin SitePathLink=QueueManagement (en complément de l'élément Netex SitePathLink) avec les règles de gestion suivantes :
* QueueingEquipment/`@version` : version du ![way] chemin
* QueueingEquipment/`@id` : de la forme Yukaimaps:QueueingEquipment:wAA_nBB_nCC:LOC, avec les mêmes règles de gestion que pour l'élément Netex SitePathLink

### QueueingEquipment/Width

QueueingEquipment/Width est rempli avec la valeur du tag Width, en mètres arrondis au centimètre.

## StaircaseEquipment

Un élément Netex StaircaseEquipment est créé pour chaque ![way] chemin SitePathLink=Stairs (en complément de l'élément Netex SitePathLink) avec les règles de gestion suivantes :
* StaircaseEquipment/`@version` : version du ![way] chemin
* StaircaseEquipment/`@id` : de la forme Yukaimaps:StaircaseEquipment:wAA_nBB_nCC:LOC, avec les mêmes règles de gestion que pour l'élément Netex SitePathLink

Un élément Netex StaircaseEquipment est également créé pour chaque ![node] noeud Obstacle=Kerb avec les règles de gestion suivantes :
* StaircaseEquipment/`@version` : version du ![node] noeud
* StaircaseEquipment/`@id` : de la forme Yukaimaps:StaircaseEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### StaircaseEquipment/Width

StaircaseEquipment/Width est rempli avec la valeur du tag Width, en mètres arrondis au centimètre.

### StaircaseEquipment/DirectionOfUse

StaircaseEquipment/DirectionOfUse est rempli avec la valeur fixe “both”.

### StaircaseEquipment/NumberofSteps

S'il est issu d'un ![way] chemin SitePathLink=Stairs : StaircaseEquipment/NumberofSteps est rempli avec la valeur du tag StepCount, avec les mêmes règles de gestion que pour l'élément Netex SitePathLink.

S'il est issu d'un ![node] noeud avec Obstacle=Kerb :  StaircaseEquipment/NumberofSteps est rempli avec la valeur fixe "1".

### StaircaseEquipment/StepHeight

S'il est issu d'un ![way] chemin SitePathLink=Stairs : StaircaseEquipment/StepHeight est rempli avec la valeur du tag StepHeight, en mètres arrondi au centimètre.

S'il est issu d'un ![node] noeud avec Obstacle=Kerb :  StaircaseEquipment/StepHeight est rempli avec la valeur du tag StepHeight ou à défaut Height ou KerbHeight, en mètres arrondi au centimètre.

### StaircaseEquipment/StepLength

StaircaseEquipment/StepLength est rempli avec la valeur du tag StepLength, en mètres arrondi au centimètre.

### StaircaseEquipment/StepColourContrast

| Tag StepColourContrast | StaircaseEquipment/StepColourContrast |
| ---------------------- | ------------------------------------- |
| "None"                 | "false"                               |
| autres valeurs         | "true"                                |
| tag absent             | élément absent                        |

### StaircaseEquipment/StepCondition

StaircaseEquipment/StepCondition est rempli avec la valeur du tag StepCondition, avec les règles de gestion suivantes :

* even si StepCondition=Even
* uneven si StepCondition=Uneven
* rough si StepCondition=Rough
* non renseigné sinon

### StaircaseEquipment/HandrailType

| valeur du tag Handrail | StaircaseEquipment/HandrailType |
| ---------------------- | ------------------------------- |
| "Right"                | "oneSide"                       |
| "Left"                 | "oneSide"                       |
| "Center"               | "oneSide"                       |
| "Both"                 | "bothSides"                     |
| "None"                 | "none"                          |
| autres conditions      | non renseigné                   |


### StaircaseEquipment/TactileWriting

StaircaseEquipment/TactileWriting est rempli avec les tags Handrail:TactileWriting :

* true si Handrail:TactileWriting=Yes
* false si Handrail:TactileWriting=No
* non renseigné sinon

### StaircaseEquipment/StairRamp

StaircaseEquipment/StairRamp est rempli avec la valeur du tag StairRamp avec les règles de gestion suivantes :

* none si StairRamp=None
* bicycle si StairRamp=Bicycle
* luggage si StairRamp=Luggage
* stroller StairRamp=Stroller
* other si StairRamp est rempli avec une autre valeur
* non renseigné sinon

### StaircaseEquipment/TopEnd

S'il est issu d'un ![way] chemin SitePathLink=Stairs, les éléments suivants peuvent être présents en fonction des tags sur le ![node] noeud correspondant au haut de l'escalier (calculé à l'aide du tag Incline) :

* StaircaseEquipment/TopEnd/ContinuingHandrail, en fonction de la valeur du tag ContinuingHandrail
* StaircaseEquipment/TopEnd/TexturedSurface, en fonction de la valeur du tag TactileWarningStrip
* StaircaseEquipment/TopEnd/VisualContrast, construit avec le tag VisualContrast

Les règles de gestion suivantes sont utilisées pour ContinuingHandrail :

* true si la valeur est BothSides
* true si la valeur est OneSide
* true si la valeur est Yes
* false si la valeur est No
* non renseigné sinon

Les règles de gestion suivantes sont utilisées pour VisualContrast :

* true si la valeur est Yes
* false si la valeur est No
* non renseigné sinon

Les règles de gestion suivantes sont utilisées pour TexturedSurface :

* true si TactileWarningStrip!=None
* false sinon

Si Incline est absent, on considère que le haut de l'escalier correspond au ![node] noeud de fin.

S'il est issu d'un ![node] noeud avec Obstacle=Kerb, StaircaseEquipment/TopEnd/TexturedSurface est construit avec les règles de gestion suivantes :

* true si TactileWarningStrip!=None
* false sinon

### StaircaseEquipment/BottomEnd

S'il est issu d'un ![way] chemin SitePathLink=Stairs, StaircaseEquipment/BottomEnd est renseigné de la même manière que StaircaseEquipment/TopEnd avec le ![node] noeud du bas de l'escalier.

Cas particulier : Si le bas de l'escalier n'a pas les tags ContinuingHandrail,TactileWarningStrip ni VisualContrast alors tout l'élément BottomEnd est absent.

S'il est issu d'un ![node] noeud avec Obstacle=Kerb, StaircaseEquipment/BottomEnd est absent.

### StaircaseEquipment/ContinuousHandrail

StaircaseEquipment/ContinuousHandrail est renseigné à partir du tag ContinuousHandrail :

* true si ContinuousHandrail=BothSides
* true si ContinuousHandrail=OneSide
* true si ContinuousHandrail=Yes
* false si ContinuousHandrail=No
* non renseigné sinon

### StaircaseEquipment/NumberOfFlights

StaircaseEquipment/NumberOfFlights est rempli avec la valeur du tag StairFlightCount, avec les mêmes règles de gestion que pour StepCount.

## EntranceEquipment

Un élément Netex EntranceEquipment est créé pour chaque ![node] noeud Entrance=* avec les règles de gestion suivantes :
* EntranceEquipment/`@version` : version du ![node] noeud
* EntranceEquipment/`@id` : de la forme Yukaimaps:EntranceEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### EntranceEquipment/Width

EntranceEquipment/Width est rempli avec valeur du tag Width, en mètres arrondis au cm.

### EntranceEquipment/Door
EntranceEquipment/Door est rempli à partir de la valeur du tag EntrancePassage :
* true si EntrancePassage=Door
* false si EntrancePassage=Opening
* élément absent sinon

### EntranceEquipment/DoorHandleOutside

EntranceEquipment/DoorHandleOutside est rempli à partir de DoorHandle:Outside :

* none si DoorHandle:Outside=None
* lever si DoorHandle:Outside=Lever
* button si DoorHandle:Outside=Button
* knob si DoorHandle:Outside=Knob
* crashBar si DoorHandle:Outside=CrashBar
* grabRail si DoorHandle:Outside=GrabRail
* windowLever si DoorHandle:Outside=WindowLever
* vertical si DoorHandle:Outside=Vertical
* other si DoorHandle:Outside=Other
* other si DoorHandle:Outside est renseigné avec une autre valeur
* élément absent sinon

### EntranceEquipment/DoorHandleInside

EntranceEquipment/DoorHandleInside est rempli à partir de DoorHandle:Inside :

* none si DoorHandle:Inside=None
* lever si DoorHandle:Inside=Lever
* button si DoorHandle:Inside=Button
* knob si DoorHandle:Inside=Knob
* crashBar si DoorHandle:Inside=CrashBar
* grabRail si DoorHandle:Inside=GrabRail
* windowLever si DoorHandle:Inside=WindowLever
* vertical si DoorHandle:Inside=Vertical
* other si DoorHandle:Inside=Other
* other si DoorHandle:Inside est renseigné avec une autre valeur
* élément absent sinon

### EntranceEquipment/RevolvingDoor
EntranceEquipment/RevolvingDoor est rempli à partir de la valeur du tag Door :
* true si Door=Revolving
* false sinon
* élément absent si le tag Door n'est pas renseigné

### EntranceEquipment/Barrier
EntranceEquipment/Barrier est rempli à partir de la valeur du tag EntrancePassage :
* true si EntrancePassage=Barrier
* false si EntrancePassage=Opening
* élément absent sinon

### EntranceEquipment/DropKerbOutside

| condition WDM                                        | EntranceEquipment/DropKerbOutside |
| ---------------------------------------------------- | --------------------------------- |
| KerbHeight est un décimal inférieur ou égal à 0.02 m | "true"                            |
| KerbCut:Width est un décimal positif                 | "true"                            |
| autres conditions                                    | "false"                           |

### EntranceEquipment/AutomaticDoor

EntranceEquipment/AutomaticDoor est rempli à partir de la valeur du tag AutomaticDoor :
* true si AutomaticDoor=Yes
* false si AutomaticDoor=No
* élément absent sinon

### EntranceEquipment/GlassDoor

EntranceEquipment/GlassDoor est rempli à partir de la valeur du tag GlassDoor :
* true si GlassDoor=Yes/Bad
* false si GlassDoor=No
* élément absent sinon

### EntranceEquipment/WheelchairPassable

EntranceEquipment/WheelchairPassable est rempli à partir de la valeur du tag WheelchairAccess :

* true si WheelchairAccess=Yes
* false sinon

### EntranceEquipment/AudioOrVideoIntercom

EntranceEquipment/AudioOrVideoIntercom est rempli à partir de la valeur du tag EntranceAttention :
* true si EntranceAttention=VideoIntercom
* false si EntranceAttention!=VideoIntercom
* élément absent sinon

### EntranceEquipment/EntranceAttention

EntranceEquipment/EntranceAttention est rempli à partir de la valeur du tag EntranceAttention :
* none si EntranceAttention=None
* other si EntranceAttention=Other
* doorbell si EntranceAttention=Doorbell
* intercom si EntranceAttention=Intercom/VideoIntercom
* élément absent sinon

### EntranceEquipment/DoorstepMark

EntranceEquipment/DoorstepMark est rempli à partir de la valeur du tag TactileWarningStrip :
* false si TactileWarningStrip=None
* true si TactileWarningStrip est présent et ne vaut pas "None"
* élément absent sinon

### EntranceEquipment/NecessaryForceToOpen

EntranceEquipment/NecessaryForceToOpen est rempli à partir la valeur du tag NecessaryForceToOpen, renseigné en Newton, avec les règles de gestion suivantes :

* heavyForce si NecessaryForceToOpen > 50
* mediumForce si NecessaryForceToOpen est entre 20 et 50
* lightForce si NecessaryForceToOpen < 20
* noForce si NecessaryForceToOpen = 0
* noForce si le tag NecessaryForceToOpen n'est pas renseigné mais que Door=None ou EntrancePassage=Opening ou AutomaticDoor=Yes
* unknown sinon

### EntranceEquipment/RampDoorbell

EntranceEquipment/RampDoorbell est rempli à partir de RampDoorbell :

* true si RampDoorbell=Yes
* false si RampDoorbell=No
* non renseigné sinon

### EntranceEquipment/Recognizable

EntranceEquipment/Recognizable est rempli à partir de Recognizable :

* true si Recognizable=Yes
* false si Recognizable=No
* non renseigné sinon

### EntranceEquipment/TurningSpacePosition

EntranceEquipment/TurningSpacePosition est rempli à partir de TurningSpacePosition avec les règles de gestion suivantes :

* none si TurningSpacePosition=None
* inside si TurningSpacePosition=Inside
* outside si TurningSpacePosition=Outside
* insideAndOutside si TurningSpacePosition=InsideAndOutside

### EntranceEquipment/WheelchairTurningCircle

EntranceEquipment/WheelchairTurningCircle est rempli avec la plus petite valeur des tags suivants en mètres arrondis au cm :

* OutsideTurningSpace:Width
* OutsideTurningSpace:Length
* InsideTurningSpace:Width
* InsideTurningSpace:Length

## ShelterEquipment

Un élément Netex ShelterEquipment est créé pour chaque ![node] noeud Amenity=Shelter ou Obstacle=Shelter avec les règles de gestion suivantes :
* ShelterEquipment/`@version` : version du ![node] noeud
* ShelterEquipment/`@id` : de la forme Yukaimaps:ShelterEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

En complément, un élément Netex ShelterEquipment est créé pour chaque ![node] noeud Quay=* avec le tag Shelter=Yes, en utilisant la position, l'identifiant de noeud et la version de noeud du quai.

### ShelterEquipment/keyList[]

ShelterEquipment/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### ShelterEquipment/Seats

ShelterEquipment/Seats est rempli avec la valeur du tag SeatCount.

### ShelterEquipment/Width

ShelterEquipment/Width est rempli avec valeur du tag Width, en mètres arrondis au cm.

## SanitaryEquipment

Un élément Netex SanitaryEquipment est créé pour chaque ![node] noeud Amenity=Toilets ou Obstacle=Toilets avec les règles de gestion suivantes :
* SanitaryEquipment/`@version` : version du ![node] noeud
* SanitaryEquipment/`@id` : de la forme Yukaimaps:SanitaryEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### SanitaryEquipment/keyList[]

SanitaryEquipment/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### SanitaryEquipment/AccessibilityAssessment

SanitaryEquipment/AccessibilityAssessment est rempli en suivant les règles communes.

La seule limitations/AccessibilityLimitation présente est WheelchairAccess, remplie à partir du tag WheelchairAccess.

L’élément AccessibilityAssessment/MobilityImpairedAccess prendra la même valeur que l’ément AccessibilityAssessment/limitations/AccessibilityLimitation/WheelchairAccess.

### SanitaryEquipment/SanitaryFacilityList

L'élément SanitaryEquipment/SanitaryFacilityList comprend une liste de clefs séparées par des espaces :
* "toilet" est toujours présente
* "wheelchairAccessToilet" est présente si WheelchairAccess=Yes
* "shower" est présente si Toilets:Shower=Yes
* "washingAndChangeFacilities" est présente si Toilets:Shower=Yes
* "babyChange" est présente si Toilets:BabyChange=Yes/Bad
* "wheelchairBabyChange" est présente si Toilets:BabyChange=Yes

### SanitaryEquipment/FreeToUse

SanitaryEquipment/FreeToUse est rempli avec la valeur du tag FreeToUse :
* true si FreeToUse=Yes
* false si FreeToUse=No
* non renseigné sinon

### SanitaryEquipment/WheelchairTurningCircle

SanitaryEquipment/WheelchairTurningCircle est rempli avec la valeur du tag ManoeuvringDiameter, en mètres arrondis au centimètre.

### SanitaryEquipment/Staffing

SanitaryEquipment/Staffing est rempli avec la valeur du tag Staffing :
* unmanned si Staffing=No
* partTime si Staffing=PartTime
* fullTime si Staffing=FullTime
* non renseigné sinon

### SanitaryEquipment/KeyScheme

SanitaryEquipment/KeyScheme est rempli avec la valeur du tag WheelchairAccess:Description.

### SanitaryEquipment/HandWashing

SanitaryEquipment/HandWashing est rempli avec la valeur du tag Toilets:HandWashing :

* true si Toilets:HandWashing=Yes
* false si Toilets:HandWashing=No
* non renseigné sinon

### SanitaryEquipment/DrinkingWater

SanitaryEquipment/DrinkingWater est rempli avec la valeur du tag Toilets:DrinkingWater :

* true si Toilets:DrinkingWater=Yes
* false si Toilets:DrinkingWater=No
* non renseigné sinon

### SanitaryEquipment/ToiletsType

SanitaryEquipment/ToiletsType est rempli avec la valeur du tag Toilets:Position :

* seated si Toilets:Position=Seated
* urinal si Toilets:Position=Urinal
* squat si Toilets:Position=Squat
* seatedAndUrinal si Toilets:Position comprend à la fois les valeurs Seated et Urinal
* non renseigné sinon

## RoughSurface

Un élément Netex RoughSurface est créé pour chaque ![node] noeud Obstacle=RoughSurface avec les règles de gestion suivantes :
* RoughSurface/`@version` : version du ![node] noeud
* RoughSurface/`@id` : de la forme Yukaimaps:RoughSurface:nAA:LOC, avec lnAA l'identifiant du ![node] noeud

### RoughSurface/keyList[]

RoughSurface/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### RoughSurface/Width

RoughSurface/Width est rempli avec valeur du tag Width, en mètres arrondis au cm.

### RoughSurface/SurfaceType

RoughSurface/SurfaceType est rempli à partir de la valeur de FlooringMaterial, en camelCase. Valeurs autorisées :
* asphalt
* bricks
* cobbles
* earth
* grass
* looseSurface
* pavingStones
* roughSurface
* smooth

Si la valeur finale n'est pas dans la liste ci-dessus, RoughSurface/SurfaceType prend la valeur "other".

Si le tag FlooringMaterial n'est pas renseigné, RoughSurface/SurfaceType prend également la valeur "other".

## GeneralSign

Un élément Netex GeneralSign est créé pour chaque ![node] noeud Amenity=Sign avec les règles de gestion suivantes :
* GeneralSign/`@version` : version du ![node] noeud
* GeneralSign/`@id` : de la forme Yukaimaps:GeneralSign:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### GeneralSign/keyList[]

GeneralSign/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### GeneralSign/Width

ShelterEquipment/Width est rempli avec valeur du tag Width, en mètres arrondis au cm.

## SeatingEquipment

Un élément Netex SeatingEquipment est créé pour chaque ![node] noeud Amenity=Seating ou Obstacle=Bench avec les règles de gestion suivantes :
* SeatingEquipment/`@version` : version du ![node] noeud
* SeatingEquipment/`@id` : de la forme Yukaimaps:SeatingEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

En complément, un élément Netex SeatingEquipment est créé pour chaque ![node] noeud Quay=* avec le tag Seating=Yes, en utilisant la position, l'identifiant de noeud et la version de noeud du quai.

### SeatingEquipment/keyList[]

SeatingEquipment/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### SeatingEquipment/Seats

SeatingEquipment/Seats est rempli avec la valeur du tag SeatCount.

### SeatingEquipment/Width

SeatingEquipment/Width est rempli avec valeur du tag Width, en mètres arrondis au cm.

### SeatingEquipment/BackRest

SeatingEquipment/BackRest est rempli avec la valeur du tag BackRest :

* true si BackRest=Yes
* false si BackRest=No
* non renseigné sinon

## TicketingEquipment

Un élément Netex TicketingEquipment est créé pour chaque ![node] noeud Amenity=TicketOffice/TicketVendingMachine avec les règles de gestion suivantes :
* TicketingEquipment/`@version` : version du ![node] noeud
* TicketingEquipment/`@id` : de la forme Yukaimaps:TicketingEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

En complément, un élément Netex TicketingEquipment est créé pour chaque ![node] noeud Quay=* avec le tag TicketVendingMachine=Yes, en utilisant la position, l'identifiant de noeud et la version de noeud du quai.

### TicketingEquipment/keyList[]

TicketingEquipment/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### TicketingEquipment/TicketOffice

TicketingEquipment/TicketOffice est rempli avec la valeur fixe "true" si l'objet WDM est Amenity=TicketOffice.

### TicketingEquipment/TicketMachines

TicketingEquipment/TicketMachines est rempli avec la valeur fixe "true" si l'objet WDM est Amenity=TicketVendingMachine ou a TicketVendingMachine=Yes.

### TicketingEquipment/InductionLoops

TicketingEquipment/InductionLoops est renseigné avec la valeur du tag MagneticInductionLoop :
* true si MagneticInductionLoop=Yes
* false si MagneticInductionLoop=No
* non renseigné sinon

### TicketingEquipment/WheelchairSuitable

TicketingEquipment/WheelchairSuitable est renseigné avec la valeur du tag WheelchairAccess :
* true si WheelchairAccess=Yes
* false si WheelchairAccess=No
* non renseigné sinon

## TicketValidatorEquipment

Un élément Netex TicketValidatorEquipment est créé pour chaque ![node] noeud Amenity=TicketValidator avec les règles de gestion suivantes :
* TicketValidatorEquipment/`@version` : version du ![node] noeud
* TicketValidatorEquipment/`@id` : de la forme Yukaimaps:TicketValidatorEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

En complément, un élément Netex TicketValidatorEquipment est créé pour chaque ![node] noeud Quay=* avec le tag TicketValidator=Yes, en utilisant la position, l'identifiant de noeud et la version de noeud du quai.

### TicketValidatorEquipment/keyList[]

TicketValidatorEquipment/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

## LuggageLockerEquipment

Un élément Netex LuggageLockerEquipment est créé pour chaque ![node] noeud Amenity=LuggageLocker avec les règles de gestion suivantes :
* LuggageLockerEquipment/`@version` : version du ![node] noeud
* LuggageLockerEquipment/`@id` : de la forme Yukaimaps:LuggageLockerEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### LuggageLockerEquipment/keyList[]

LuggageLockerEquipment/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

## TrolleyStandEquipment

Un élément Netex TrolleyStandEquipment est créé pour chaque ![node] noeud Amenity=TrolleyStand avec les règles de gestion suivantes :
* TrolleyStandEquipment/`@version` : version du ![node] noeud
* TrolleyStandEquipment/`@id` : de la forme Yukaimaps:TrolleyStandEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### TrolleyStandEquipment/keyList[]

TrolleyStandEquipment/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### TrolleyStandEquipment/FreeToUse

TrolleyStandEquipment/FreeToUse est renseigné avec la valeur du tag FreeToUse :
* true si FreeToUse=Yes
* false si FreeToUse=No
* non renseigné sinon

## MeetingpointService

Un élément Netex MeetingpointService est créé pour chaque ![node] noeud Amenity=MeetingPoint avec les règles de gestion suivantes :
* MeetingpointService/`@version` : version du ![node] noeud
* MeetingpointService/`@id` : de la forme Yukaimaps:MeetingpointService:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### MeetingpointService/keyList[]

MeetingpointService/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

## PassengerSafetyEquipment

Un élément Netex PassengerSafetyEquipment est créé pour chaque ![node] noeud Amenity=EmergencyPhone avec les règles de gestion suivantes :
* PassengerSafetyEquipment/`@version` : version du ![node] noeud
* PassengerSafetyEquipment/`@id` : de la forme Yukaimaps:PassengerSafetyEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### PassengerSafetyEquipment/keyList[]

PassengerSafetyEquipment/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### PassengerSafetyEquipment/PublicCode

PassengerSafetyEquipment/PublicCode est rempli avec la valeur du tag PublicCode.

### PassengerSafetyEquipment/SosPanel

PassengerSafetyEquipment/SosPanel est rempli avec la valeur fixe "true".

## RubbishDisposalEquipment

Un élément Netex RubbishDisposalEquipment est créé pour chaque ![node] noeud Amenity=RubbishDisposal avec les règles de gestion suivantes :
* RubbishDisposalEquipment/`@version` : version du ![node] noeud
* RubbishDisposalEquipment/`@id` : de la forme Yukaimaps:RubbishDisposalEquipment:nAA:LOC, avec nAA l'identifiant du ![node] noeud

En complément, un élément Netex RubbishDisposalEquipment est créé pour chaque ![node] noeud Quay=* avec le tag RubbishDisposal=Yes, en utilisant la position, l'identifiant de noeud et la version de noeud du quai.

### RubbishDisposalEquipment/keyList[]

RubbishDisposalEquipment/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

## CommunicationService

Un élément Netex CommunicationService est créé pour chaque ![node] noeud Amenity=PostBox avec les règles de gestion suivantes :
* CommunicationService/`@version` : version du ![node] noeud
* CommunicationService/`@id` : de la forme Yukaimaps:CommunicationService:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### CommunicationService/keyList[]

CommunicationService/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### CommunicationService/PublicCode

CommunicationService/PublicCode est rempli avec la valeur du tag PublicCode.

### CommunicationService/ServiceList

CommunicationService/ServiceList est rempli avec la valeur fixe "postbox".

## AssistanceService

Un élément Netex AssistanceService est créé pour chaque ![node] noeud Amenity=ReceptionDesk/TicketOffice avec les règles de gestion suivantes :
* AssistanceService/`@version` : version du ![node] noeud
* AssistanceService/`@id` : de la forme Yukaimaps:AssistanceService:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### AssistanceService/keyList[]

AssistanceService/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### AssistanceService/AssistanceFacilityList

AssistanceService/AssistanceFacilityList est rempli avec la valeur fixe "information".

### AssistanceService/Staffing

AssistanceService/Staffing est rempli avec la valeur du tag Staffing :
* unmanned si Staffing=No
* partTime si Staffing=PartTime
* fullTime si Staffing=FullTime
* non renseigné sinon

### AssistanceService/AccessibilityTrainedStaff

AssistanceService/AccessibilityTrainedStaff est rempli avec la valeur du tag AccessibilityTrainedStaff :
* true si AccessibilityTrainedStaff=Yes
* false si AccessibilityTrainedStaff=No
* non renseigné sinon

# AccessSpace et PointOfInterestSpace

Un élément Netex AccessSpace est créé pour chaque ![area] zone avec InsideSpace!=Quay, avec les règles de gestion suivantes :
* PointOfInterestSpace/`@version` ou AccessSpace/`@version` : version de la ![area] zone
* PointOfInterestSpace/`@id` ou AccessSpace/`@id` : de la forme Yukaimaps:AccessSpace:wAA:LOC ou Yukaimaps:PointOfInterestSpace:wAA:LOC, avec wAA l'identifiant de la ![area] zone

(...)

# ParkingBay

Un élément Netex ParkingBay est créé pour chaque ![node] noeud avec ParkingBay=Disabled, avec les règles de gestion suivantes :
* ParkingBay/`@version` : version du ![node] noeud
* ParkingBay/`@id` : de la forme Yukaimaps:ParkingBay:nAA:LOC, avec nAA l'identifiant du ![node] noeud

### ParkingBay/keyList[]

ParkingBay/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

### ParkingBay/Description

ParkingBay/Description est rempli avec la valeur du tag Description.

### ParkingBay/Centroid/Location

La géométrie du ![node] noeud est exportée dans ParkingBay/Centroid/Location en suivant les règles communes.

### ParkingBay/AccessibilityAssessment

ParkingBay/AccessibilityAssessment est rempli en suivant les règles communes.

Les limitations/AccessibilityLimitation présentes sont :

* WheelchairAccess (cf ci-dessous)
* StepFreeAccess :
  * true si StepFreeAccess=Yes
  * false si StepFreeAccess=No
  * unknown sinon
* VisualSignsAvailable :
  * false si ParkingVisibility=Unmarked
  * true si ParkingVisibility=Demarcated
  * partial si ParkingVisibility a une autre valeur
  * unknown sinon

WheelchairAccess est renseigné en suivant les règles communes à partir du tag WheelchairAccess.

Si le tag WheelchairAccess n'est pas renseigné, WheelchairAccess est rempli à l'aide des règles suivantes :

- false si Tilt > 2%
- false si Slope > 2%
- false si Width < 3.3 m
- false si FlooringMaterial = Gravel/Sand/Uneven
- false si Flooring = Hazardous
- false si StepFreeAccess=No
- true si toutes les conditions suivantes sont remplies :
	- Tilt ≤ 2%
	- Slope ≤ 2%
	- Width ≥ 3,3 m
	- FlooringMaterial != Gravel/Sand/Uneven
	- Flooring != Hazardous/Discomfortable
	- StepFreeAccess=Yes

Enfin, si aucune des conditions n'est remplie, WheelchairAccess vaudra "unknown".

**validityConditions/ValidityCondition/Description**

L’élément validityConditions est construit en suivant les règles communes.

La valeur du tag WheelchairAccess:Description est utilisée en suivant les règles communes.

En complément, certaines valeurs de ParkingVisibility peuvent venir ajouter une phrase à l'élément validityConditions/ValidityCondition/Description :

| valeur du tag ParkingVisibility | Description                                                                  |
| ------------------------------- | ---------------------------------------------------------------------------- |
| SignageOnly                     | "signalétique présente mais pas de marquage au sol"                          |
| MarkingOnly                     | "marquage au sol présent mais pas de signalétique"                           |
| Docks                           | "démarquation physique uniquement"                                           |
| autres conditions               | rien n'est ajouté                                                            |

**MobilityImpairedAccess**

L'élément AccessibilityAssessment/MobilityImpairedAccess est construit en suivant les règles communes.

### ParkingBay/PublicUse
L'élément ParkingBay/PublicUse est rempli avec la valeur fixe "disabledPublicOnly".

### ParkingBay/Lighting
L'élément ParkingBay/Lighting est rempli avec la valeur du tag Lighting avec les mêmes règles de gestion que pour les SitePathLink.

### ParkingBay/ParkingVehicleType
L'élément ParkingBay/ParkingVehicleType est rempli avec la valeur fixe "car".

### ParkingBay/BayGeometry
L'élément ParkingBay/BayGeometry est rempli avec la valeur du tag BayGeometry, avec la première lettre en minuscule.

Si la valeur finale n'est pas dans la liste suivante, alors l'élément prend la valeur "other":
- orthogonal
- angled
- parallel
- freeFormat
- unspecified

Si le tag BayGeometry n'est pas renseigné, l'élément est absent.

### ParkingBay/ParkingVisibility
L'élément ParkingBay/ParkingVisibility est rempli avec la valeur du tag ParkingVisibility, avec la première lettre en minuscule.

Cas particulier : si ParkingVisibility=MarkingOnly, la valeur à utiliser sera "demarcated"

Si la valeur finale n'est pas dans la liste suivante, alors l'lément prend la valeur "other":
- unmarked
- signageOnly
- demarcated
- docks

Si le tag n'est pas renseigné, l'élément est absent.

### ParkingBay/Length

L'élément ParkingBay/Length est rempli avec valeur du tag Length, en mètres arrondis au cm.

### ParkingBay/Width

L'élément ParkingBay/Width est rempli avec valeur du tag Width, en mètres arrondis au cm.

### ParkingBay/RechargingAvailable

L'élément ParkingBay/RechargingAvailable est rempli à partir de la valeur du tag VehicleRecharging
* true si VehicleRecharging=Yes
* false sinon

# PathJunction

Un élément Netex PathJunction est créé
* pour chaque ![node] noeud avec PathJunction=\*
* pour chaque ![node] noeud avec Obstacle=\* mais pas Amenity=\*
* pour chaque ![node] noeud qui constitue le ![node] noeud de début ou de fin d'un SitePathLink et n'a ni Amenity=* ni ParkingBay=* ni Entrance=*.

On utilise les règles de gestion suivantes :
* PathJunction/`@version` : version du ![node] noeud
* PathJunction/`@id` : de la forme Yukaimaps:PathJunction:nAA:LOC, avec nAA l'identifiant du ![node] noeud

## PathJunction/keyList[]

PathJunction/keyList[] est renseigné en suivant les règles communes à partir des éventuels tags qui n’ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

## PathJunction/Location

Si le tag NonGeographicalLocation=Yes est présent, l’élément PathJunction/Location est absent.

Sinon, la géométrie du ![node] noeud est exportée avec les règles de gestion suivantes:
* PathJunction/Location/`@id` : de la forme nAA avec nAA l'identifiant du ![node] noeud
* PathJunction/Location/Longitude : longitude du ![node] noeud
* PathJunction/Location/Latitude : latitude du ![node] noeud
* PathJunction/Location/Altitude : la valeur du tag Altitude. S'il est absent, l'élément Netex est absent.

# Autres

Si au moins un objet exporté comporte un tag WDM Ref:Import:Source=OpenStreetMap, alors on ajoute également l'objet DataSource défini comme tel:
* DataSource/`@id`: FR:Yukaimaps:DataSource:OpenStreetMap
* DataSource/`@version`: any
* DataSource/Name : OpenStreetMap
* DataSource/Description : OpenStreetMap est un ensemble de données ouvertes, disponible sous la licence libre ODbL accordée par la Fondation OpenStreetMap
* DataSource/DataLicenceCode/`@ref` : ODbL
* DataSource/DataLicenceUrl : https://wiki.osmfoundation.org/wiki/Licence

# Règles générales
## Éléments fréquents

Les éléments suivants sont présents sur plusieurs types d'objets Netex, avec des règles de gestion communes.

### keyList[]

keyList[] comprend une liste d'éléments Netex KeyValue.

Cette liste est constituée avec les éventuels tags qui n'ont pas été convertis en éléments Netex, avec le typeOfKey=WDM_tag.

Par exemple:
```xml
<keyList>
    <KeyValue typeOfKey="WDM_tag">
        <Key>FlooringMaterial</Key>
        <Value>Asphalt</Value>
    </KeyValue>
    <KeyValue typeOfKey="WDM_tag">
        <Key>Flooring</Key>
        <Value>Good</Value>
    </KeyValue>
</keyList>
```

Dans le cas où le tag Ref:Import:Id ou Ref:Export:Id a été utilisé pour définir l’identifiant de l’objet Netex, des éléments KeyValue supplémentaires sont présents pour conserver l’identifiant et la version de l’objet WDM. Le typeOfKey vaut alors “WDM_id”.

Par exemple, le chemin w3245 aura :
```xml
<KeyValue typeOfKey="WDM_id">
    <Key>Id</Key>
    <Value>w3245</Value>
</KeyValue>
<KeyValue typeOfKey="WDM_id">
    <Key>Version</Key>
    <Value>2</Value>
</KeyValue>
```

Sinon, l'élément keyList[] entier est absent.

### Centroid/Location

Si le tag NonGeographicalLocation=Yes est présent, l’élément Centroid est absent.

Sinon, la position ponctuelle de l'objet est exportée avec les règles de gestion suivantes:
* Centroid/Location/`@id` : identifiant de l'objet
* Centroid/Location/Longitude : longitude de l'objet
* Centroid/Location/Latitude : latitude de l'objet
* Centroid/Location/Altitude : la valeur du tag Altitude. S’il est absent, l’élément Netex est absent.

### gml:Polygon

Si le tag NonGeographicalLocation=Yes est présent, l’élément gml:Polygon est absent.

Sinon, la géométrie polygonale de la ![area] zone est exportée sous la forme d’un polygone en WGS 84 dans gml:Polygon/gml:exterior/gml:LinearRing/gml:pos[]. Elle est reconstituée à partir de la position des ![node] noeuds constituant la ![area] zone, dans l’ordre.

L’identifiant de l'objet est repris dans gml:Polygon/`@gml:id`.

```xml
<gml:Polygon gml:id="w113530">
    <gml:exterior>
        <gml:LinearRing>
        <gml:pos>2.07707394642 48.81620913074</gml:pos>
        <gml:pos>2.07860502458 48.81616760389</gml:pos>
        <gml:pos>2.07519250712 48.81606840073</gml:pos>
        <gml:pos>2.07707394642 48.81620913074</gml:pos>
        </gml:LinearRing>
    </gml:exterior>
</gml:Polygon>
```

### AccessibilityAssessment

AccessibilityAssessment est rempli avec les règles de gestion suivantes :
* AccessibilityAssessment/`@id` : de la forme “Yukaimaps:AccessibilityAssessment:xxx:”" avec xxx une valeur autoincrémentée sur tout l’export
* AccessibilityAssessment/`@version` : valeur fixe “any”

**validityConditions/ValidityCondition/Description**

Un élément AccessibilityAssessment/validityConditions peut être présent.

Il comprend alors un unique ValidityCondition, dont l’identifiant est autoincrémenté sur tout l'export et la version vaut “any”. L'élément
AccessibilityAssessment/validityConditions/ValidityCondition/Description est construit en concaténant un certain nombre de phrases, séparées par " - ".

Ces phrases sont constituées de la valeur des tags :
- WheelchairAccess:Description
- AudibleSignals:Description
- VisualSigns:Description

Si les trois tags sont absents, et qu’un des tags WheelchairAccess, AudibleSignals ou VisualSigns a pour valeur “Limited”, alors on ajoutera la phrase “Accessibilité partielle non détaillée”.

D'autres phrases peuvent venir s'ajouter dans certains cas particuliers.

Sinon, l’élément validityConditions et tout son contenu sont absents.

**MobilityImpairedAccess**

AccessibilityAssessment/MobilityImpairedAccess est rempli à partir des limitations/AccessibilityLimitation :
- unknown si certains éléments de AccessibilityLimitation sont à unknown
- false si tous les éléments de AccessibilityLimitation sont à false
- true si tous les éléments de AccessibilityLimitation sont à true
- partial sinon

**limitations/AccessibilityLimitation**

Puis, un élément AccessibilityAssessment/limitations est présent.

Il comprend alors un unique AccessibilityLimitation, dont l’identifiant est autoincrémenté sur tout l'export et la version vaut “any”.

Si présent, l'élément limitations/AccessibilityLimitation/WheelchairAccess est rempli à partir du tag WheelchairAccess:
* true si WheelchairAccess=Yes
* false si WheelchairAccess=No
* partial si WheelchairAccess=Limited
* unknown sinon

Puis, les éléments suivants peuvent être présents, avec des règles de gestion dépendant de l'objet Netex :
- limitations/AccessibilityLimitation/StepFreeAccess
- limitations/AccessibilityLimitation/EscalatorFreeAccess
- limitations/AccessibilityLimitation/LiftFreeAccess

Si présent, l'élément limitations/AccessibilityLimitation/AudibleSignalsAvailable est rempli à partir du tag AudibleSignals:
* true si AudibleSignals=Yes
* false si AudibleSignals=No
* partial si AudibleSignals=Limited
* unknown sinon

Si présent, l'élément limitations/AccessibilityLimitation/VisualSignsAvailable est rempli à partir du tag VisualSigns:
* true si VisualSigns=Yes
* false si VisualSigns=No
* partial si VisualSigns=Limited
* unknown sinon

### facilities

L'élément facilities comprend toujours un unique élément SiteFacilitySet de la forme :
* SiteFacilitySet/`@id` : de la forme “Yukaimaps:SiteFacilitySet:xxx:”" avec xxx une valeur autoincrémentée sur tout l’export
* SiteFacilitySet/`@version` : valeur fixe “any”

Puis, le SiteFacilitySet peut contenir les éléments suivants :
* AccessibilityInfoFacilityList
* AssistanceFacilityList
* AccessibilityToolList
* FamilyFacilityList
* MedicalFacilityList
* MobilityFacilityList
* PassengerCommsFacilityList
* PassengerInformationEquipmentList
* SanitaryFacilityList
* TicketingFacilityList
* AccessFacilityList
* EmergencyServiceList
* LuggageLockerFacilityList
* LuggageServiceFacilityList
* ParkingFacilityList
* Staffing

Chacun des éléments enfant de SiteFacilitySet contient une liste de valeurs. Si aucune valeur ne peut être remplie, l'élément est absent.

S'il n'y a aucun élément enfant rempli, les éléments SiteFacilitySet et facilities ne sont pas exportés.

Les valeurs suivantes peuvent être présentes dans l'élément AccessibilityInfoFacilityList :
* audioInformation si AudioInformation=Yes/Bad ou si Audiblesignals=Yes
* audioForHearingImpaired si AudioInformation=Yes ou s'il y a au moins un Amenity=ReceptionDesk/TicketOffice avec MagneticInductionLoop=Yes à l'intérieur de l'objet
* visualDisplays si VisualDisplays=Yes/Bad
* displaysForVisuallyImpaired si VisualDisplays=Yes
* largePrintTimetables si LargePrintTimetable=Yes

Les valeurs suivantes peuvent être présentes dans l'élément AssistanceFacilityList :
* boardingAssistance si le tag Assistance comprend la valeur Boarding
* wheelchairAssistance si le tag Assistance comprend la valeur Wheelchair
* unaccompaniedMinorAssistance si le tag Assistance comprend la valeur UnaccompaniedMinor
* conductor si le tag Assistance comprend la valeur Conductor
* information si le tag Assistance comprend la valeur Information ou si un Amenity=ReceptionDesk est présent à l'intérieur de l'objet concerné
* none si Assistance=None et si aucun Amenity=ReceptionDesk n'est présent à l'intérieur de l'objet concerné

Les valeurs suivantes peuvent être présentes dans l'élément AccessibilityToolList :
* wheelchair si le tag AccessibilityToolLending comprend la valeur Wheelchair
* walkingstick si le tag AccessibilityToolLending comprend la valeur WalkingStick
* audioNavigator si le tag AccessibilityToolLending comprend la valeur AudioNavigator
* visualNavigator si le tag AccessibilityToolLending comprend la valeur VisualNavigator
* passengerCart si le tag AccessibilityToolLending comprend la valeur PassengerCart ou si un Amenity=TrolleyStand est présent à l'intérieur de l'objet concerné
* pushchair si le tag AccessibilityToolLending comprend la valeur Pushchair
* umbrella si le tag AccessibilityToolLending comprend la valeur Umbrella

Les valeurs suivantes peuvent être présentes dans l'élément FamilyFacilityList :
* none si FamilyServices=None
* servicesForChildren si le tag FamilyServices comprend la valeur Children
* nurseryService si le tag FamilyServices comprend la valeur Nursery

Les valeurs suivantes peuvent être présentes dans l'élément MedicalFacilityList :
* defibrillator si un Amenity=Defibrillator est présent à l'intérieur de l'objet concerné

Les valeurs suivantes peuvent être présentes dans l'élément MobilityFacilityList :
* stepFreeAccess s'il n'y a ni SitePathLink=Stairs, ni Obstable=Kerb ni objet avec KerbHeight=* à l'intérieur de l'objet concerné
* suitableForWheelchairs si WheelchairAccess=Yes
* boardingAssistance si le tag Assistance comprend la valeur Boarding
* unaccompaniedMinorAssistance si le tag Assistance comprend la valeur UnaccompaniedMinor
* tactilePlatformEdges si TactileWarningStrip!=None sur tous les quais à l'intérieur de l'objet
* tactileGuidingStrips s'il y a au moins un SitePathLink avec TactileGuidingStrip=Yes à l'intérieur de l'objet
* unknown si aucune des conditions ci-dessus n'est remplie

Les valeurs suivantes peuvent être présentes dans l'élément PassengerCommsFacilityList :
* freeWifi si WiFi=Free
* publicWifi si WiFi!=No
* internet si WiFi!=No
* postBox si un Amenity=PostBox est présent à l'intérieur de l'objet concerné

Les valeurs suivantes peuvent être présentes dans l'élément PassengerInformationEquipmentList :
* informationDesk si un Amenity=ReceptionDesk/TicketOffice est présent à l'intérieur de l'objet concerné
* realTimeDepartures si RealTimeDepartures=Yes

Les valeurs suivantes peuvent être présentes dans l'élément SanitaryFacilityList :
* none si Toilets=No et qu'aucun Amenity=Toilets n'est présent à l'intérieur de l'objet concerné
* toilet si Toilets=Yes/Bad ou si un Amenity=Toilets est présent à l'intérieur de l'objet concerné
* wheelchairAccessToilet si Toilets=Yes ou si au moins un Amenity=Toilets avec WheelchairAccess=Yes est présent à l'intérieur de l'objet concerné
* shower si Toilets:Shower=Yes ou si au moins un Amenity=Toilets avec Toilets:Shower=Yes est présent à l'intérieur de l'objet concerné
* washingAndChangeFacilities si Toilets:Shower=Yes ou si au moins un Amenity=Toilets avec Toilets:Shower=Yes est présent à l'intérieur de l'objet concerné
* babyChange si Toilets:BabyChange=Yes/Bad ou si au moins un Amenity=Toilets avec Toilets:BabyChange=Yes/Bad est présent à l'intérieur de l'objet concerné
* wheelchairBabyChange si Toilets:BabyChange=Yes ou si au moins un Amenity=Toilets avec Toilets:BabyChange=Yes est présent à l'intérieur de l'objet concerné

Les valeurs suivantes peuvent être présentes dans l'élément TicketingFacilityList :
* ticketMachines si le tag Tickets comprend la valeur VendingMachine ou si un Amenity=TicketVendingMachine est présent à l'intérieur de l'objet concerné
* ticketOffice si le tag Tickets comprend la valeur Office ou si un Amenity=TicketOffice est présent à l'intérieur de l'objet concerné
* ticketOnDemandMachines si le tag Tickets comprend la valeur OnDemandMachines
* mobileTicketing si le tag Tickets comprend la valeur MobileTicketing

Les valeurs suivantes peuvent être présentes dans l'élément AccessFacilityList :
* lift si un SitePathLink=Elevator est présent à l'intérieur de l'objet concerné
* wheelchairLift si un SitePathLink=Elevator avec WheelchairAccess=Yes est présent à l'intérieur de l'objet concerné
* escalator si un SitePathLink=Escalator est présent à l'intérieur de l'objet concerné
* travelator si un SitePathLink=Travelator est présent à l'intérieur de l'objet concerné
* ramp si un SitePathLink=Ramp est présent à l'intérieur de l'objet concerné
* steps si un Obstable=Kerb ou un objet avec KerbHeight=* est présent à l'intérieur de l'objet concerné
* stairs si un SitePathLink=Stairs est présent à l'intérieur de l'objet concerné
* validator si un Amenity=TicketValidator est présent à l'intérieur de l'objet concerné

Les valeurs suivantes peuvent être présentes dans l'élément EmergencyServiceList :
* police si le tag Emergency comprend la valeur Police
* fire si le tag Emergency comprend la valeur Fire
* firstAid si le tag Emergency comprend la valeur FirstAid
* sosPoint si le tag Emergency comprend la valeur SOSPoint ou si un Amenity=EmergencyPhone est présent à l'intérieur de l'objet concerné

Les valeurs suivantes peuvent être présentes dans l'élément LuggageLockerFacilityList :
* lockers si un Amenity=LuggageLocker est présent à l'intérieur de l'objet concerné

Les valeurs suivantes peuvent être présentes dans l'élément LuggageServiceFacilityList :
* freeTrolleys si un Amenity=TrolleyStand avec FreeToUse=Yes est présent à l'intérieur de l'objet concerné
* paidTrolleys si un Amenity=TrolleyStand avec FreeToUse=No est présent à l'intérieur de l'objet concerné
* other si un Amenity=TrolleyStand sans tag FreeToUse est présent à l'intérieur de l'objet concerné

Les valeurs suivantes peuvent être présentes dans l'élément ParkingFacilityList :
* carPark si NearbyCarPark=Yes
* cyclePark si NearbyCyclePark=Yes

Les valeurs suivantes peuvent être présentes dans l'élément Staffing :
* unmanned si Staffing=No ou si un Amenity=ReceptionDesk/TicketOffice avec Staffing=No est présent à l'intérieur de l'objet concerné
* partTime si Staffing=PartTime ou si un Amenity=ReceptionDesk/TicketOffice avec Staffing=PartTime est présent à l'intérieur de l'objet concerné
* fullTime si Staffing=FullTime ou si un Amenity=ReceptionDesk/TicketOffice avec Staffing=FullTime est présent à l'intérieur de l'objet concerné

### equipmentPlaces[]

equipmentPlaces[] contient une liste de références vers des équipements ainsi que leurs positions respectives.

equipmentPlaces[] comprend des éléments EquipmentPlace avec :
* equipmentPlaces/EquipmentPlace/`@id` : de la forme `Yukaimaps:EquipmentPlace:AA` avec AA auto-incrémenté sur tout l'export
* equipmentPlaces/EquipmentPlace/@id/`@version` : valeur fixe "any"

Chaque EquipmentPlace contient un élément equipmentPositions qui comprend toujours un unique EquipmentPosition avec
* EquipmentPlace/equipmentPositions/EquipmentPosition/`@id` : de la forme `Yukaimaps:EquipmentPosition:XXX:LOC` avec XX l'identifiant de l'équipement (nAA ou WAA_nBB_nCC)
* EquipmentPlace/equipmentPositions/EquipmentPosition/`@version` : valeur fixe "any"

L'élément EquipmentPosition contient :
- un élément EquipmentRef de la forme `<EquipmentRef ref="Yukaimaps:RampEquipment:w45561_n2_n502:LOC" version="12"/>`
- un élément Location qui reprend la position de l'équipement si c'est ![node] ou son barycentre sinon. Si l'équipement a un tag NonGeographicalLocation=Yes, l'élément Location n'est pas présent

Exemple
```xml
<equipmentPlaces>
    <EquipmentPlace id="Yukaimaps:EquipmentPlace:1" version="any">
        <equipmentPositions>
            <EquipmentPosition id="Yukaimaps:EquipmentPosition:n45561:LOC" version="any">
                <EquipmentRef ref="Yukaimaps:SanitaryEquipment:n45561:LOC" version="12"/>
                <Location>
                    <Longitude>0.00040037734</Longitude>
                    <Latitude>0.00063481563</Latitude>
                </Location>
            </EquipmentPosition>
        </equipmentPositions>
    </EquipmentPlace>
    <EquipmentPlace id="Yukaimaps:EquipmentPlace:2" version="any">
        <equipmentPositions>
            <EquipmentPosition id="Yukaimaps:EquipmentPosition:w45561_n2_n502:LOC" version="any">
                <EquipmentRef ref="Yukaimaps:ShelterEquipment:n502:LOC" version="12"/>
            </EquipmentPosition>
        </equipmentPositions>
    </EquipmentPlace>
</equipmentPlaces>
```





[node]: https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png
[way]: https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_way.svg/20px-Osm_element_way.svg.png
[area]: https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_area.svg/20px-Osm_element_area.svg.png
[relation]:https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_relation.svg/20px-Osm_element_relation.svg.png
