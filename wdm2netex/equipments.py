#
# Equipments transformers
#

from lxml import etree
from lxml.builder import E
from . import mappings, model, properties


def crossingEquipmentToXml(wds, spl):
	"""Transforms a single crossing equipment into Netex"""

	# Determine Netex ID
	ceqId = None
	startNode = None
	endNode = None

	if isinstance(spl, model.Node):
		ceqId = f"Yukaimaps:CrossingEquipment:n{spl.attrs['id']}:LOC"

	elif isinstance(spl, model.Way):
		startNodeId = spl.nodes[0]
		startNode = wds.nodes[startNodeId]
		endNodeId = spl.nodes[len(spl.nodes)-1]
		endNode = wds.nodes[endNodeId]
		ceqId = f"Yukaimaps:CrossingEquipment:w{spl.attrs['id']}_n{startNodeId}_n{endNodeId}:LOC"

	# Create equipment
	ceq = E.CrossingEquipment(
		id=ceqId,
		version=spl.attrs.get("version") or "any"
	)

	properties.appendKeyList(ceq, spl)

	#
	# Tags
	#

	properties.appendPositiveFloat(ceq, spl, "Width", "Width", 2)
	ceq.append(E.DirectionOfUse("both"))

	# CrossingType
	crossing = spl.tags.get("Crossing")
	barrier = spl.tags.get("CrossingBarrier")
	crossingIsland = spl.tags.get("CrossingIsland")
	ceqCrossingType = None

	if crossing == "Rail" and barrier in [None, "Yes"]:
		ceqCrossingType = "levelCrossing"
	elif crossing == "UrbanRail" and barrier == "Yes":
		ceqCrossingType = "levelCrossing"
	elif crossing == "Rail" and barrier == "No":
		ceqCrossingType = "barrowCrossing"
	elif crossing == "UrbanRail" and barrier in [None, "No"]:
		ceqCrossingType = "barrowCrossing"
	elif crossing in ["Road", "CycleWay"] and crossingIsland in ["No", None]:
		ceqCrossingType = "roadCrossing"
	elif crossing in ["Road", "CycleWay"] and crossingIsland == "Yes":
		ceqCrossingType = "roadCrossingWithIsland"
	elif crossing is not None and crossing not in ["Rail", "UrbanRail", "Road", "CycleWay"]:
		ceqCrossingType = "other"

	if ceqCrossingType is not None:
		ceq.append(E.CrossingType(ceqCrossingType))


	properties.appendDirectMappings(ceq, spl, [
		{ "wdm": "ZebraCrossing", "netex": "ZebraCrossing", "mapping": mappings.MAPPING_STATUS_TRUEFALSE },
		{ "wdm": "PedestrianLights", "netex": "PedestrianLights", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "AcousticCrossingAids", "netex": "AcousticCrossingAids", "mapping": mappings.MAPPING_STATUS_TRUEFALSE },
		{ "wdm": "TactileGuidingStrip", "netex": "TactileGuidanceStrips", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
	])

	# TactileWarningStrip
	if startNode is not None and endNode is not None:
		startTWS = startNode.tags.get("TactileWarningStrip")
		endTWS = endNode.tags.get("TactileWarningStrip")
		tws = None

		if startTWS in [None, "None"] and endTWS in [None, "None"]:
			tws = "noTactileStrip"
		elif startTWS not in [None, "None"] and endTWS not in [None, "None"]:
			tws = "tactileStripAtBothEnds"
		elif startTWS not in [None, "None"] and endTWS in [None, "None"]:
			tws = "tactileStripAtBeginning"
		elif startTWS in [None, "None"] and endTWS not in [None, "None"]:
			tws = "tactileStripAtEnd"

		if tws is not None:
			ceq.append(E.TactileWarningStrip(tws))

	properties.appendDirectMappings(ceq, spl, [
		{ "wdm": "VisualGuidanceBands", "netex": "VisualGuidanceBands", "mapping": mappings.MAPPING_YESNO_TRUEFALSE }
	])

	# DroppedKerb
	dk = "false"

	if startNode is not None and endNode is not None:
		startKH = startNode.tags.get("KerbHeight")
		endKH = endNode.tags.get("KerbHeight")
		startKCW = startNode.tags.get("KerbCut:Width")
		endKCW = endNode.tags.get("KerbCut:Width")
		startKD = startNode.tags.get("KerbDesign")
		endKD = endNode.tags.get("KerbDesign")

		if startKH is not None and endKH is not None:
			try:
				startKH = float(startKH) <= 0.02
				endKH = float(endKH) <= 0.02
				if startKH and endKH:
					dk = "true"
			except ValueError:
				pass

		if properties.isValidPositiveFloat(startKCW) and properties.isValidPositiveFloat(endKCW):
			dk = "true"
		
		kerbDesignValid = ["Lowered", "KerbCut", "Flush", "None"]
		if startKD in kerbDesignValid and endKD in kerbDesignValid:
			dk = "true"

	ceq.append(E.DroppedKerb(dk))

	# MarkingStatus, BumpCrossing
	properties.appendDirectMappings(ceq, spl, [
		{ "wdm": "ZebraCrossing", "netex": "MarkingStatus", "mapping": {
			"Good": "good", "Worn": "worn", "Hazardous": "hazardous",
			"None": "none", "*": "unknown", "": "unknown", "Discomfortable": "worn",
		} },
		{ "wdm": "VibratingCrossingAids", "netex": "VibratingCrossingAids", "mapping": {
			"Good": "true", "Worn": "true", "Discomfortable": "true",
			"Hazardous": "true", "None": "false",
		} },
		{ "wdm": "BumpCrossing", "netex": "BumpCrossing", "mapping": mappings.MAPPING_YESNO_TRUEFALSE }
	])

	# VisualObstacle
	if startNode is not None and endNode is not None:
		vo = None
		startVO = startNode.tags.get("VisualObstacle")
		endVO = endNode.tags.get("VisualObstacle")

		if startVO == endVO:
			if startVO == "None":
				vo = "none"
			elif startVO == "CarParking":
				vo = "carParking"
			elif startVO == "Vegetation":
				vo = "vegetation"
			elif startVO == "Building":
				vo = "building"
			elif startVO == "StreetFurniture":
				vo = "streetFurniture"
		elif startVO not in [None, "None"] or endVO not in [None, "None"]:
			vo = "other"

		if vo is not None:
			ceq.append(E.VisualObstacle(vo))

	return ceq


def entranceEquipmentToXml(wds, e):
	"""Transforms a single entrance equipment into Netex"""

	# Create equipment
	eq = E.EntranceEquipment(
		id=f"Yukaimaps:EntranceEquipment:n{e.attrs['id']}:LOC",
		version=e.attrs.get("version") or "any"
	)

	# Width, Door, DoorHandleOutside, DoorHandleInside, RevolvingDoor, Barrier
	properties.appendPositiveFloat(eq, e, "Width", "Width", 2)
	properties.appendDirectMappings(eq, e, [
		{ "wdm": "EntrancePassage", "netex": "Door", "mapping": {"Door": "true", "Opening": "false"} },
		{ "wdm": "DoorHandle:Outside", "netex": "DoorHandleOutside", "mapping": mappings.MAPPING_DOORHANDLE },
		{ "wdm": "DoorHandle:Inside", "netex": "DoorHandleInside", "mapping": mappings.MAPPING_DOORHANDLE },
		{ "wdm": "Door", "netex": "RevolvingDoor", "mapping": {"Revolving": "true", "*": "false", "": ""} },
		{ "wdm": "EntrancePassage", "netex": "Barrier", "mapping": {"Barrier": "true", "Opening": "false"} },
	])

	# DropKerbOutside
	kh = e.tags.get("KerbHeight")
	kcw = e.tags.get("KerbCut:Width")
	dko = "false"

	if properties.isValidPositiveFloat(kh, allowZero=True) and float(kh) <= 0.02:
		dko = "true"
	elif properties.isValidPositiveFloat(kcw):
		dko = "true"
	
	eq.append(E.DropKerbOutside(dko))

	# AutomaticDoor, GlassDoor, WheelchairPassable, AudioOrVideoIntercom
	# EntranceAttention, DoorstepMark
	properties.appendDirectMappings(eq, e, [
		{ "wdm": "AutomaticDoor", "netex": "AutomaticDoor", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "GlassDoor", "netex": "GlassDoor", "mapping": mappings.MAPPING_YESBADNO_TRUEFALSE },
		{ "wdm": "WheelchairAccess", "netex": "WheelchairPassable", "mapping": mappings.MAPPING_YESNONE_TRUEFALSE },
		{ "wdm": "EntranceAttention", "netex": "AudioOrVideoIntercom", "mapping": {"VideoIntercom": "true", "*": "false", "": ""} },
		{ "wdm": "EntranceAttention", "netex": "EntranceAttention", "mapping": {
			"None": "none", "Other": "other", "Doorbell": "doorbell",
			"Intercom": "intercom", "VideoIntercom": "intercom"
		} },
		{ "wdm": "TactileWarningStrip", "netex": "DoorstepMark", "mapping": {"None": "false", "*": "true", "": ""} },
	])

	# NecessaryForceToOpen
	nfto = properties.getIntegerTag(e.tags, "NecessaryForceToOpen")
	nftoOut = None

	if nfto is not None:
		if nfto > 50:
			nftoOut = "heavyForce"
		elif nfto >= 20:
			nftoOut = "mediumForce"
		elif nfto > 0:
			nftoOut = "lightForce"
		elif nfto == 0:
			nftoOut = "noForce"
	elif e.tags.get("Door") == "None" or e.tags.get("EntrancePassage") == "Opening" or e.tags.get("AutomaticDoor") == "Yes":
		nftoOut = "noForce"
	
	eq.append(E.NecessaryForceToOpen(nftoOut or "unknown"))

	# RampDoorbell, Recognizable, TurningSpacePosition
	properties.appendDirectMappings(eq, e, [
		{ "wdm": "RampDoorbell", "netex": "RampDoorbell", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "Recognizable", "netex": "Recognizable", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "TurningSpacePosition", "netex": "TurningSpacePosition", "mapping": {
			"None": "none", "Inside": "inside", "Outside": "outside", "InsideAndOutside": "insideAndOutside"
		} },
	])

	# WheelchairTurningCircle
	otsw = properties.getFloatTag(e.tags, "OutsideTurningSpace:Width")
	otsl = properties.getFloatTag(e.tags, "OutsideTurningSpace:Length")
	itsw = properties.getFloatTag(e.tags, "InsideTurningSpace:Width")
	itsl = properties.getFloatTag(e.tags, "InsideTurningSpace:Length")
	wtc = [ v for v in [otsw, otsl, itsw, itsl] if v is not None ]
	if len(wtc) > 0:
		eq.append(E.WheelchairTurningCircle("%.2f" % min(wtc)))

	return eq


def rampEquipmentToXml(wds, splId):
	"""Transforms a single ramp equipment into Netex"""

	spl = wds.sitePathLinks[splId]

	# Create equipment
	eq = E.RampEquipment(
		id=f"Yukaimaps:RampEquipment:w{spl.attrs['id']}_n{spl.nodes[0]}_n{spl.nodes[len(spl.nodes)-1]}:LOC",
		version=spl.attrs.get("version") or "any"
	)

	properties.appendKeyList(eq, spl)
	properties.appendPositiveFloat(eq, spl, "Width", "Width", 2)

	# Length
	prevCoords = None
	cumulatedDist = 0
	for c in wds.getNodesCoordinates(spl.nodes):
		if prevCoords is not None:
			cumulatedDist += properties.distance(prevCoords, c)
		prevCoords = c
	eq.append(E.Length("%.2f" % cumulatedDist))

	properties.appendDirectMappings(eq, spl, [
		{ "wdm": "MaxWeight", "netex": "MaximumLoad" }
	])
	properties.appendGradient(eq, spl.tags.get("Slope"))
	properties.appendDirectMappings(eq, spl, [
		{ "wdm": "Handrail", "netex": "HandrailType", "mapping": mappings.MAPPING_RLCBN_SIDES },
		{ "wdm": "TactileGuidingStrip", "netex": "TactileGuidanceStrips", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "VisualGuidanceBands", "netex": "VisualGuidanceBands", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
	])
	eq.append(E.Temporary("false"))
	properties.appendDirectMappings(eq, spl, [
		{ "wdm": "RestStopDistance", "netex": "RestStopDistance" },
		{ "wdm": "SafetyEdge", "netex": "SafetyEdge", "mapping": mappings.MAPPING_RLCBN_SIDES },
		{ "wdm": "TurningSpacePosition", "netex": "TurningSpace", "mapping": {
			"None": "none", "Bottom": "bottom", "Top": "top", "TopAndBottom": "topAndBottom"
		}}
	])

	return eq


def escalatorEquipmentToXml(wds, splId):
	"""Transforms a single escalator equipment into Netex"""

	spl = wds.sitePathLinks[splId]

	# Create equipment
	eq = E.EscalatorEquipment(
		id=f"Yukaimaps:EscalatorEquipment:w{spl.attrs['id']}_n{spl.nodes[0]}_n{spl.nodes[len(spl.nodes)-1]}:LOC",
		version=spl.attrs.get("version") or "any"
	)

	properties.appendKeyList(eq, spl)
	properties.appendDirectMappings(eq, spl, [{ "wdm": "PublicCode", "netex": "PublicCode" }])
	properties.appendPositiveFloat(eq, spl, "Width", "Width", 2)

	# DirectionOfUse
	incline = spl.tags.get("Incline")
	conveying = spl.tags.get("Conveying")
	dirOfUse = None

	if conveying == "Reversible":
		dirOfUse = "both"
	elif incline == "Up" and conveying == "Forward":
		dirOfUse = "up"
	elif incline == "Up" and conveying == "Backward":
		dirOfUse = "down"
	elif incline == "Down" and conveying == "Forward":
		dirOfUse = "down"
	elif incline == "Down" and conveying == "Backward":
		dirOfUse = "up"

	if dirOfUse is not None:
		eq.append(E.DirectionOfUse(dirOfUse))

	properties.appendDirectMappings(eq, spl, [
		{ "wdm": "StoppedIfUnused", "netex": "TactileActuators", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "StoppedIfUnused", "netex": "EnergySaving", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "MonitoringRemoteControl", "netex": "MonitoringRemoteControl", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
	])

	return eq


def travelatorEquipmentToXml(wds, splId):
	"""Transforms a single travelator equipment into Netex"""

	spl = wds.sitePathLinks[splId]

	# Create equipment
	eq = E.TravelatorEquipment(
		id=f"Yukaimaps:TravelatorEquipment:w{spl.attrs['id']}_n{spl.nodes[0]}_n{spl.nodes[len(spl.nodes)-1]}:LOC",
		version=spl.attrs.get("version") or "any"
	)

	properties.appendKeyList(eq, spl)
	properties.appendDirectMappings(eq, spl, [{ "wdm": "PublicCode", "netex": "PublicCode" }])
	properties.appendPositiveFloat(eq, spl, "Width", "Width", 2)
	properties.appendDirectMappings(eq, spl, [
		{ "wdm": "Conveying", "netex": "DirectionOfUse", "mapping": {
			"Reversible": "both", "Forward": "up", "Backward": "up"
		}},
		{ "wdm": "StoppedIfUnused", "netex": "TactileActuators", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "StoppedIfUnused", "netex": "EnergySaving", "mapping": mappings.MAPPING_YESNO_TRUEFALSE }
	])

	# Length
	prevCoords = None
	cumulatedDist = 0
	for c in wds.getNodesCoordinates(spl.nodes):
		if prevCoords is not None:
			cumulatedDist += properties.distance(prevCoords, c)
		prevCoords = c
	eq.append(E.Length("%.2f" % cumulatedDist))

	properties.appendGradient(eq, spl.tags.get("Slope"))

	return eq


def liftEquipmentToXml(wds, spl):
	"""Transforms a single lift equipment into Netex"""

	# Create equipment
	eqId = None
	if isinstance(spl, model.Node):
		eqId = f"Yukaimaps:LiftEquipment:n{spl.attrs['id']}:LOC"
	else:
		eqId = f"Yukaimaps:LiftEquipment:w{spl.attrs['id']}_n{spl.nodes[0]}_n{spl.nodes[len(spl.nodes)-1]}:LOC"

	eq = E.LiftEquipment(
		id=eqId,
		version=spl.attrs.get("version") or "any"
	)

	properties.appendKeyList(eq, spl)
	properties.appendDirectMappings(eq, spl, [{ "wdm": "PublicCode", "netex": "PublicCode" }])
	properties.appendPositiveFloat(eq, spl, "Depth", "Depth", 2)
	properties.appendDirectMappings(eq, spl, [
		{ "wdm": "MaxWeight", "netex": "MaximumLoad" },
		{ "wdm": "WheelchairAccess", "netex": "WheelchairPassable", "mapping": mappings.MAPPING_YESNONE_TRUEFALSE },
	])
	properties.appendPositiveFloat(eq, spl, "ManoeuvringDiameter", "WheelchairTurningCircle", 2)
	properties.appendPositiveFloat(eq, spl, "InternalWidth", "InternalWidth", 2)
	properties.appendDirectMappings(eq, spl, [
		{ "wdm": "Handrail", "netex": "HandrailType", "mapping": mappings.MAPPING_RLCBN_SIDES },
		{ "wdm": "HandrailHeight", "netex": "HandrailHeight" },
		{ "wdm": "RaisedButtons", "netex": "RaisedButtons", "mapping": mappings.MAPPING_NONESTAR_FALSETRUE },
		{ "wdm": "RaisedButtons", "netex": "BrailleButtons", "mapping": { "Braille": "true", "*": "false" } },
		{ "wdm": "MirrorOnOppositeSide", "netex": "MirrorOnOppositeSide", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "Attendant", "netex": "Attendant", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "AutomaticDoor", "netex": "Automatic", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "AlarmButton", "netex": "AlarmButton", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "AudioAnnouncements", "netex": "AudioAnnouncements", "mapping": mappings.MAPPING_YESNONE_TRUEFALSE }
	])

	# ReachedFloorAnnouncement
	aa = spl.tags.get("AudioAnnouncements")
	vd = spl.tags.get("VisualDisplays")
	rfa = None
	if aa == "No" and vd == "No":
		rfa = "none"
	elif vd == "Yes" and aa in ["No", None]:
		rfa = "visual"
	elif aa == "Yes" and vd in ["No", None]:
		rfa = "audio"
	elif aa == "Yes" and vd == "Yes":
		rfa = "visualAndAudio"
	if rfa:
		eq.append(E.ReachedFloorAnnouncement(rfa))

	properties.appendDirectMappings(eq, spl, [
		{ "wdm": "MagneticInductionLoop", "netex": "MagneticInductionLoop", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
	])

	return eq


def queueingEquipmentToXml(wds, splId):
	"""Transforms a single queueing equipment into Netex"""

	spl = wds.sitePathLinks[splId]

	# Create equipment
	eq = E.QueueingEquipment(
		id=f"Yukaimaps:QueueingEquipment:w{spl.attrs['id']}_n{spl.nodes[0]}_n{spl.nodes[len(spl.nodes)-1]}:LOC",
		version=spl.attrs.get("version") or "any"
	)

	properties.appendKeyList(eq, spl)
	properties.appendPositiveFloat(eq, spl, "Width", "Width", 2)

	return eq


def staircaseEquipmentToXml(wds, wdmFeature):
	"""Transforms a single staircase equipment into Netex"""

	isKerb = wdmFeature.tags.get("Obstacle") == "Kerb"

	# Create equipment
	eqId = None
	if isKerb:
		eqId = f"Yukaimaps:StaircaseEquipment:n{wdmFeature.attrs['id']}:LOC"
	else:
		eqId = f"Yukaimaps:StaircaseEquipment:w{wdmFeature.attrs['id']}_n{wdmFeature.nodes[0]}_n{wdmFeature.nodes[len(wdmFeature.nodes)-1]}:LOC"

	eq = E.StaircaseEquipment(
		id=eqId,
		version=wdmFeature.attrs.get("version") or "any"
	)

	# Width, DirectionOfUse
	properties.appendPositiveFloat(eq, wdmFeature, "Width", "Width", 2)
	eq.append(E.DirectionOfUse("both"))

	# NumberofSteps
	if isKerb:
		eq.append(E.NumberOfSteps("1"))
	else:
		properties.appendPositiveInteger(eq, wdmFeature, "StepCount", "NumberOfSteps")

	# StepHeight
	if isKerb:
		if wdmFeature.tags.get("StepHeight") is not None:
			properties.appendPositiveFloat(eq, wdmFeature, "StepHeight", "StepHeight", 2)
		elif wdmFeature.tags.get("Height") is not None:
			properties.appendPositiveFloat(eq, wdmFeature, "Height", "StepHeight", 2)
		elif wdmFeature.tags.get("KerbHeight") is not None:
			properties.appendPositiveFloat(eq, wdmFeature, "KerbHeight", "StepHeight", 2)
	else:
		properties.appendPositiveFloat(eq, wdmFeature, "StepHeight", "StepHeight", 2)

	# StepLength, StepColourContrast, Handrail
	properties.appendPositiveFloat(eq, wdmFeature, "StepLength", "StepLength", 2)
	properties.appendDirectMappings(eq, wdmFeature, [
		{ "wdm": "StepColourContrast", "netex": "StepColourContrast", "mapping": mappings.MAPPING_NONESTAR_FALSETRUE },
		{ "wdm": "StepCondition", "netex": "StepCondition", "mapping": {"Even": "even", "Uneven": "uneven", "Rough": "rough"}},
		{ "wdm": "Handrail", "netex": "HandrailType", "mapping": mappings.MAPPING_RLCBN_SIDES },
		{ "wdm": "Handrail:TactileWriting", "netex": "TactileWriting", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "StairRamp", "netex": "StairRamp", "mapping": {"None": "none", "Bicycle": "bicycle", "Luggage": "luggage", "Stroller": "stroller", "*": "other"}},
	])

	# TopEnd, BottomEnd
	if isKerb:
		tws = wdmFeature.tags.get("TactileWarningStrip")
		eq.append(E.TopEnd(E.TexturedSurface(
			"true" if tws is not None and tws != "None" else "false"
		)))
	else:
		# Determine top and bottom end
		firstNode = wds.nodes.get(wdmFeature.nodes[0])
		lastNode = wds.nodes.get(wdmFeature.nodes[-1])
		if firstNode is not None and lastNode is not None:
			incline = wdmFeature.tags.get("Incline") or "Up"
			bottomNode = firstNode if incline == "Up" else lastNode
			topNode = lastNode if incline == "Up" else firstNode

			topEnd = E.TopEnd()
			properties.appendDirectMappings(topEnd, topNode, [
				{ "wdm": "ContinuingHandrail", "netex": "ContinuingHandrail", "mapping": mappings.MAPPING_SIDES_TRUEFALSE },
				{ "wdm": "TactileWarningStrip", "netex": "TexturedSurface", "mapping": mappings.MAPPING_NONESTAREMPTY_FALSETRUE },
				{ "wdm": "VisualContrast", "netex": "VisualContrast", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
			])
			eq.append(topEnd)

			bottomEnd = E.BottomEnd()
			properties.appendDirectMappings(bottomEnd, bottomNode, [
				{ "wdm": "ContinuingHandrail", "netex": "ContinuingHandrail", "mapping": mappings.MAPPING_SIDES_TRUEFALSE },
				{ "wdm": "TactileWarningStrip", "netex": "TexturedSurface", "mapping": mappings.MAPPING_NONESTAR_FALSETRUE },
				{ "wdm": "VisualContrast", "netex": "VisualContrast", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
			])
			if len(bottomEnd) > 0:
				eq.append(bottomEnd)

	# ContinuousHandrail
	properties.appendDirectMappings(eq, wdmFeature, [
		{ "wdm": "ContinuousHandrail", "netex": "ContinuousHandrail", "mapping": mappings.MAPPING_SIDES_TRUEFALSE },
	])

	# NumberOfFlights
	if isKerb:
		eq.append(E.NumberOfFlights("1"))
	else:
		properties.appendPositiveInteger(eq, wdmFeature, "StairFlightCount", "NumberOfFlights")
	
	return eq


def genericEquipmentToXml(type: str, wdmEq: model.Feature):
	"""Transforms a generic equipment into Netex.

	Only IDs and keylist are set, no custom tag is added.
	"""

	# Create equipment
	ntxEq = etree.Element(type)

	# Check if equipment is derivated from another main object (like Quay)
	#  To avoid re-using export ID if any
	isDerivatedEquipment = False
	if (
		wdmEq.tags.get("Shelter") == "Yes"
		or wdmEq.tags.get("Seating") == "Yes"
		or wdmEq.tags.get("TicketVendingMachine") == "Yes"
		or wdmEq.tags.get("TicketValidator") == "Yes"
		or wdmEq.tags.get("RubbishDisposal") == "Yes"
	):
		isDerivatedEquipment = True

	properties.setNetexIdVersion(ntxEq, wdmEq, type, forceFallbackId=isDerivatedEquipment)
	properties.appendKeyList(ntxEq, wdmEq)

	return ntxEq


def sanitaryEquipmentToXml(wds, wdmEq, objId):
	"""Transforms a single toilets into Netex"""

	ntxEq = genericEquipmentToXml("SanitaryEquipment", wdmEq)

	# AccessibilityAssessment
	access = properties.wdmTagsToNetexAccess(wdmEq.tags, objId)
	if access is not None:
		ntxEq.append(access)

	# SanitaryFacilityList
	sflValList = ["toilet"]
	if wdmEq.tags.get("WheelchairAccess") == "Yes":
		sflValList.append("wheelchairAccessToilet")
	if wdmEq.tags.get("Toilets:Shower") == "Yes":
		sflValList.append("shower")
		sflValList.append("washingAndChangeFacilities")
	if wdmEq.tags.get("Toilets:BabyChange") in ["Yes", "Bad"]:
		sflValList.append("babyChange")
		if wdmEq.tags.get("Toilets:BabyChange") == "Yes":
			sflValList.append("wheelchairBabyChange")
	ntxEq.append(E.SanitaryFacilityList(" ".join(sflValList)))

	properties.appendDirectMappings(ntxEq, wdmEq, [{ "wdm": "FreeToUse", "netex": "FreeToUse", "mapping": mappings.MAPPING_YESNO_TRUEFALSE }])
	properties.appendPositiveFloat(ntxEq, wdmEq, "ManoeuvringDiameter", "WheelchairTurningCircle", 2)
	properties.appendDirectMappings(ntxEq, wdmEq, [
		mappings.MAPPING_RULE_STAFFING,
		{ "wdm": "WheelchairAccess:Description", "netex": "KeyScheme" },
		{ "wdm": "Toilets:HandWashing", "netex": "HandWashing", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{ "wdm": "Toilets:DrinkingWater", "netex": "DrinkingWater", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
	])

	# ToiletsType
	wdmTT = wdmEq.tags.get("Toilets:Position") or ""
	if wdmTT == "Seated":
		ntxEq.append(E.ToiletsType("seated"))
	elif wdmTT == "Urinal":
		ntxEq.append(E.ToiletsType("urinal"))
	elif wdmTT == "Squat":
		ntxEq.append(E.ToiletsType("squat"))
	elif "Seated" in wdmTT and "Urinal" in wdmTT:
		ntxEq.append(E.ToiletsType("seatedAndUrinal"))

	return ntxEq


def roughSurfaceToXml(wdmEq):
	"""Transforms a rough surface obstacle into Netex"""

	ntxEq = genericEquipmentToXml("RoughSurface", wdmEq)
	properties.appendPositiveFloat(ntxEq, wdmEq, "Width", "Width", 2)
	properties.appendDirectMappings(ntxEq, wdmEq, [ mappings.MAPPING_RULE_SURFACETYPE ])

	return ntxEq


def generalSignToXml(wdmEq):
	"""Transforms a sign into Netex"""

	ntxEq = genericEquipmentToXml("GeneralSign", wdmEq)
	properties.appendPositiveFloat(ntxEq, wdmEq, "Width", "Width", 2)

	return ntxEq


def seatingEquipmentToXml(wdmEq):
	"""Transforms a bench into Netex"""

	ntxEq = genericEquipmentToXml("SeatingEquipment", wdmEq)
	properties.appendDirectMappings(ntxEq, wdmEq, [
		{"wdm": "SeatCount", "netex": "Seats" },
		{"wdm": "BackRest", "netex": "BackRest", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
	])
	properties.appendPositiveFloat(ntxEq, wdmEq, "Width", "Width", 2)

	return ntxEq


def ticketingEquipmentToXml(wdmEq):
	"""Transforms a ticketing equipment into Netex"""

	ntxEq = genericEquipmentToXml("TicketingEquipment", wdmEq)

	# TicketOffice, TicketMachines
	amenity = wdmEq.tags.get("Amenity")
	if amenity == "TicketOffice":
		ntxEq.append(E.TicketOffice("true"))
	if amenity == "TicketVendingMachine" or wdmEq.tags.get("TicketVendingMachine") == "Yes":
		ntxEq.append(E.TicketMachines("true"))
	
	# InductionLoops, WheelchairSuitable
	properties.appendDirectMappings(ntxEq, wdmEq, [
		{"wdm": "MagneticInductionLoop", "netex": "InductionLoops", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
		{"wdm": "WheelchairAccess", "netex": "WheelchairSuitable", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
	])

	return ntxEq


def trolleyStandEquipmentToXml(wdmEq):
	"""Transforms a trolley stand equipment into Netex"""

	ntxEq = genericEquipmentToXml("TrolleyStandEquipment", wdmEq)

	# FreeToUse
	properties.appendDirectMappings(ntxEq, wdmEq, [
		{"wdm": "FreeToUse", "netex": "FreeToUse", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
	])

	return ntxEq


def passengerSafetyEquipmentToXml(wdmEq):
	"""Transforms a passenger safety equipment into Netex"""

	ntxEq = genericEquipmentToXml("PassengerSafetyEquipment", wdmEq)
	properties.appendDirectMappings(ntxEq, wdmEq, [{"wdm": "PublicCode", "netex": "PublicCode" }])
	ntxEq.append(E.SosPanel("true"))

	return ntxEq


def communicationServiceToXml(wdmEq):
	"""Transforms a post box into Netex"""

	ntxEq = genericEquipmentToXml("CommunicationService", wdmEq)
	properties.appendDirectMappings(ntxEq, wdmEq, [{"wdm": "PublicCode", "netex": "PublicCode" }])
	ntxEq.append(E.ServiceList("postbox"))

	return ntxEq


def assistanceServiceToXml(wdmEq):
	"""Transforms a reception desk/ticket office into Netex"""

	ntxEq = genericEquipmentToXml("AssistanceService", wdmEq)
	ntxEq.append(E.AssistanceFacilityList("information"))
	properties.appendDirectMappings(ntxEq, wdmEq, [
		mappings.MAPPING_RULE_STAFFING,
		{"wdm": "AccessibilityTrainedStaff", "netex": "AccessibilityTrainedStaff", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
	])

	return ntxEq


def shelterEquipmentToXml(wdmEq):
	"""Transforms a shelter into Netex"""

	ntxEq = genericEquipmentToXml("ShelterEquipment", wdmEq)
	properties.appendDirectMappings(ntxEq, wdmEq, [
		{"wdm": "SeatCount", "netex": "Seats" },
	])
	properties.appendPositiveFloat(ntxEq, wdmEq, "Width", "Width", 2)

	return ntxEq
