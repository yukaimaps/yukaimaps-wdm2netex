from dataclasses import dataclass
from typing import Optional, List, Dict
from shapely import Point, LineString, Polygon, Geometry, STRtree, area
import functools
import operator
from pyproj import Transformer


TRANFORM_4326_3857 = Transformer.from_crs(4326, 3857, always_xy=True)
QUAY_TO_STOPPLACE_TRANSPORT_MODE = {
	"Bus": "OnstreetBus",
	"Coach": "CoachStation",
	"Water": "FerryPort",
	"Tram": "OnstreetTram",
	"Funicular": "RailStation",
	"TrolleyBus": "TramStation",
	"CableWay": "LiftStation",
	"Rail": "RailStation",
	"Metro": "MetroStation",
	"Air": "Airport"
}


class Feature:
	"""A single feature"""

	TYPE_LETTER = "f"

	def __init__(self, tags: dict, attrs: dict):
		"""
		Parameters
		----------
		tags : dict
			Tags associated to this feature (part of Walk Data Model)
		attrs : dict
			Metadata attributes of the feature (version, user...)
		"""

		self.tags = tags
		self.attrs = attrs
	
	def getId(self):
		return self.TYPE_LETTER + self.attrs.get("id")


class Node(Feature):
	"""A point feature, equivalent to an OSM node"""

	TYPE_LETTER = "n"

	def __init__(self, tags: dict, attrs: dict):
		"""
		Parameters
		----------
		tags : dict
			Tags associated to this feature (part of Walk Data Model)
		attrs : dict
			Metadata attributes of the feature (lat, lon, version, user...)
		"""

		super().__init__(tags, attrs)
		self.geom = None

	def getCoordinates(self):
		"""Get node coordinates in WGS84 (EPSG:4326)

		Returns
		-------
		dict
			{ lat, lon }
		"""

		return { "lat": float(self.attrs["lat"]), "lon": float(self.attrs["lon"]) }
	
	def toGeom(self):
		"""Get this node as a Shapely Point geometry (in EPSG:3857)

		Returns
		-------
		shapely.Point
			The point geometry
		"""

		if self.geom is None:
			self.geom = Point(
				TRANFORM_4326_3857.transform(
					float(self.attrs["lon"]),
					float(self.attrs["lat"])
				)
			)

		return self.geom


class Way(Feature):
	"""A linear feature, equivalent to an OSM way"""

	TYPE_LETTER = "w"

	def __init__(self, tags: dict, attrs: dict, nodes: list):
		"""
		Parameters
		----------
		tags : dict
			Tags associated to this feature (part of Walk Data Model)
		attrs : dict
			Metadata attributes of the feature (version, user...)
		nodes : list
			List of nodes ID
		"""

		super().__init__(tags, attrs)
		self.nodes = nodes
		self.geom = None
		self.area = None
	
	def toGeom(self, nodes):
		"""Get this way as a Shapely LineString or Polygon geometry (in EPSG:3857)

		Parameters
		----------
		nodes : dict
			All nodes available in the Walk Data Set

		Returns
		-------
		shapely.Geometry
			The way geometry
		"""
		
		if self.geom is None:
			coords = []

			# Retrieve node from their reference in way
			for nodeId in self.nodes:
				node = nodes[nodeId]
				if node is not None:
					coords.append(
						TRANFORM_4326_3857.transform(
							float(node.attrs["lon"]),
							float(node.attrs["lat"])
						)
					)

			# Transform according to definition
			if self.nodes[0] == self.nodes[-1]:
				self.geom = Polygon(coords)
			else:
				self.geom = LineString(coords)
		
		return self.geom

	def getArea(self, nodes):
		"""Computes the geometry area (in projection units)"""

		if self.area is None:
			self.area = area(self.toGeom(nodes))

		return self.area


@dataclass
class RelationMember:
	role: Optional[str]
	type: str
	ref: str

	def getMemberId(self):
		"""Get the member ID in f1234 format"""
		return self.type[0:1] + self.ref


class Relation(Feature):
	"""A relation feature, as in OSM"""

	TYPE_LETTER = "r"

	def __init__(self, tags: dict, attrs: dict, members: list[RelationMember] = []):
		"""
		Parameters
		----------
		tags : dict
			Tags associated to this feature (part of Walk Data Model)
		attrs : dict
			Metadata attributes of the feature (version, user...)
		nodes : list
			List of nodes ID
		"""

		super().__init__(tags, attrs)
		self.members = members


class WalkDataSet:
	"""Wrapper for all data contained in a walk dataset"""

	def __init__(self):
		# All objects below are using f1234 ID format
		# Except self.nodes, which uses 1234 ID format
		self.sitePathLinks = {}
		self.sitePathLinksSiteRefs = {}
		self.placesPathLinks = {}
		self.nonSplEquipments = {}
		self.nodes = {}
		self.entrances = {}
		self.entrancesSiteRefs = {}
		self.quays = {}
		self.quaysSiteRefs = {}
		self.stopPlaces = {}
		self.multiModalStopPlaces = {}
		self.stopPlacesQuays = {}
		self.parentStopPlace = {}
		self.stopPlacesEntrances = {}
		self.poiEntrances = {}
		self.hasOsmData = False
		self.quayTree = False
		self.quayTreeIds = []
		self.parkingBays = {}
		self.pointsOfInterest = {}
		self.pathJunctions = {}
		self.amenities = []
		self.amenitiesIdx = None


	def _checkHasOsmData(self, f: Feature):
		"""Checks if given feature is imported from OSM"""
		if f.tags.get("Ref:Import:Source") == "OpenStreetMap":
			self.hasOsmData = True


	def _prepareQuayTree(self):
		"""Creates the STRTree indexing all quays for finding related StopPlaces"""

		if not self.quayTree:
			geoms = []
			for q in self.quays.values():
				if isinstance(q, Node):
					geoms.append(q.toGeom())
					self.quayTreeIds.append(q.getId())
				elif isinstance(q, Way):
					geoms.append(q.toGeom(self.nodes))
					self.quayTreeIds.append(q.getId())

			self.quayTree = STRtree(geoms)


	def _addSplPathJunction(self, splNodeId: str):
		"""Adds a extremity node of a SitePathLink if available and valid"""

		node = self.nodes[splNodeId]
		if node:
			# Check its tags
			if (
				node.tags.get("Amenity") is None
				and node.tags.get("ParkingBay") is None
				and node.tags.get("Entrance") is None
				and node.tags.get("Quay") is None
			):
				self.addPathJunction(node)


	def addSitePathLink(self, spl: Feature):
		"""Add a single site path link to the dataset

		Parameters
		----------
		spl : Feature
			The site path link feature
		"""

		self.sitePathLinks[spl.getId()] = spl
		self._checkHasOsmData(spl)

		# Also add first/last node as PathJunction
		if isinstance(spl, Way):
			self._addSplPathJunction(spl.nodes[0])
			self._addSplPathJunction(spl.nodes[-1])


	def addEntrance(self, e: Feature):
		"""Add a single entrance to the dataset

		Parameters
		----------
		e : Feature
			The entrance feature
		"""
		self.entrances[e.getId()] = e
		self._checkHasOsmData(e)


	def addQuay(self, q: Feature):
		"""Add a single quay to the dataset

		Parameters
		----------
		q : Feature
			The quay feature
		"""

		self.quays[q.getId()] = q
		self._checkHasOsmData(q)


	def addStopPlace(self, sp: Feature, quays: List[Feature] = []):
		"""Add a single stop place to the dataset.

		This may be called after all addQuay calls to allow proper linking between quays and stop places.

		Parameters
		----------
		sp : Feature
			The feature
		"""

		self.stopPlaces[sp.getId()] = sp
		self._checkHasOsmData(sp)

		#
		# Find associated quays
		#

		#  - Provided by user
		if len(quays) > 0:
			if sp.getId() in self.stopPlacesQuays:
				self.stopPlacesQuays[sp.getId()] += quays
			else:
				self.stopPlacesQuays[sp.getId()] = quays

		#  - Nodes are skipped
		#  - Ways are found based on their geometry
		elif isinstance(sp, Way):
			self._prepareQuayTree()

			for insideQuay in sorted(self.quayTree.query(sp.toGeom(self.nodes), predicate="contains")):
				q = self.quays[self.quayTreeIds[insideQuay]]
				if sp.getId() in self.stopPlacesQuays:
					self.stopPlacesQuays[sp.getId()].append(q.getId())
				else:
					self.stopPlacesQuays[sp.getId()] = [q.getId()]

		#  - Relations are processed according to their member ID
		elif isinstance(sp, Relation):
			for m in sp.members:
				if m.getMemberId() in self.quays:
					if sp.getId() in self.stopPlacesQuays:
						self.stopPlacesQuays[sp.getId()].append(m.getMemberId())
					else:
						self.stopPlacesQuays[sp.getId()] = [m.getMemberId()]


	def addEquipment(self, eq: Feature):
		"""Add a single equipment to the dataset.

		Note that this may be used only for equipments that are NOT SitePathLinks.

		Parameters
		----------
		eq : Feature
			The equipment feature
		"""

		self.nonSplEquipments[eq.getId()] = eq
		self._checkHasOsmData(eq)


	def addParkingBay(self, pb: Feature):
		"""Add a single parking bay to the dataset.

		Parameters
		----------
		pb : Feature
			The parking bay feature
		"""

		self.parkingBays[pb.getId()] = pb
		self._checkHasOsmData(pb)


	def addPointOfInterest(self, poi: Feature):
		"""Add a single POI to the dataset.

		Parameters
		----------
		poi : Feature
			The POI feature
		"""

		self.pointsOfInterest[poi.getId()] = poi
		self._checkHasOsmData(poi)


	def addPathJunction(self, pj: Feature):
		"""Add a single Path Junction to the dataset.

		Parameters
		----------
		pj : Feature
			The Path Junction feature
		"""

		self.pathJunctions[pj.getId()] = pj
		self._checkHasOsmData(pj)


	def addNode(self, n: Node):
		"""Registers a node

		Parameters
		----------
		n : Node
			The node object
		"""

		self.nodes[n.attrs["id"]] = n

		if "Amenity" in n.tags:
			self.amenities.append(n)


	def getNodesCoordinates(self, nodesId: list):
		"""Get the list of coordinates for a list of nodes ID

		Parameters
		----------
		nodesId : list
			List of nodes unique identifiers

		Returns
		-------
		list
			List of corresponding {lat, lon} coordinates
		"""

		return [ self.nodes[id].getCoordinates() for id in nodesId ]


	def findInnerAmenities(self, feature: Way, withTags: Optional[Dict[str,str]] = {}):
		"""Look for specific amenities inside given feature.

		Parameters
		----------
		feature : Way
			The polygon to look inside
		withTags : dict
			List of tags inner features should have
		
		Returns
		-------
		list
			List of found amenities inside
		"""

		# Compute amenities index at first launch
		if self.amenitiesIdx is None:
			self.amenitiesIdx = STRtree([ a.toGeom() for a in self.amenities ])
		
		# Run search on given feature, spatially then based on tagging
		matchAmenities = self.amenitiesIdx.query(feature.toGeom(self.nodes), 'intersects')
		matchAmenities = [ self.amenities[ma] for ma in matchAmenities ]
		matchAmenities = [ ma for ma in matchAmenities if withTags.items() <= ma.tags.items() ]

		return matchAmenities


	def postProcessDataset(self):
		"""Run post-process tasks on the dataset:
			- Create StopPlace for each Quay not in a StopPlace
			- Separate multimodal StopPlace into monomodal StopPlace + a grouping StopPlace
			- List entrances associated to StopPlaces
		"""

		#
		# Quays out of a Stop Place
		#

		quaysInStopPlace = set(functools.reduce(operator.iconcat, self.stopPlacesQuays.values(), []))
		quaysNotInStopPlace = sorted(list(set(self.quays.keys()) - quaysInStopPlace))
		quaysNotInSPByName = {}
		stopPlacesToCreate = []

		# Check for similarly named quays
		for quayNotInStopPlace in quaysNotInStopPlace:
			q = self.quays[quayNotInStopPlace]
			qName = q.tags.get("Name")

			if qName is not None:
				if qName in quaysNotInSPByName:
					quaysNotInSPByName[qName].append(q)
				else:
					quaysNotInSPByName[qName] = [q]

			# Unnamed quay is going to its own StopPlace
			else:
				stopPlacesToCreate.append([q])

		# Eventually split similarly named quays into distinct stop places
		# If they are too distant from each other
		for quayName in quaysNotInSPByName:
			quays = quaysNotInSPByName[quayName]

			# Many quays
			if len(quays) >= 2:
				# Create index for these quays
				quaysIdx = STRtree([
					(q.toGeom(self.nodes) if isinstance(q, Way) else q.toGeom())
					for q in quays
				])
				unprocessedQuaysIds = list(range(len(quays)))

				# Find groups of 100m diameter
				while len(unprocessedQuaysIds) > 0:
					firstQuayId = unprocessedQuaysIds[0]
					firstQuay = quays[firstQuayId]

					# No remaining quay -> first goes to its own stop place
					if len(unprocessedQuaysIds) == 0:
						stopPlacesToCreate.append([firstQuay])
					
					# One or more -> find all quays near
					else:
						nearQuaysIds = quaysIdx.query(
							firstQuay.toGeom(self.nodes) if isinstance(firstQuay, Way) else firstQuay.toGeom(),
							"dwithin",
							100
						)

						# Remove already processed quays
						nearQuaysIds = [ q for q in nearQuaysIds if q in unprocessedQuaysIds ]

						stopPlacesToCreate.append([ quays[i] for i in nearQuaysIds ])
						unprocessedQuaysIds = list(set(unprocessedQuaysIds) - set(nearQuaysIds))

			# Single quay goes to its own stop place
			else:
				stopPlacesToCreate.append([quays[0]])

		
		# Create each Stop Places based on generated groupings
		for sptc in stopPlacesToCreate:
			spType = sptc[0].tags.get("Quay") or "Bus"
			quaysIds = [q.getId() for q in sptc]
			spQId = min(quaysIds)
			sp = Feature({
				"StopPlace": QUAY_TO_STOPPLACE_TRANSPORT_MODE.get(spType),
				"Name": sptc[0].tags.get("Name"),
				"Ref:Export:Id": f"Yukaimaps:StopPlace:g_{spQId}_{spType.lower()}:LOC"
			}, {
				"id": spQId
			})
			self.addStopPlace(sp, quaysIds)
		

		#
		# Multi-modal Stop Places
		#

		# Look for stop places with many quays
		newMonoModalStopPlaces = []
		for spId in self.stopPlacesQuays:
			spQuaysIds = self.stopPlacesQuays[spId]

			if len(spQuaysIds) > 1:
				# Find quays transport modes
				spQuays = [ self.quays[qid] for qid in spQuaysIds ]
				spQuaysTM = set([ q.tags["Quay"] for q in spQuays if q.tags.get("Quay") is not None ])

				# Many transport modes = multimodal
				if len(spQuaysTM) > 1:
					sp = self.stopPlaces[spId]
					self.multiModalStopPlaces[spId] = sp

					# Create a stop place for each transport mode
					for transportMode in sorted(spQuaysTM):
						tmQuays = [ q.getId() for q in spQuays if q.tags.get("Quay") == transportMode ]
						tmSpId = f"{sp.getId()}_{transportMode}"
						tmSp = Feature({
							"StopPlace": QUAY_TO_STOPPLACE_TRANSPORT_MODE.get(transportMode),
							"Name": sp.tags.get("Name"),
							"Ref:Export:Id": f"Yukaimaps:StopPlace_{transportMode.lower()}:{sp.getId()}:LOC"
						}, {
							"id": tmSpId
						})
						newMonoModalStopPlaces.append((tmSp, tmQuays))
						self.parentStopPlace[tmSp.getId()] = spId
		
		# Add new monomodal stop places
		for nmmsp in newMonoModalStopPlaces:
			self.addStopPlace(*nmmsp)

		# Remove multimodal stop places past references
		for spId in self.multiModalStopPlaces:
			self.stopPlacesQuays.pop(spId)
			self.stopPlaces.pop(spId)
		
		# Quay site ref
		for spId, quaysIds in self.stopPlacesQuays.items():
			for quayId in quaysIds:
				self.quaysSiteRefs[quayId] = spId


		#
		# Stop Places Entrances
		#

		candidatesEntrances = [ e for e in self.entrances.values() if e.tags.get("Entrance") == "StopPlace" ]
		entrancesIdx = STRtree([ e.toGeom() for e in candidatesEntrances ])
		candidatesStopPlaces = list(self.multiModalStopPlaces.values()) + list(self.stopPlaces.values())
		candidatesStopPlaces = [ c for c in candidatesStopPlaces if isinstance(c, Way) ]

		for sp in candidatesStopPlaces:
			matchEntrances = entrancesIdx.query(sp.toGeom(self.nodes), 'intersects')
			self.stopPlacesEntrances[sp.getId()] = [ candidatesEntrances[me].getId() for me in matchEntrances ]
			self.poiEntrances[sp.getId()] = [ candidatesEntrances[me].getId() for me in matchEntrances ]

			# Associate stop place to its entrances
			for eId in self.stopPlacesEntrances[sp.getId()]:
				self.entrancesSiteRefs[eId] = sp.getId()
		

		#
		# Point of Interest entrances
		#

		candidatesEntrances = [ e for e in self.entrances.values() if e.tags.get("Entrance") in ["PointOfInterest", "Building"] ]
		entrancesIdx = STRtree([ e.toGeom() for e in candidatesEntrances ])
		candidatesPOIs = list(self.pointsOfInterest.values())
		candidatesPOIs = [ c for c in candidatesPOIs if isinstance(c, Way) and c.tags.get("PointOfInterest") != "StopPlace" ]

		for poi in candidatesPOIs:
			matchEntrances = entrancesIdx.query(poi.toGeom(self.nodes), 'intersects')
			self.poiEntrances[poi.getId()] = [ candidatesEntrances[me].getId() for me in matchEntrances ]

			# Associate POI to its entrances
			for eId in self.poiEntrances[poi.getId()]:
				self.entrancesSiteRefs[eId] = poi.getId()


		#
		# Site Path Links parent (SiteRef)
		#

		candidatesPlaces = candidatesStopPlaces + candidatesPOIs
		placesIdx = STRtree([ p.toGeom(self.nodes) for p in candidatesPlaces ])

		for splId, spl in self.sitePathLinks.items():
			splGeom = spl.toGeom(self.nodes) if isinstance(spl, Way) else spl.toGeom()
			matchPlaces = placesIdx.query(splGeom, 'within')
			matchPlaces = [ candidatesPlaces[mp] for mp in matchPlaces ]

			# Sort by size if many matches
			if len(matchPlaces) > 1:
				matchPlaces = sorted(matchPlaces, key=lambda p: p.getArea(self.nodes))
			
			if len(matchPlaces) > 0:
				self.sitePathLinksSiteRefs[splId] = matchPlaces[0].getId()

				# Also add to placesPathLinks (for POI/StopPlace -> SPL link)
				for p in matchPlaces:
					if not p.getId() in self.placesPathLinks:
						self.placesPathLinks[p.getId()] = [spl]
					else:
						self.placesPathLinks[p.getId()].append(spl)
