#
# Manages creation of Netex features properties
#

from lxml import etree
from lxml.builder import E
from haversine import haversine, Unit
import math
import copy
from . import model, mappings
from typing import Dict, Optional


def getTagsFromWDMFeature(f : etree.Element):
	"""Read tags from XML markup of a Feature"""

	tags = {}
	for tag in f.iter('tag'):
		tags[tag.attrib["k"]] = tag.attrib["v"]
	return tags


def setNetexIdVersion(ntx, wdm, fallbackType, fallbackId = None, forceFallbackId: bool = False):
	"""Appends ID and version attributes to Netex feature"""

	if not forceFallbackId and (wdm.tags.get("Ref:Import:Source") or "").lower() == "netex":
		ntx.attrib["id"] = wdm.tags.get("Ref:Import:Id")
		ntx.attrib["version"] = (
			"any" if wdm.tags.get("Ref:Import:Version") is None
			else f"{wdm.tags['Ref:Import:Version']}+{wdm.attrs.get('version') or 'any'}"
		)

	elif not forceFallbackId and wdm.tags.get("Ref:Export:Id") is not None:
		ntx.attrib["id"] = wdm.tags.get("Ref:Export:Id")
		ntx.attrib["version"] = "+" + wdm.attrs['version'] if wdm.attrs.get("version") is not None else "any"

	else:
		id = fallbackId or wdm.getId()
		ntx.attrib["id"] = f"Yukaimaps:{fallbackType}:{id}:LOC"
		ntx.attrib["version"] = wdm.attrs.get('version') or "any"


def appendKeyList(tree: etree._Element, feature: model.Feature):
	"""Creates the keyList for a given element"""

	# Keylist (other tags)
	keyList = E.keyList()
	for key in sorted(feature.tags):
		if (
			(tree.tag not in mappings.KEYLIST_TAGS or key not in mappings.KEYLIST_TAGS[tree.tag])
			and (feature.tags.get("Ref:Import:Source", "").lower() != "netex" or key not in mappings.KEYLIST_TAGS_IMPORT)
		):
			val = feature.tags[key]
			keyList.append(
				E.KeyValue(
					E.Key(key),
					E.Value(val),
					typeOfKey = "WDM_tag"
				)
			)
	
	# Embed WDM ID if not used as main feature ID
	if "id" in tree.attrib and not tree.attrib["id"].startswith("Yukaimaps:"):
		keyList.append(E.KeyValue(
			E.Key("Id"),
			E.Value(feature.getId()),
			typeOfKey = "WDM_id"
		))
		keyList.append(E.KeyValue(
			E.Key("Version"),
			E.Value(feature.attrs.get("version") or "any"),
			typeOfKey = "WDM_id"
		))

	if len(keyList) > 0:
		tree.append(keyList)


def appendPositiveInteger(tree, feature, wdmTag, netexTag):
	"""Add a tag to XML tree if it is a positive integer"""

	if wdmTag in feature.tags:
		try:
			val = int(feature.tags[wdmTag])
			if val > 0:
				se = etree.SubElement(tree, netexTag)
				se.text = str(feature.tags[wdmTag])
		except ValueError:
			# If number is not an integer, ignore value
			pass


def appendGradient(tree, slope):
	"""Appends Gradient and GradientType tags given feature"""

	if slope is not None:
		try:
			gradientPct = float(slope)
			gradientDeg = anglePctToDegrees(gradientPct)
			if gradientDeg >= 0:
				tree.append(E.Gradient(str(gradientDeg)))

				if tree.tag != "TravelatorEquipment":
					if gradientPct == 0:
						tree.append(E.GradientType("level"))
					elif gradientPct <= 4:
						tree.append(E.GradientType("gentle"))
					elif gradientPct <= 5:
						tree.append(E.GradientType("medium"))
					elif gradientPct <= 8:
						tree.append(E.GradientType("steep"))
					else:
						tree.append(E.GradientType("verySteep"))
		except ValueError:
			pass


def appendPostalAddress(tree, tags, objId):
	"""Appends a PostalAddress data"""

	addr = tags.get("AddressLine")
	postcode = tags.get("PostCode")

	if addr is not None or postcode is not None:
		postalAddress = E.PostalAddress(
			version = "any",
			id = f"Yukaimaps:PostalAddress:{objId}"
		)

		if addr is not None:
			postalAddress.append(E.AddressLine1(addr))
		
		if postcode is not None:
			postalAddress.append(E.PostCode(postcode))

		tree.append(postalAddress)


def isValidPositiveFloat(val, allowZero = False):
	"""Checks if a string represents a valid positive float number"""
	try:
		val = float(val)
		return val >= 0 if allowZero else val > 0
	except ValueError:
		return False
	except TypeError:
		return False


def appendPositiveFloat(tree, feature, wdmTag, netexTag, precision):
	"""Add a tag to XML tree if it is a positive float"""

	if wdmTag in feature.tags and isValidPositiveFloat(feature.tags[wdmTag]):
		se = etree.SubElement(tree, netexTag)
		se.text = f"%.{precision}f" % float(feature.tags[wdmTag])


def appendDirectMappings(tree, feature, directMappings):
	"""Add simple tags transforms to XML tree

	Parameters
	----------
	tree : lxml.etree.ElementTree
		The XML tree to append this tag to
	feature : model.Feature
		The input WDM feature to read
	directMappings : list
		The list of direct tag mappings to apply.
		Each entry of the list is a dict with this structure:
			{ "wdm": "INPUT_WDM_TAG", "netex": "OUTPUT_NETEX_TAG", "mapping": { "WDM_VAL": "NETEX_VAL" } }
		If no "mapping" property is given, value is passed as is.
		If in "mapping" an empty string is set as WDM_VAL, it will be used as a default when input WDM tag is undefined.
		If in "mapping" a "*" string is set as WDM_VAL, it will be used as a fallback value if WDM tag doesn't match another precise value.
	"""

	for dm in directMappings:
		se = etree.Element(dm["netex"])
		if dm["wdm"] in feature.tags:
			if "mapping" in dm:
				if feature.tags[dm["wdm"]] in dm["mapping"]:
					se.text = dm["mapping"][feature.tags[dm["wdm"]]]
					if se.text != "":
						tree.append(se)
				elif "*" in dm["mapping"]:
					se.text = dm["mapping"]["*"]
					if se.text != "":
						tree.append(se)
			else:
				se.text = feature.tags[dm["wdm"]]
				if se.text != "":
					tree.append(se)
		elif "mapping" in dm and "" in dm["mapping"]:
			se.text = dm["mapping"][""]
			if se.text != "":
				tree.append(se)


def getGeometryXml(wds, f, objId: int = None, directLocation: bool = False):
	"""Get geometry in Netex format for a given WDM feature"""

	# Geometry
	if f.tags.get("NonGeographicalLocation") == "Yes":
		return None

	elif isinstance(f, model.Node):
		loc = E.Location(
			E.Longitude(f.attrs['lon']),
			E.Latitude(f.attrs['lat']),
			id=f"n{f.attrs['id']}"
		)

		if f.tags.get("Altitude") is not None:
			loc.append(E.Altitude(f.tags["Altitude"]))

		return loc if directLocation else E.Centroid(loc)

	elif isinstance(f, model.Way):
		polygon = etree.Element("{"+mappings.NS_GML["xmlns"]+"}Polygon", srsName = "wgs84", nsmap = mappings.NS_GML)
		polygon.attrib["{"+mappings.NS_GML["xmlns"]+"}id"] = f"w{f.attrs['id']}"
		if objId is not None:
			polygon.attrib["{"+mappings.NS_GML["xmlns"]+"}id"] += f"_{objId}"
		exterior = etree.SubElement(polygon, "{"+mappings.NS_GML["xmlns"]+"}exterior")
		linearRing = etree.SubElement(exterior, "{"+mappings.NS_GML["xmlns"]+"}LinearRing")

		for c in wds.getNodesCoordinates(f.nodes):
			pos = etree.SubElement(linearRing, "{"+mappings.NS_GML["xmlns"]+"}pos")
			pos.text = f"{c['lon']} {c['lat']}"
		
		return polygon


def appendGeometry(wds, f, fNtx, objId: int = None, directLocation: bool = False):
	"""Creates geometry in Netex feature based on given WDM feature"""

	geom = getGeometryXml(wds, f, objId, directLocation)
	if geom is not None:
		fNtx.append(geom)


def appendPlaceRef(wds, nodeId, ntxFeature, containerName, netexMembersByWdsId):
	"""Creates Netex PlaceRef in a From/To container for a given feature"""

	# Find Netex element for given node
	nodeType = "PathJunction"
	nodeAttrib = None

	for ntxType in ["PathJunction", "Entrance", "ParkingBay", "Quay", "Equipment"]:
		if nodeId in netexMembersByWdsId[ntxType]:
			nodeType = ntxType
			nodeAttrib = netexMembersByWdsId[ntxType][nodeId]
			break
	
	nodeRef = nodeAttrib["id"] if nodeAttrib is not None else f"Yukaimaps:{nodeType}:{nodeId}:LOC"
	nodeVersion = nodeAttrib["version"] if nodeAttrib is not None else (wds.nodes[nodeId[1:]].attrs.get("version") or "any")

	# Define PlaceRef
	container = etree.SubElement(ntxFeature, containerName)
	container.append(E.PlaceRef(ref = nodeRef, version = nodeVersion))

	# Add EntranceRef if any
	if nodeType == "Entrance":
		container.append(E.EntranceRef(ref = nodeRef, version = nodeVersion))


def appendFacilities(wds: model.WalkDataSet, tree: etree._Element, feature: model.Feature, objId: int):
	"""Creates the list of facilities into the Netex element"""

	facilities = {}
	innerAmenities = wds.findInnerAmenities(feature) if isinstance(feature, model.Way) else []
	innerTicketOffices = [ a for a in innerAmenities if a.tags["Amenity"] == "TicketOffice" ]
	innerReceptionDesks = [ a for a in innerAmenities if a.tags["Amenity"] == "ReceptionDesk" ]
	innerOffices = innerTicketOffices + innerReceptionDesks
	innerTrolleys = [ a for a in innerAmenities if a.tags["Amenity"] == "TrolleyStand"]

	# AccessibilityInfoFacilityList
	if feature.tags.get("StopPlace") is not None:
		valList = []

		if feature.tags.get("AudioInformation") in ["Yes", "Bad"] or feature.tags.get("AudibleSignals") == "Yes":
			valList.append("audioInformation")
			if feature.tags.get("AudioInformation") == "Yes":
				valList.append("audioForHearingImpaired")
		# Look for inner Amenity=ReceptionDesk/TicketOffice with MagneticInductionLoop=Yes
		elif next((rd for rd in innerOffices if rd.tags.get("MagneticInductionLoop") == "Yes"), False):
			valList.append("audioForHearingImpaired")

		if feature.tags.get("VisualDisplays") in ["Yes", "Bad"]:
			valList.append("visualDisplays")
			if feature.tags.get("VisualDisplays") == "Yes":
				valList.append("displaysForVisuallyImpaired")
		
		if feature.tags.get("LargePrintTimetable") == "Yes":
			valList.append("largePrintTimetables")
		
		facilities["AccessibilityInfoFacilityList"] = valList

	# AssistanceFacilityList
	assistanceBoarding = False
	assistanceMinor = False
	if feature.tags.get("Assistance") is not None:
		valList = []
		if feature.tags["Assistance"] == "None" and len(innerReceptionDesks) == 0:
			valList.append("none")
		else:
			ast = feature.tags["Assistance"].split(";")
			if "Boarding" in ast:
				valList.append("boardingAssistance")
				assistanceBoarding = True
			if "Wheelchair" in ast:
				valList.append("wheelchairAssistance")
			if "UnaccompaniedMinor" in ast:
				assistanceMinor = True
				valList.append("unaccompaniedMinorAssistance")
			if "Conductor" in ast:
				valList.append("conductor")
			if "Information" in ast or len(innerReceptionDesks) > 0:
				valList.append("information")

		facilities["AssistanceFacilityList"] = valList

	# AccessibilityToolList
	atlValList = []
	if feature.tags.get("AccessibilityToolLending") is not None:
		atl = feature.tags["AccessibilityToolLending"].split(";")
		if "Wheelchair" in atl:
			atlValList.append("wheelchair")
		if "WalkingStick" in atl:
			atlValList.append("walkingstick")
		if "AudioNavigator" in atl:
			atlValList.append("audioNavigator")
		if "VisualNavigator" in atl:
			atlValList.append("visualNavigator")
		if "PassengerCart" in atl:
			atlValList.append("passengerCart")
		if "Pushchair" in atl:
			atlValList.append("pushchair")
		if "Umbrella" in atl:
			atlValList.append("umbrella")
	# Look for inner trolley stands
	if "passengerCart" not in atlValList:
		if len(innerTrolleys) > 0:
			atlValList.append("passengerCart")
	facilities["AccessibilityToolList"] = atlValList

	# FamilyFacilityList
	if feature.tags.get("FamilyServices") is not None:
		if feature.tags["FamilyServices"] == "None":
			facilities["FamilyFacilityList"] = ["none"]
		else:
			fs = feature.tags["FamilyServices"].split(";")
			valList = []

			if "Children" in fs:
				valList.append("servicesForChildren")
			if "Nursery" in fs:
				valList.append("nurseryService")
			
			facilities["FamilyFacilityList"] = valList
	
	# MedicalFacilityList
	if next((a for a in innerAmenities if a.tags["Amenity"] == "Defibrillator"), False):
		facilities["MedicalFacilityList"] = ["defibrillator"]

	# MobilityFacilityList
	#  - TODO: stepFreeAccess, tactileGuidingStrips
	mflValList = []
	if feature.tags.get("WheelchairAccess") == "Yes":
		mflValList.append("suitableForWheelchairs")
	if assistanceBoarding:
		mflValList.append("boardingAssistance")
	if assistanceMinor:
		mflValList.append("unaccompaniedMinorAssistance")
	# Look for inner quays with tactile warning strips
	if feature.tags.get("StopPlace") is not None:
		quays = wds.stopPlacesQuays.get(feature.getId())
		if quays is not None and len(quays) > 0:
			quays = [ wds.quays[q] for q in quays if q in wds.quays ]
			if len(quays) == len([ q for q in quays if q.tags.get("TactileWarningStrip") not in [None, "None"]]):
				mflValList.append("tactilePlatformEdges")
	facilities["MobilityFacilityList"] = ["unknown"] if len(mflValList) == 0 else mflValList

	# PassengerCommsFacilityList
	pcflValList = []
	wifi = feature.tags.get("WiFi")
	if wifi is not None:
		if wifi == "Free":
			pcflValList.append("freeWifi")
		if wifi != "No":
			pcflValList.append("publicWifi")
			pcflValList.append("internet")
	if next((a for a in innerAmenities if a.tags["Amenity"] == "PostBox"), False):
		pcflValList.append("postBox")
	facilities["PassengerCommsFacilityList"] = pcflValList

	# PassengerInformationEquipmentList
	pielValList = []
	if len(innerOffices) > 0:
		pielValList.append("informationDesk")
	if feature.tags.get("RealTimeDepartures") == "Yes":
		pielValList.append("realTimeDepartures")
	facilities["PassengerInformationEquipmentList"] = pielValList

	# SanitaryFacilityList
	sflValList = []
	innerToilets = [ a for a in innerAmenities if a.tags["Amenity"] == "Toilets" ]
	if feature.tags.get("Toilets") == "No" and len(innerToilets) == 0:
		sflValList.append("none")
	if feature.tags.get("Toilets") in ["Yes", "Bad"] or len(innerToilets) > 0:
		sflValList.append("toilet")
		if (
			feature.tags.get("Toilets") == "Yes"
			or next((a for a in innerToilets if a.tags.get("WheelchairAccess") == "Yes"), False)
		):
			sflValList.append("wheelchairAccessToilet")
	if (
		feature.tags.get("Toilets:Shower") == "Yes"
		or next((a for a in innerToilets if a.tags.get("Toilets:Shower") == "Yes"), False)
	):
		sflValList.append("shower")
		sflValList.append("washingAndChangeFacilities")
	if (
		feature.tags.get("Toilets:BabyChange") in ["Yes", "Bad"]
		or next((a for a in innerToilets if a.tags.get("Toilets:BabyChange") in ["Yes", "Bad"]), False)
	):
		sflValList.append("babyChange")
		if (
			feature.tags.get("Toilets:BabyChange") == "Yes"
			or next((a for a in innerToilets if a.tags.get("Toilets:BabyChange") == "Yes"), False)
		):
			sflValList.append("wheelchairBabyChange")
	facilities["SanitaryFacilityList"] = sflValList

	# TicketingFacilityList
	tflValList = []
	tickets = feature.tags.get("Tickets", "").split(";")
	if (
		"VendingMachine" in tickets
		or next((a for a in innerAmenities if a.tags["Amenity"] == "TicketVendingMachine"), False)
	):
		tflValList.append("ticketMachines")
	if "Office" in tickets or len(innerTicketOffices) > 0:
		tflValList.append("ticketOffice")
	if "OnDemandMachines" in tickets:
		tflValList.append("ticketOnDemandMachines")
	if "MobileTicketing" in tickets:
		tflValList.append("mobileTicketing")
	facilities["TicketingFacilityList"] = tflValList

	# TODO: AccessFacilityList

	# EmergencyServiceList
	eslValList = []
	emergency = feature.tags.get("Emergency", "").split(";")
	if "Police" in emergency:
		eslValList.append("police")
	if "Fire" in emergency:
		eslValList.append("fire")
	if "FirstAid" in emergency:
		eslValList.append("firstAid")
	if (
		"SOSPoint" in emergency
		or next((a for a in innerAmenities if a.tags["Amenity"] == "EmergencyPhone"), False)
	):
		eslValList.append("sosPoint")
	facilities["EmergencyServiceList"] = eslValList

	# LuggageLockerFacilityList
	if next((a for a in innerAmenities if a.tags["Amenity"] == "LuggageLocker"), False):
		facilities["LuggageLockerFacilityList"] = ["lockers"]
	
	# LuggageServiceFacilityList
	if len(innerTrolleys) > 0:
		lsflValList = []
		if next((a for a in innerTrolleys if a.tags.get("FreeToUse") == "Yes"), False):
			lsflValList.append("freeTrolleys")
		if next((a for a in innerTrolleys if a.tags.get("FreeToUse") == "No"), False):
			lsflValList.append("paidTrolleys")
		if next((a for a in innerTrolleys if a.tags.get("FreeToUse") is None), False):
			lsflValList.append("other")
		facilities["LuggageServiceFacilityList"] = lsflValList

	# ParkingFacilityList
	facilities["ParkingFacilityList"] = []
	if feature.tags.get("NearbyCarPark") == "Yes":
		facilities["ParkingFacilityList"].append("carPark")
	if feature.tags.get("NearbyCyclePark") == "Yes":
		facilities["ParkingFacilityList"].append("cyclePark")

	# Staffing
	staffValList = []
	if (
		feature.tags.get("Staffing") == "No"
		or next((a for a in innerOffices if a.tags.get("Staffing") == "No"), False)
	):
		staffValList.append("unmanned")
	if (
		feature.tags.get("Staffing") == "PartTime"
		or next((a for a in innerOffices if a.tags.get("Staffing") == "PartTime"), False)
	):
		staffValList.append("partTime")
	if (
		feature.tags.get("Staffing") == "FullTime"
		or next((a for a in innerOffices if a.tags.get("Staffing") == "FullTime"), False)
	):
		staffValList.append("fullTime")
	facilities["Staffing"] = staffValList

	# Add all facilities in Netex feature
	fset = E.SiteFacilitySet(
		id = f"Yukaimaps:SiteFacilitySet:{objId}:",
		version = "any"
	)
	for k, vl in facilities.items():
		if len(vl) > 0:
			f = etree.SubElement(fset, k)
			f.text = " ".join(vl)
	
	if len(fset) > 0:
		tree.append(E.facilities(fset))


def appendPlaceEquipments(ntx, placeEquipments, skipContainer = False):
	"""Creates PlaceEquipments entries in given Netex feature"""

	if len(placeEquipments) > 0:
		pEq = E.placeEquipments() if not skipContainer else ntx
		for eqType in placeEquipments:
			etree.SubElement(pEq, eqType+"Ref", ref=placeEquipments[eqType])
		if not skipContainer:
			ntx.append(pEq)


def appendEquipmentPlaces(ntx, equipmentPlaces, objId):
	"""Creates EquipmentPlaces entries in given Netex feature"""

	if len(equipmentPlaces) > 0:
		pEq = E.equipmentPlaces()
		for i, eq in enumerate(equipmentPlaces):
			eqPosId = eq["equipment"].attrib["id"].replace(eq["type"], "EquipmentPosition").replace(":LOC", f"_{objId}_{i}:LOC")
			eqpos = E.EquipmentPosition(
				E.EquipmentRef(
					ref=eq["equipment"].attrib["id"],
					version=eq["equipment"].attrib["version"]
				),
				id = eqPosId,
				version = "any"
			)

			# Location (from geom)
			if eq.get("geom") is not None:
				newloc = copy.deepcopy(eq["geom"])
				eqpos.append(newloc)

			# Location (from feature)
			elif eq.get("feature") is not None:
				loc = eq["feature"].find("Centroid/Location")
				if loc is not None:
					newloc = copy.deepcopy(loc)
					del newloc.attrib["id"]
					eqpos.append(newloc)

			pEq.append(
				E.EquipmentPlace(
					E.equipmentPositions(eqpos),
					id = f"Yukaimaps:EquipmentPlace:{objId}{i}",
					version = "any"
				)
			)
		ntx.append(pEq)


def findAndAppendAmenityEquipmentPlaces(wds, wdmFeature, ntxFeature, objId, equipmentPlacesByWdsId):
	"""Search for inner amenities in given feature, and complete equipmentPlaces property in Netex feature"""

	equipmentPlaces = []
	if isinstance(wdmFeature, model.Node):
		for eqType, eqList in equipmentPlacesByWdsId.items():
			if wdmFeature.getId() in eqList:
				equipmentPlaces.append({ "type": eqType, **eqList[wdmFeature.getId()] })
	
	elif isinstance(wdmFeature, model.Way):
		innerAmenities = [ pa.getId() for pa in wds.findInnerAmenities(wdmFeature) ]
		for eqType, eqList in equipmentPlacesByWdsId.items():
			equipmentPlaces += [
				{ "type": eqType, **eqList[eqId] }
				for eqId in innerAmenities if eqId in eqList
			]
	
	appendEquipmentPlaces(ntxFeature, equipmentPlaces, objId)


def findAndAppendSplPlaceEquipments(wds, wdmFeature, ntxFeature, netexMembersByWdsId, placeEquipmentsByWdsSplId):
	pathLinks = []
	placeEqNtx = E.placeEquipments()
	if wdmFeature.getId() in wds.placesPathLinks:
		for spl in wds.placesPathLinks[wdmFeature.getId()]:
			# Find equipments
			if spl.tags.get("SitePathLink") in ["Ramp", "Stairs", "Escalator", "Travelator", "Elevator", "Crossing", "QueueManagement"]:
				if spl.getId() in placeEquipmentsByWdsSplId:
					appendPlaceEquipments(placeEqNtx, placeEquipmentsByWdsSplId[spl.getId()], True)

			# Also read pathLinks
			if spl.getId() in netexMembersByWdsId["SitePathLink"]:
				for ntxSplAttrib in netexMembersByWdsId["SitePathLink"][spl.getId()]:
					pathLinks.append(E.PathLinkRef(
						ref=ntxSplAttrib["id"],
						version=ntxSplAttrib["version"]
					))
	if len(placeEqNtx) > 0:
		ntxFeature.append(placeEqNtx)
	
	return pathLinks


def getPathMinimumWidth(wds, path, pathNodes = []):
	"""Computes the minimum width of a path regarding obstacles you can find along"""

	if "Width" in path.tags:
		if isValidPositiveFloat(path.tags["Width"]):
			width = float(path.tags["Width"])

			# Remaining width will be lowered depending on found obstacles
			pathRemainingWidth = width

			# SitePathLink=Crossing special case
			if path.tags["SitePathLink"] == "Crossing":
				firstNode = wds.nodes.get(pathNodes[0])
				kcwFirst = firstNode.tags.get("KerbCut:Width")
				if isValidPositiveFloat(kcwFirst):
					pathRemainingWidth = min(pathRemainingWidth, float(kcwFirst))
				
				lastNode = wds.nodes.get(pathNodes[-1])
				kcwLast = lastNode.tags.get("KerbCut:Width")
				if isValidPositiveFloat(kcwLast):
					pathRemainingWidth = min(pathRemainingWidth, float(kcwLast))
			
			# Default case
			else:
				for nodeId in pathNodes:
					node = wds.nodes[nodeId]
					# Check if node is obstacle with width (but not overhanging >= 2.20m height)
					if (
						"Obstacle" in node.tags
						and isValidPositiveFloat(node.tags.get("RemainingWidth"))
						and (
							node.tags.get("ObstacleType") != "Overhanging"
							or "Height" not in node.tags
							or not isValidPositiveFloat(node.tags["Height"])
							or float(node.tags["Height"]) < 2.2
						)
					):
						pathRemainingWidth = min(pathRemainingWidth, float(node.tags.get("RemainingWidth")))

			return "%.2f" % pathRemainingWidth

	return None


def wdm3StatesToNetex(val):
	"""Transforms WDM Yes/No/Limited values into Netex true/false/partial"""
	if isinstance(val, set) and len(val) == 1:
		val = list(val)[0]

	if isinstance(val, set):
		return "unknown" if None in val else "partial"
	else:
		if val == "Yes":
			return "true"
		elif val in ["Limited", "Bad"]:
			return "partial"
		elif val is None:
			return "unknown"
		else:
			return "false"


def wdmTagsToNetexAccess(tags, blockId, nodesTags = []):
	"""Transforms Walk data model tags into Netex accessibility block"""

	res = None
	mandatoryByTag = (
		"SitePathLink" in tags
		or "Entrance" in tags
		or "ParkingBay" in tags
		or tags.get("Amenity") == "Toilets"
		or tags.get("Obstacle") == "Toilets"
	)

	# Accessibility tags
	if (
		mandatoryByTag
		or len([ k for k in mappings.WDM_ACCESS_TAGS if k in tags ]) > 0
	):
		res = E.AccessibilityAssessment(
			id = f"Yukaimaps:AccessibilityAssessment:{blockId}:",
			version = "any"
		)

		# Validity conditions (empty for now, completed later)
		resValidDesc = E.Description()
		resValidCond = E.validityConditions(
			E.ValidityCondition(
				resValidDesc,
				id = f"Yukaimaps:ValidityCondition:{blockId}:",
				version = "any"
			)
		)
		validityConditions = [ tags[k] for k in mappings.WDM_ACCESS_DESC_TAGS if k in tags ]
		res.append(resValidCond)


		# Reading main access tags
		availableMainTags = [ k for k in mappings.WDM_ACCESS_MAIN_TAGS if k in tags ]
		availableCrossingTags = [ k for k in mappings.WDM_ACCESS_CROSSING_TAGS if k in tags ]
		if (
			len(availableMainTags) > 0
			or mandatoryByTag
		):

			#
			# Accessibility limitations
			#

			limitations = E.AccessibilityLimitation()

			# WheelchairAccess
			wheelchairAccess = E.WheelchairAccess("unknown")
			limitations.append(wheelchairAccess)

			# Technical details to define wheelchair access if missing
			tilt = getIntegerTag(tags, "Tilt")
			slope = getIntegerTag(tags, "Slope")
			width = getFloatTag(tags, "Width")
			depth = getFloatTag(tags, "Depth")
			flooringMaterial = tags.get("FlooringMaterial")
			flooring = tags.get("Flooring")
			automaticDoor = tags.get("AutomaticDoor")
			stepFree = tags.get("StepFreeAccess")
			force = getIntegerTag(tags, "NecessaryForceToOpen")
			kerb = getFloatTag(tags, "KerbHeight")
			hasObstacleNode = next((v for v in nodesTags if "Obstacle" in v), None) is not None
			hasKerbNode = next((v for v in nodesTags if v.get("Obstacle") == "Kerb"), None) is not None
			hasJunctionNode = next((v for v in nodesTags if v.get("PathJunction") == "Crossing"), None) is not None
			hasKerbHeight004Node = next(
				(
					v for v in nodesTags
					if
						v.get("PathJunction") == "Crossing"
						and getFloatTag(v, "KerbHeight") is not None
						and getFloatTag(v, "KerbHeight") > 0.04
				),
				None
			) is not None
			hasKerbHeight002Node = next(
				(
					v for v in nodesTags
					if
						v.get("PathJunction") == "Crossing"
						and getFloatTag(v, "KerbHeight") is not None
						and getFloatTag(v, "KerbHeight") >= 0.02
				),
				None
			) is not None
			hasRaisedKerbNode = next((v for v in nodesTags if v.get("PathJunction") == "Crossing" and v.get("KerbDesign") == "Raised"), None) is not None
			hasJunctionUndefinedKerbNode = next((v for v in nodesTags if v.get("PathJunction") == "Crossing" and v.get("KerbDesign") is None and v.get("KerbHeight") is None), None) is not None

			if tags.get("SitePathLink") in ["Stairs", "Escalator", "Travelator"]:
				wheelchairAccess.text = "false"

			elif "WheelchairAccess" in tags:
				wheelchairAccess.text = wdm3StatesToNetex(tags["WheelchairAccess"])
				if "StopPlace" in tags and wheelchairAccess.text == "partial":
					validityConditions.append("Présence inconnue d'un cheminement pratiquable en fauteuil roulant entre les quais")

			elif tags.get("SitePathLink") == "Ramp":
				if (
					(slope is not None and slope > 5)
					or (width is not None and width < 1.4)
				):
					wheelchairAccess.text = "false"
				elif(
					slope is not None and slope <= 5
					and width is not None and width >= 1.4
				):
					wheelchairAccess.text = "true"

			elif tags.get("SitePathLink") == "Elevator":
				if(
					(depth is not None and depth < 1.25)
					or automaticDoor == "No"
					or (width is not None and width < 0.8)
				):
					wheelchairAccess.text = "false"
				elif(
					automaticDoor is not None and automaticDoor != "No"
					and depth is not None and depth >= 1.25
					and width is not None and width >= 0.8
				):
					wheelchairAccess.text = "true"
			
			elif "SitePathLink" in tags:
				if(
					(tilt is not None and tilt > 5)
					or (slope is not None and slope > 8)
					or (width is not None and width < 0.9)
					or flooringMaterial in ["Gravel", "Sand", "Uneven"]
					or flooring == "Hazardous"
					or hasObstacleNode
					or hasKerbHeight004Node
					or hasRaisedKerbNode
					or hasJunctionUndefinedKerbNode
				):
					wheelchairAccess.text = "false"
				elif (
					tilt is not None and tilt <= 2
					and slope is not None and slope <= 5
					and width is not None and width >= 1.4
					and flooringMaterial not in [None, "Gravel", "Sand", "Uneven"]
					and flooring not in [None, "Hazardous", "Discomfortable"]
					and not hasObstacleNode
					and not hasKerbHeight002Node
				):
					wheelchairAccess.text = "true"
				elif (
					tilt is not None and tilt <= 5
					and slope is not None and slope <= 8
					and width is not None and width >= 0.9
					and flooringMaterial not in [None, "Gravel", "Sand", "Uneven"]
					and flooring not in [None, "Hazardous"]
					and not hasObstacleNode
					and not hasKerbHeight004Node
				):
					wheelchairAccess.text = "partial"

			elif tags.get("ParkingBay") == "Disabled":
				if (
					(tilt is not None and tilt > 2)
					or (slope is not None and slope > 2)
					or (width is not None and width < 3.3)
					or flooringMaterial in ["Gravel", "Sand", "Uneven"]
					or flooring == "Hazardous"
					or stepFree == "No"
				):
					wheelchairAccess.text = "false"

				elif (
					tilt is not None and tilt <= 2
					and slope is not None and slope <= 2
					and width is not None and width >= 3.3
					and flooringMaterial not in [None, "Gravel", "Sand", "Uneven"]
					and flooring not in [None, "Hazardous", "Discomfortable"]
					and stepFree == "Yes"
				):
					wheelchairAccess.text = "true"
			
			elif "Entrance" in tags:
				if (
					(width is not None and width < 0.8)
					or (force is not None and force > 50)
					or (kerb is not None and kerb > 0.04)
				):
					wheelchairAccess.text = "false"
				elif (
					width is not None and width >= 0.8
					and kerb is not None and kerb < 0.02
					and (
						(force is not None and force <= 50)
						or tags.get("Door") == "None"
						or tags.get("EntrancePassage") == "Opening"
					)
				):
					wheelchairAccess.text = "true"

			# StepFreeAccess
			sfa = None
			if "SitePathLink" in tags:
				if tags.get("SitePathLink") == "Stairs":
					sfa = "false"
				elif tags.get("SitePathLink") in ["Escalator", "Travelator", "Elevator", "Ramp"]:
					sfa = "true"
				elif hasObstacleNode or hasJunctionNode:
					if (
						hasKerbNode
						or hasKerbHeight004Node
						or hasRaisedKerbNode
						or hasJunctionUndefinedKerbNode
					):
						sfa = "false"
					elif not hasKerbHeight002Node:
						sfa = "true"
					elif not hasKerbHeight004Node:
						sfa = "partial"
						validityConditions.append("Présence de marches de moins de 4 cm")
					else:
						sfa = "unknown"
				else:
					sfa = "true"

			elif "StopPlace" in tags or tags.get("ParkingBay") == "Disabled":
				sfa = wdm3StatesToNetex(tags.get("StepFreeAccess"))

			elif "Entrance" in tags:
				kh = tags.get("KerbHeight")
				kcw = tags.get("KerbCut:Width")
				sfa = "unknown"

				if isValidPositiveFloat(kh, allowZero=True):
					khv = float(kh)
					if khv > 0.04:
						sfa = "false"
					elif khv < 0.02:
						sfa = "true"
					else:
						sfa = "partial"
						validityConditions.append("Présence d'une marche de moins de 4 cm")
				elif isValidPositiveFloat(kcw):
					sfa = "partial"
					validityConditions.append("Présence d'une abaissé de trottoir de taille inconnue")
			
			if sfa is not None:
				limitations.append(E.StepFreeAccess(sfa))

			# EscalatorFreeAccess
			if tags.get("SitePathLink") == "Escalator":
				limitations.append(E.EscalatorFreeAccess("false"))
				wheelchairAccess.text = "false"
			elif "SitePathLink" in tags:
				limitations.append(E.EscalatorFreeAccess("true"))
			elif "StopPlace" in tags:
				limitations.append(E.EscalatorFreeAccess(wdm3StatesToNetex(tags.get("EscalatorFreeAccess"))))

			# LiftFreeAccess
			if tags.get("SitePathLink") == "Elevator":
				limitations.append(E.LiftFreeAccess("false"))
			elif "SitePathLink" in tags:
				limitations.append(E.LiftFreeAccess("true"))
			elif "StopPlace" in tags:
				limitations.append(E.LiftFreeAccess(wdm3StatesToNetex(tags.get("LiftFreeAccess"))))

			# AudibleSignalsAvailable
			if tags.get("SitePathLink") is not None and tags["SitePathLink"] not in ["Hall", "Corridor", "Crossing", "Elevator"]:
				pass

			elif tags.get("SitePathLink") == "Elevator":
				limitations.append(E.AudibleSignalsAvailable(wdm3StatesToNetex(tags.get("AudioAnnouncements"))))

			elif tags.get("SitePathLink") == "Crossing":
				aca = tags.get("AcousticCrossingAids")
				asa = "unknown"

				if aca == "None":
					asa = "false"
				elif aca in ["Good", "Worn"]:
					asa = "true"
				elif aca == "Discomfortable":
					asa = "partial"
					validityConditions.append("Répétiteurs sonores avec dégradatation entrainant une difficulté d'usage ou un inconfort")
				elif aca == "Hazardous":
					asa = "partial"
					validityConditions.append("Répétiteurs sonores avec dégradatation entrainant un problème de sécurité immédiat")

				limitations.append(E.AudibleSignalsAvailable(asa))

			elif "StopPlace" in tags and "AudioInformation" in tags:
				asval = wdm3StatesToNetex(tags["AudioInformation"])
				limitations.append(E.AudibleSignalsAvailable(asval))
				if asval == "partial":
					validityConditions.append("Signalétique auditive non accessible malentendants")

			elif "AudibleSignals" in tags:
				asval = wdm3StatesToNetex(tags["AudibleSignals"])
				limitations.append(E.AudibleSignalsAvailable(asval))
				if "StopPlace" in tags and asval == "partial":
					validityConditions.append("Signalétique auditive dépendante du quai")

			# Fallback
			elif "StopPlace" in tags or "Quay" in tags:
				limitations.append(E.AudibleSignalsAvailable("unknown"))

			# VisualSignsAvailable
			if tags.get("SitePathLink") is not None and tags["SitePathLink"] not in ["Hall", "Corridor", "Crossing", "Elevator"]:
				pass

			elif tags.get("SitePathLink") == "Elevator":
				limitations.append(E.VisualSignsAvailable(wdm3StatesToNetex(tags.get("VisualDisplays"))))

			elif tags.get("SitePathLink") == "Crossing":
				vsa = "unknown"
				zebraCrossing = tags.get("ZebraCrossing")
				if zebraCrossing == "None":
					vsa = "false"
				elif zebraCrossing in ["Good", "Worn"]:
					vsa = "true"
				elif zebraCrossing == "Discomfortable":
					vsa = "partial"
					validityConditions.append("Marquage au sol avec dégradation entraînant une difficulté d’usage ou d’inconfort")
				elif zebraCrossing == "Hazardous":
					vsa = "partial"
					validityConditions.append("Marquage au sol avec dégradation entraînant un problème de sécurité immédiat")
				limitations.append(E.VisualSignsAvailable(vsa))

			elif "StopPlace" in tags:
				vsa = wdm3StatesToNetex(tags.get("VisualSigns"))
				limitations.append(E.VisualSignsAvailable(vsa))
				if vsa == "partial":
					validityConditions.append("Signalétique visuelle dépendante du quai")

			elif tags.get("ParkingBay") == "Disabled":
				pv = tags.get("ParkingVisibility")
				if pv == "Unmarked":
					limitations.append(E.VisualSignsAvailable("false"))
				elif pv == "Demarcated":
					limitations.append(E.VisualSignsAvailable("true"))
				elif pv is not None:
					limitations.append(E.VisualSignsAvailable("partial"))
					if pv == "SignageOnly":
						validityConditions.append("Signalétique présente mais pas de marquage au sol")
					elif pv == "MarkingOnly":
						validityConditions.append("Marquage au sol présent mais pas de signalétique")
					elif pv == "Docks":
						validityConditions.append("Démarquation physique uniquement")
				else:
					limitations.append(E.VisualSignsAvailable("unknown"))
			
			elif "VisualSigns" in tags:
				limitations.append(E.VisualSignsAvailable(wdm3StatesToNetex(tags["VisualSigns"])))
			
			# Fallback
			elif "Quay" in tags:
				limitations.append(E.VisualSignsAvailable("unknown"))


			#
			# Mobility impaired access
			#

			accessValues = set(limitations.xpath("/descendant::*/text()"))
			mia = "partial"

			if "unknown" in accessValues:
				mia = "unknown"
			elif len(accessValues) == 1 and "false" in accessValues:
				mia = "false"
			elif len(accessValues) == 1 and "true" in accessValues:
				mia = "true"

			res.append(E.MobilityImpairedAccess(mia))
			res.append(E.limitations(limitations))


		# Complete validity descriptions
		if len(validityConditions) == 0 and len([ k for k in mappings.WDM_ACCESS_MAIN_TAGS if k in tags and tags[k] == "Limited"]) > 0:
			validityConditions = ["Accessibilité partielle non détaillée"]

		if len(validityConditions) == 0:
			res.remove(resValidCond)
		else:
			resValidDesc.text = " - ".join(validityConditions)


	return res


def distance(start, end):
	"""Computes distance between start and end coordinates using Haversine formula

	Parameters
	----------
	start : list
		{lat, lon} coordinates
	end : list
		{lat, lon} coordinates

	Returns
	-------
	float
		Distance between coordinates, in meters
	"""

	return haversine((start["lat"], start["lon"]), (end["lat"], end["lon"]), unit = Unit.METERS)


def anglePctToDegrees(valPct):
	"""Transforms an angle value in % into same angle in degrees (rounded to next integer)"""
	return math.ceil(round(360 / (2 * math.pi) * 100 * math.atan(valPct / 100)) / 100)


def combineTags(t1, t2, mandatoryKeys=[]):
	"""Appends tags from t2 to merge with t1."""

	if mandatoryKeys is None:
		mandatoryKeys = []

	for k2 in t2.keys() | mandatoryKeys:
		v2 = t2.get(k2)
		if k2 in t1:
			v1 = t1[k2]
			if isinstance(v1, set):
				v1.add(v2)
			elif isinstance(v1, str):
				if v1 != v2:
					t1[k2] = set([v1, v2])
		elif k2 in mandatoryKeys:
			if v2 is not None:
				t1[k2] = set([None, v2])
		else:
			t1[k2] = v2
	
	return t1


def getIntegerTag(tags: Dict[str,str], key: str) -> Optional[int]:
	"""Returns integer value read from tag if tag exist and value is a valid integer"""

	val = getFloatTag(tags, key)
	if val is not None:
		if val == round(val):
			return int(round(val))
	return None


def getFloatTag(tags: Dict[str,str], key: str) -> Optional[float]:
	"""Returns integer value read from tag if tag exist and value is a valid integer"""

	try:
		val = tags.get(key)
		if val is not None:
			return float(val)
	except ValueError:
		pass

	return None
