#
# Converts WDM dataset into Netex
#

from lxml import etree
from lxml.builder import E
import datetime
from . import model, mappings, features, equipments, properties


def readWdmFromXML(inputPath: str):
	"""Parses a WDM XML file

	Parameters
	----------
	inputPath : str
		Path to XML input file

	Returns
	-------
	model.WalkDataset
		Walk dataset read from file
	"""

	with open(inputPath, 'r') as inputFile:
		xml = etree.parse(inputFile)
		wds = model.WalkDataSet()

		# Read nodes
		for node in xml.iter('node'):
			wdsNode = model.Node(properties.getTagsFromWDMFeature(node), node.attrib)
			wds.addNode(wdsNode)

			# Various equipments
			if (
				wdsNode.tags.get("SitePathLink") == "Crossing"
				or wdsNode.tags.get("Obstacle") in ["Toilets", "Shelter", "RoughSurface", "Bench"]
				or wdsNode.tags.get("Amenity") in [
					"Toilets", "TicketOffice", "TicketVendingMachine", "TicketValidator", "LuggageLocker",
					"TrolleyStand", "MeetingPoint", "EmergencyPhone", "RubbishDisposal", "PostBox",
					"ReceptionDesk", "Shelter", "Sign", "Seating"
				]
				or wdsNode.tags.get("TicketVendingMachine") == "Yes"
				or wdsNode.tags.get("Shelter") == "Yes"
				or wdsNode.tags.get("Seating") == "Yes"
				or wdsNode.tags.get("RubbishDisposal") == "Yes"
				or wdsNode.tags.get("TicketValidator") == "Yes"
			):
				wds.addEquipment(wdsNode)

			# Node elevators
			if wdsNode.tags.get("SitePathLink") == "Elevator":
				wds.addSitePathLink(wdsNode)
			
			# Entrances
			if wdsNode.tags.get("Entrance") is not None:
				wds.addEntrance(wdsNode)

			# Quays
			if wdsNode.tags.get("Quay") is not None:
				wds.addQuay(wdsNode)
			
			# Stop Places
			if "StopPlace" in wdsNode.tags:
				wds.addStopPlace(wdsNode)
			
			# Parking bays
			if wdsNode.tags.get("ParkingBay") == "Disabled":
				wds.addParkingBay(wdsNode)
			
			# Points of interest
			if wdsNode.tags.get("PointOfInterest") is not None:
				wds.addPointOfInterest(wdsNode)
			
			# Path Junctions
			if (
				wdsNode.tags.get("PathJunction") is not None
				or (wdsNode.tags.get("Obstacle") is not None and wdsNode.tags.get("Amenity") is None)
			):
				wds.addPathJunction(wdsNode)

		# Read site path links
		for splw in xml.findall('way/tag[@k="SitePathLink"]..'):
			nodes = [ n.attrib["ref"] for n in splw.iter("nd") ]
			wds.addSitePathLink(model.Way(properties.getTagsFromWDMFeature(splw), splw.attrib, nodes))
		
		# Read polygon quays
		for qw in xml.findall('way/tag[@k="InsideSpace"][@v="Quay"]..') + xml.findall('way/tag[@k="Quay"]..'):
			nodes = [ n.attrib["ref"] for n in qw.iter("nd") ]
			wds.addQuay(model.Way(properties.getTagsFromWDMFeature(qw), qw.attrib, nodes))

		# Read polygon POIs
		for spw in xml.findall('way/tag[@k="PointOfInterest"]..'):
			nodes = [ n.attrib["ref"] for n in spw.iter("nd") ]
			wdmFeature = model.Way(properties.getTagsFromWDMFeature(spw), spw.attrib, nodes)
			wds.addPointOfInterest(wdmFeature)
			if wdmFeature.tags.get("StopPlace") is not None:
				wds.addStopPlace(wdmFeature)
		
		# Read stop places relations
		for spr in xml.findall('relation/tag[@k="StopPlace"]..'):
			members = [ model.RelationMember(
				m.attrib["role"],
				m.attrib["type"],
				m.attrib["ref"]
			) for m in spr.iter("member") ]
			wds.addStopPlace(model.Relation(properties.getTagsFromWDMFeature(spr), spr.attrib, members))

		wds.postProcessDataset()

		return wds


def writeWdmIntoNetex(wds: model.WalkDataSet, output: str, pubts: datetime.datetime = datetime.datetime.now()):
	"""Writes a walk dataset into a Netex XML file

	Parameters
	----------
	wds : model.WalkDataSet
		Walk dataset to convert
	output : str
		File path to write XML in
	pubts : datetime.datetime
		Publication date to set in metadata
	"""

	# XML root
	root = etree.Element("PublicationDelivery", nsmap=mappings.NS_NETEX)
	root.attrib["version"] = "1.09:FR-NETEX_ACCESSIBILITE-2.1-0.0"
	root.attrib["{"+mappings.NS_NETEX["xsi"]+"}schemaLocation"] = "http://www.netex.org.uk/netex"

	# File metadata
	root.append(E.PublicationTimestamp(pubts.isoformat()))
	root.append(E.ParticipantRef("YukaimapsWDM2Netex"))
	root.append(E.Description(
		"Export Yukaimaps des données d'accessibilité et de cheminement - Cet export contient des données issues d'OpenStreetMap (© contributeurs OpenStreetMap)."
		if wds.hasOsmData else
		"Export Yukaimaps des données d'accessibilité et de cheminement"
	))


	#
	# Data parts
	#

	members = etree.Element("members")
	# Property order heres determines how features are sorted in final XML
	# DO NOT CHANGE IT
	membersByType = {
		"Quay": [],
		"StopPlace": [],
		"PointOfInterest": [],
		"Entrance": [],
		"SitePathLink": [],
		"Equipment": [],
		"AccessSpace": [],
		"PointOfInterestSpace": [],
		"ParkingBay": [],
		"PathJunction": [],
	}
	netexMembersByWdsId = {
		"Quay": {},
		"StopPlace": {},
		"Entrance": {},
		"ParkingBay": {},
		"PointOfInterest": {},
		"SitePathLink": {},
		"PathJunction": {},
		"Equipment": {},
	}
	equipmentPlacesByWdsId = {
		"EntranceEquipment": {},
		"CrossingEquipment": {},
		"ShelterEquipment": {},
		"SanitaryEquipment": {},
		"TicketingEquipment": {},
		"TicketValidatorEquipment": {},
		"LuggageLockerEquipment": {},
		"TrolleyStandEquipment": {},
		"MeetingpointService": {},
		"PassengerSafetyEquipment": {},
		"RubbishDisposalEquipment": {},
		"CommunicationService": {},
		"AssistanceService": {},
		"RoughSurface": {},
		"GeneralSign": {},
		"SeatingEquipment": {},
	}
	placeEquipmentsByWdsSplId = {}
	autoIncObjId = 0

	# Entrances
	for eId in wds.entrances:
		e = wds.entrances[eId]

		# EntranceEquipment
		ntxEq = equipments.entranceEquipmentToXml(wds, e)
		membersByType["Equipment"].append(ntxEq)

		# Entrance
		ntxE = features.entranceToXml(wds, e, autoIncObjId, ntxEq)
		membersByType["Entrance"].append(ntxE)
		netexMembersByWdsId["Entrance"][eId] = ntxE.attrib
		autoIncObjId += 1
		equipmentPlacesByWdsId["EntranceEquipment"][eId] = { "feature": ntxE, "equipment": ntxEq }

	# Parking bays
	for pbId in wds.parkingBays:
		pb = wds.parkingBays[pbId]
		ntxPoi = features.parkingBayToXml(wds, pb, autoIncObjId)
		membersByType["ParkingBay"].append(ntxPoi)
		netexMembersByWdsId["ParkingBay"][pbId] = ntxPoi.attrib
		autoIncObjId += 1

	# Equipments (non-SitePathLinks, or at least non-linear SitePathLinks)
	for eqId in wds.nonSplEquipments:
		eq = wds.nonSplEquipments[eqId]

		if eq.tags.get("SitePathLink") == "Crossing":
			ntxC = equipments.crossingEquipmentToXml(wds, eq)
			membersByType["Equipment"].append(ntxC)
			equipmentPlacesByWdsId["CrossingEquipment"][eqId] = { "feature": None, "equipment": ntxC }
			netexMembersByWdsId["Equipment"][eqId] = ntxC.attrib

		elif eq.tags.get("SitePathLink") == "Elevator":
			membersByType["Equipment"].append(equipments.liftEquipmentToXml(wds, eq))
		
		if eq.tags.get("Obstacle") == "Shelter" or eq.tags.get("Amenity") == "Shelter" or eq.tags.get("Shelter") == "Yes":
			ntxEq = equipments.shelterEquipmentToXml(eq)
			membersByType["Equipment"].append(ntxEq)
			equipmentPlacesByWdsId["ShelterEquipment"][eqId] = {
				"feature": None,
				"equipment": ntxEq,
				"geom": properties.getGeometryXml(wds, eq, directLocation=True)
			}
			netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib
			autoIncObjId += 1

		if eq.tags.get("Obstacle") == "Toilets" or eq.tags.get("Amenity") == "Toilets":
			ntxSEq = equipments.sanitaryEquipmentToXml(wds, eq, autoIncObjId)
			membersByType["Equipment"].append(ntxSEq)
			equipmentPlacesByWdsId["SanitaryEquipment"][eqId] = {
				"feature": None,
				"equipment": ntxSEq,
				"geom": properties.getGeometryXml(wds, eq, directLocation=True)
			}
			netexMembersByWdsId["Equipment"][eqId] = ntxSEq.attrib
			autoIncObjId += 1
		
		if eq.tags.get("Obstacle") == "RoughSurface":
			ntxEq = equipments.roughSurfaceToXml(eq)
			membersByType["Equipment"].append(ntxEq)
			equipmentPlacesByWdsId["RoughSurface"][eqId] = {
				"feature": None,
				"equipment": ntxEq,
				"geom": properties.getGeometryXml(wds, eq, directLocation=True)
			}
			netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib
		
		if eq.tags.get("Amenity") == "Sign":
			ntxEq = equipments.generalSignToXml(eq)
			membersByType["Equipment"].append(ntxEq)
			equipmentPlacesByWdsId["GeneralSign"][eqId] = {
				"feature": None,
				"equipment": ntxEq,
				"geom": properties.getGeometryXml(wds, eq, directLocation=True)
			}
			netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib
		
		if eq.tags.get("Amenity") == "Seating" or eq.tags.get("Obstacle") == "Bench" or eq.tags.get("Seating") == "Yes":
			ntxEq = equipments.seatingEquipmentToXml(eq)
			membersByType["Equipment"].append(ntxEq)
			equipmentPlacesByWdsId["SeatingEquipment"][eqId] = {
				"feature": None,
				"equipment": ntxEq,
				"geom": properties.getGeometryXml(wds, eq, directLocation=True)
			}
			netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib

		if eq.tags.get("Amenity") in ["TicketOffice", "TicketVendingMachine"] or eq.tags.get("TicketVendingMachine") == "Yes":
			ntxEq = equipments.ticketingEquipmentToXml(eq)
			membersByType["Equipment"].append(ntxEq)
			equipmentPlacesByWdsId["TicketingEquipment"][eqId] = {
				"feature": None,
				"equipment": ntxEq,
				"geom": properties.getGeometryXml(wds, eq, directLocation=True)
			}
			netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib

		if eq.tags.get("Amenity") == "TrolleyStand":
			ntxEq = equipments.trolleyStandEquipmentToXml(eq)
			membersByType["Equipment"].append(ntxEq)
			equipmentPlacesByWdsId["TrolleyStandEquipment"][eqId] = {
				"feature": None,
				"equipment": ntxEq,
				"geom": properties.getGeometryXml(wds, eq, directLocation=True)
			}
			netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib
		
		# TODO : re-add MeetingpointService when Netex XSD is fixed
		# if eq.tags.get("Amenity") == "MeetingPoint":
		# 	ntxEq = equipments.genericEquipmentToXml("MeetingpointService", eq)
		# 	membersByType["Equipment"].append(ntxEq)
		# 	equipmentPlacesByWdsId["MeetingpointService"][eqId] = {
		# 		"feature": None,
		# 		"equipment": ntxEq,
		# 		"geom": properties.getGeometryXml(wds, eq, directLocation=True)
		# 	}
		# 	netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib
		
		if eq.tags.get("Amenity") == "EmergencyPhone":
			ntxEq = equipments.genericEquipmentToXml("PassengerSafetyEquipment", eq)
			membersByType["Equipment"].append(ntxEq)
			equipmentPlacesByWdsId["PassengerSafetyEquipment"][eqId] = {
				"feature": None,
				"equipment": ntxEq,
				"geom": properties.getGeometryXml(wds, eq, directLocation=True)
			}
			netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib
		
		if eq.tags.get("Amenity") == "PostBox":
			ntxEq = equipments.communicationServiceToXml(eq)
			membersByType["Equipment"].append(ntxEq)
			equipmentPlacesByWdsId["CommunicationService"][eqId] = {
				"feature": None,
				"equipment": ntxEq,
				"geom": properties.getGeometryXml(wds, eq, directLocation=True)
			}
			netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib
		
		if eq.tags.get("Amenity") in ["ReceptionDesk", "TicketOffice"]:
			ntxEq = equipments.assistanceServiceToXml(eq)
			membersByType["Equipment"].append(ntxEq)
			equipmentPlacesByWdsId["AssistanceService"][eqId] = {
				"feature": None,
				"equipment": ntxEq,
				"geom": properties.getGeometryXml(wds, eq, directLocation=True)
			}
			netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib
		
		# TODO : re-add LuggageLocker after Netex XSD fix
		for amenityVal in ["TicketValidator", "RubbishDisposal"]:
			if eq.tags.get("Amenity") == amenityVal or eq.tags.get(amenityVal) == "Yes":
				ntxEq = equipments.genericEquipmentToXml(amenityVal+"Equipment", eq)
				membersByType["Equipment"].append(ntxEq)
				equipmentPlacesByWdsId[amenityVal+"Equipment"][eqId] = {
					"feature": None,
					"equipment": ntxEq,
					"geom": properties.getGeometryXml(wds, eq, directLocation=True)
				}
				netexMembersByWdsId["Equipment"][eqId] = ntxEq.attrib

	# Quays
	for qId in wds.quays:
		q = wds.quays[qId]
		ntxQ = features.quayToXml(wds, q, autoIncObjId, equipmentPlacesByWdsId)
		netexMembersByWdsId["Quay"][qId] = ntxQ.attrib
		membersByType["Quay"].append(ntxQ)
		autoIncObjId += 1

	# Path junctions
	for pjId in wds.pathJunctions:
		pj = wds.pathJunctions[pjId]
		ntxPj = features.pathJunctionToXml(wds, pj)
		membersByType["PathJunction"].append(ntxPj)
		netexMembersByWdsId["PathJunction"][pjId] = ntxPj.attrib

	# SitePathLinks
	for splId in wds.sitePathLinks:
		netexMembersByWdsId["SitePathLink"][splId] = [] # Many SPL created in Netex for a single WDM feature
		spl = wds.sitePathLinks[splId]

		# Append SPL as equipment
		placeEquipments = {}
		if spl.tags.get("SitePathLink") == "Crossing":
			ceq = equipments.crossingEquipmentToXml(wds, spl)
			placeEquipments["CrossingEquipment"] = ceq.attrib["id"]
			membersByType["Equipment"].append(ceq)
			equipmentPlacesByWdsId["CrossingEquipment"][splId] = { "feature": None, "equipment": ceq }

		elif spl.tags.get("SitePathLink") == "Ramp":
			eq = equipments.rampEquipmentToXml(wds, splId)
			placeEquipments["RampEquipment"] = eq.attrib["id"]
			membersByType["Equipment"].append(eq)

		elif spl.tags.get("SitePathLink") == "Escalator":
			eq = equipments.escalatorEquipmentToXml(wds, splId)
			placeEquipments["EscalatorEquipment"] = eq.attrib["id"]
			membersByType["Equipment"].append(eq)

		elif spl.tags.get("SitePathLink") == "Travelator":
			eq = equipments.travelatorEquipmentToXml(wds, splId)
			placeEquipments["TravelatorEquipment"] = eq.attrib["id"]
			membersByType["Equipment"].append(eq)

		elif spl.tags.get("SitePathLink") == "Elevator":
			eq = equipments.liftEquipmentToXml(wds, spl)
			placeEquipments["LiftEquipment"] = eq.attrib["id"]
			membersByType["Equipment"].append(eq)

		elif spl.tags.get("SitePathLink") == "QueueManagement":
			eq = equipments.queueingEquipmentToXml(wds, splId)
			placeEquipments["QueueingEquipment"] = eq.attrib["id"]
			membersByType["Equipment"].append(eq)

		elif spl.tags.get("SitePathLink") == "Stairs":
			eq = equipments.staircaseEquipmentToXml(wds, spl)
			placeEquipments["StaircaseEquipment"] = eq.attrib["id"]
			membersByType["Equipment"].append(eq)
			# Note that it doesn't belong to equipmentPlaces,
			#  which is only for Obstacle=Kerb on SitePathLinks

		# Way management
		if isinstance(spl, model.Way):
			# Check if SitePathLink needs a Level duplication
			splLevels = spl.tags.get("Level")
			splVal = spl.tags.get("SitePathLink")
			ntxLevels = [None]
			if (
				splVal not in ["Stairs", "Elevator", "Escalator"]
				and splLevels is not None
				and ";" in splLevels
			):
				ntxLevels = splLevels.split(";")
			
			for ntxLevel in ntxLevels:
				# Check if SitePathLink needs to be split on current node
				currentStartNodePos = 0
				currentNodePos = 1
				currentEquipmentPlaces = []
				maxNodePos = len(spl.nodes) - 1

				while currentNodePos < maxNodePos:
					currentNode = wds.nodes.get(spl.nodes[currentNodePos])

					# Check if current node is also an equipment
					for eqType in [
						"EntranceEquipment", "CrossingEquipment", "SanitaryEquipment", "TicketingEquipment",
						"RoughSurface", "GeneralSign", "ShelterEquipment", "TicketValidatorEquipment",
						"TrolleyStandEquipment", "PassengerSafetyEquipment", "RubbishDisposalEquipment",
						"SeatingEquipment", # TODO : re-enable "LuggageLockerEquipment" & "MeetingpointService"
					]:
						if currentNode.getId() in equipmentPlacesByWdsId[eqType]:
							eqp = equipmentPlacesByWdsId[eqType][currentNode.getId()]
							currentEquipmentPlaces.append({ "type": eqType, **eqp })
					
					# Check if current node is a Kerb -> create an equipmentPlace
					if currentNode.tags.get("Obstacle") == "Kerb":
						eq = equipments.staircaseEquipmentToXml(wds, currentNode)
						membersByType["Equipment"].append(eq)
						currentEquipmentPlaces.append({
							"type": "StaircaseEquipment",
							"feature": None,
							"equipment": eq,
						})

					# Apply way splitting on entrance/elavator/sitepathlink
					if currentNode is not None and (
						"Entrance" in currentNode.tags
						or currentNode.tags.get("SitePathLink") == "Elevator"
						# TODO : OR intersection node with another SitePathLink
					):
						ntxSpl = features.sitePathLinkToXml(
							wds, splId, autoIncObjId, netexMembersByWdsId, placeEquipments,
							equipmentPlaces = currentEquipmentPlaces,
							nodesPos = [currentStartNodePos, currentNodePos],
							levels=[ntxLevel],
						)
						netexMembersByWdsId["SitePathLink"][splId].append(ntxSpl.attrib)
						membersByType["SitePathLink"].append(ntxSpl)

						autoIncObjId += 1
						currentStartNodePos = currentNodePos
						currentEquipmentPlaces = []

					currentNodePos += 1

				# Append last segment
				ntxSpl = features.sitePathLinkToXml(
					wds, splId, autoIncObjId, netexMembersByWdsId, placeEquipments,
					equipmentPlaces = currentEquipmentPlaces,
					nodesPos = [currentStartNodePos, maxNodePos],
					levels=[ntxLevel],
				)
				netexMembersByWdsId["SitePathLink"][splId].append(ntxSpl.attrib)
				membersByType["SitePathLink"].append(ntxSpl)
				placeEquipmentsByWdsSplId[splId] = placeEquipments
				autoIncObjId += 1

		# Node management
		elif isinstance(spl, model.Node):
			# Elevators with level-handling
			if spl.tags.get("SitePathLink") == "Elevator":
				try:
					levels = sorted([ float(l) for l in (spl.tags.get("Level") or "0;1").split(";") ])

					for i in range(len(levels) - 1):
						minLevel = levels[i]
						maxLevel = levels[i+1]
						ntxSpl = features.sitePathLinkToXml(
							wds, splId, autoIncObjId, netexMembersByWdsId,
							placeEquipments, levels = [minLevel, maxLevel]
						)
						netexMembersByWdsId["SitePathLink"][splId].append(ntxSpl.attrib)
						membersByType["SitePathLink"].append(ntxSpl)
						autoIncObjId += 1
				except ValueError:
					pass

	# Stop Places
	for i, spContainer in enumerate([wds.multiModalStopPlaces, wds.stopPlaces]):
		isMonoModal = i == 1
		
		for spId in spContainer:
			sp = spContainer[spId]
			ntxSp = features.stopPlaceToXml(
				wds, sp, autoIncObjId, netexMembersByWdsId, isMonoModal,
				equipmentPlacesByWdsId, placeEquipmentsByWdsSplId
			)
			netexMembersByWdsId["StopPlace"][sp.getId()] = ntxSp.attrib
			membersByType["StopPlace"].append(ntxSp)
			autoIncObjId += 1

	# Points of Interest
	for poiId in wds.pointsOfInterest:
		poi = wds.pointsOfInterest[poiId]
		ntxPoi = features.pointOfInterestToXml(
			wds, poi, autoIncObjId, netexMembersByWdsId,
			equipmentPlacesByWdsId, placeEquipmentsByWdsSplId
		)
		netexMembersByWdsId["PointOfInterest"][poi.getId()] = ntxPoi.attrib
		membersByType["PointOfInterest"].append(ntxPoi)
		autoIncObjId += 1

	# Post-process : entrances, quays & sitepathlinks' SiteRefs
	for memberType in ["Entrance", "Quay", "SitePathLink"]:
		for ntxE in membersByType[memberType]:
			siteref = ntxE.find("SiteRef")
			if siteref is not None:
				siterefwds = siteref.attrib.get("wdsref")
				if siterefwds:
					ntxSiteId = None

					if siterefwds in netexMembersByWdsId["StopPlace"]:
						ntxSiteId = netexMembersByWdsId["StopPlace"][siterefwds]
					elif memberType != "Quay" and siterefwds in netexMembersByWdsId["PointOfInterest"]:
						ntxSiteId = netexMembersByWdsId["PointOfInterest"][siterefwds]

					if ntxSiteId is not None:
						siteref.attrib["ref"] = ntxSiteId["id"]
						siteref.attrib["version"] = ntxSiteId["version"]
						del siteref.attrib["wdsref"]
					else:
						siteref.getparent().remove(siteref)
				else:
					siteref.getparent().remove(siteref)


	# Add members sorted to their category
	for mc in membersByType:
		addedMembersIds = []
		for m in membersByType[mc]:
			if m.attrib["id"] not in addedMembersIds:
				members.append(m)
				addedMembersIds.append(m.attrib["id"])


	# Dataset wrapper
	dataObjects = etree.SubElement(root, "dataObjects")
	GeneralFrame = etree.SubElement(dataObjects, "GeneralFrame", id = "FR:GeneralFrame:NETEX_ACCESSIBILITE:", version = "any")
	GeneralFrame.append(E.FrameDefaults(E.DefaultLocationSystem("EPSG:4326")))
	if len(members) > 0:
		# DataSource
		if wds.hasOsmData:
			members.append(E.DataSource(
				E.Name("OpenStreetMap"),
				E.Description("OpenStreetMap est un ensemble de données ouvertes, disponible sous la licence libre ODbL accordée par la Fondation OpenStreetMap"),
				E.DataLicenceCode(ref="ODbL"),
				E.DataLicenceUrl("https://wiki.osmfoundation.org/wiki/Licence"),
				id="FR:Yukaimaps:DataSource:OpenStreetMap",
				version="any"
			))

		GeneralFrame.append(members)

	# Writing to file
	et = etree.ElementTree(root)
	et.write(output, encoding="utf-8", pretty_print=True, xml_declaration=True)
