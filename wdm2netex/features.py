#
# Creates Netex features
#

from lxml import etree
from lxml.builder import E
from . import model, mappings, equipments, properties


def entranceToXml(wds, entrance, objId, entranceEquipment):
	"""Transforms a single Entrance into Netex format"""

	entranceNtx = E.Entrance() if entrance.tags.get("Entrance") == "StopPlace" else E.PointOfInterestEntrance()
	properties.setNetexIdVersion(entranceNtx, entrance, "Entrance")

	# KeyList, Description
	properties.appendKeyList(entranceNtx, entrance)
	properties.appendDirectMappings(entranceNtx, entrance, [
		{ "wdm": "Description", "netex": "Description" }
	])

	properties.appendGeometry(wds, entrance, entranceNtx)
	properties.appendPostalAddress(entranceNtx, entrance.tags, objId)

	# AccessibilityAssessment
	access = properties.wdmTagsToNetexAccess(entrance.tags, objId)
	if access is not None:
		entranceNtx.append(access)

	# SiteRef
	if entrance.getId() in wds.entrancesSiteRefs:
		entranceNtx.append(E.SiteRef(
			wdsref=wds.entrancesSiteRefs[entrance.getId()]
		))

	properties.appendPlaceEquipments(entranceNtx, { "EntranceEquipment": entranceEquipment.attrib["id"] })

	# PublicCode, Label
	properties.appendDirectMappings(entranceNtx, entrance, [
		{ "wdm": "PublicCode", "netex": "PublicCode" },
		{ "wdm": "Name", "netex": "Label" },
	])

	# EntranceType
	ep = entrance.tags.get("EntrancePassage")
	door = entrance.tags.get("Door")
	autodoor = entrance.tags.get("AutomaticDoor")
	entranceType = None

	if ep == "Opening":
		entranceType = "opening"
	elif ep in ["Gate", "Barrier"]:
		entranceType = "gate"
	elif ep == "Door":
		if autodoor == "Yes":
			entranceType = "automaticDoor"
		elif door == "Revolving":
			entranceType = "revolvingDoor"
		elif door == "Swing":
			entranceType = "swingDoor"
		else:
			entranceType = "door"
	
	if entranceType is not None:
		entranceNtx.append(E.EntranceType(entranceType))

	# IsEntry, IsExit
	properties.appendDirectMappings(entranceNtx, entrance, [
		{ "wdm": "IsEntry", "netex": "IsEntry", "mapping": mappings.MAPPING_YESNOSTAREMPTY_TRUEFALSEEMPTYTRUE },
		{ "wdm": "IsExit", "netex": "IsExit", "mapping": mappings.MAPPING_YESNOSTAREMPTY_TRUEFALSEEMPTYTRUE },
	])

	# Width, Height
	properties.appendPositiveFloat(entranceNtx, entrance, "Width", "Width", 2)
	properties.appendPositiveFloat(entranceNtx, entrance, "Height", "Height", 2)

	return entranceNtx


def parkingBayToXml(wds, parkingBay, objId):
	"""Transforms a single ParkingBay into Netex format"""

	parkingBayNtx = E.ParkingBay()
	properties.setNetexIdVersion(parkingBayNtx, parkingBay, "ParkingBay")

	# KeyList, Description
	properties.appendKeyList(parkingBayNtx, parkingBay)
	properties.appendDirectMappings(parkingBayNtx, parkingBay, [
		{ "wdm": "Description", "netex": "Description" }
	])

	properties.appendGeometry(wds, parkingBay, parkingBayNtx)

	# AccessibilityAssessment
	access = properties.wdmTagsToNetexAccess(parkingBay.tags, objId)
	if access is not None:
		parkingBayNtx.append(access)

	# PublicUse
	parkingBayNtx.append(E.PublicUse("disabledPublicOnly"))

	# Lighting
	properties.appendDirectMappings(parkingBayNtx, parkingBay, [ mappings.MAPPING_RULE_LIGHTING ])

	# ParkingVehicleType
	parkingBayNtx.append(E.ParkingVehicleType("car"))

	# BayGeometry, ParkingVisibility
	properties.appendDirectMappings(parkingBayNtx, parkingBay, [
		{ "wdm": "BayGeometry", "netex": "BayGeometry", "mapping": {
			"Orthogonal": "orthogonal", "Angled": "angled", "Parallel": "parallel",
			"FreeFormat": "freeFormat", "Unspecified": "unspecified", "*": "other"
		}},
		{ "wdm": "ParkingVisibility", "netex": "ParkingVisibility", "mapping": {
			"Unmarked": "unmarked", "SignageOnly": "signageOnly", "MarkingOnly": "demarcated",
			"Demarcated": "demarcated", "Docks": "docks", "*": "other"
		}}
 	])

	# Width, Length
	properties.appendPositiveFloat(parkingBayNtx, parkingBay, "Length", "Length", 2)
	properties.appendPositiveFloat(parkingBayNtx, parkingBay, "Width", "Width", 2)

	# RechargingAvailable
	properties.appendDirectMappings(parkingBayNtx, parkingBay, [
		{ "wdm": "VehicleRecharging", "netex": "RechargingAvailable", "mapping": mappings.MAPPING_YESNONE_TRUEFALSE }
	])

	return parkingBayNtx


def quayToXml(wds, quay, objId, equipmentPlacesByWdsId = {}):
	"""Transforms a single Quay into Netex format"""

	quayNtx = E.Quay()
	properties.setNetexIdVersion(quayNtx, quay, "Quay")

	# KeyList, Name, Description
	properties.appendKeyList(quayNtx, quay)
	properties.appendDirectMappings(quayNtx, quay, [
		{ "wdm": "Name", "netex": "Name" },
		{ "wdm": "Description", "netex": "Description" }
	])


	# Geometry
	properties.appendGeometry(wds, quay, quayNtx)

	# AccessibilityAssessment
	access = properties.wdmTagsToNetexAccess(quay.tags, objId)
	if access is not None:
		quayNtx.append(access)

	properties.appendDirectMappings(quayNtx, quay, [
		mappings.MAPPING_RULE_COVERED,
		mappings.MAPPING_RULE_GATED,
		mappings.MAPPING_RULE_LIGHTING
	])

	# SiteRef
	quayStopPlaceId = wds.quaysSiteRefs.get(quay.getId())
	if quayStopPlaceId is not None:
		quayNtx.append(E.SiteRef(
			wdsref = quayStopPlaceId
		))

	# equipmentPlaces
	properties.findAndAppendAmenityEquipmentPlaces(wds, quay, quayNtx, objId, equipmentPlacesByWdsId)

	# TransportMode, PublicCode
	properties.appendDirectMappings(quayNtx, quay, [
		{ "wdm": "Quay", "netex": "TransportMode", "mapping": mappings.MAPPING_QUAY_TRANSPORT_MODE },
		{ "wdm": "PublicCode", "netex": "PublicCode" }
	])

	return quayNtx


def stopPlaceToXml(wds, sp, objId, netexMembersByWdsId, isMonoModal = True, equipmentPlacesByWdsId = {}, placeEquipmentsByWdsSplId = {}):
	"""Transforms a single StopPlace into Netex format"""

	transportMode = mappings.MAPPING_STOPPLACE_TRANSPORT_MODE.get(sp.tags.get("StopPlace"))
	if transportMode is None and isMonoModal:
		transportMode = "bus"
	
	spNtx = E.StopPlace()
	properties.setNetexIdVersion(spNtx, sp, "StopPlace_"+transportMode if isMonoModal else "StopPlace")

	# Analyses for quays
	quays = None
	combinedMandatoryKeys = ["WheelchairAccess"] if not isMonoModal else []
	combinedTags = properties.combineTags({}, sp.tags, combinedMandatoryKeys)
	if sp.getId() in wds.stopPlacesQuays:
		quays = E.quays()

		for q in wds.stopPlacesQuays[sp.getId()]:
			ntxId = netexMembersByWdsId["Quay"][q]
			quay = wds.quays[q]
			quays.append(E.QuayRef(
				version=ntxId["version"],
				ref=ntxId["id"]
			))
			combinedTags = properties.combineTags(combinedTags, quay.tags, combinedMandatoryKeys)

	# KeyList, Name, Description
	properties.appendKeyList(spNtx, sp)
	properties.appendDirectMappings(spNtx, sp, [
		{ "wdm": "Name", "netex": "Name" },
		{ "wdm": "Description", "netex": "Description" }
	])

	properties.appendGeometry(wds, sp, spNtx, 1)

	# placeTypes
	spNtx.append(E.placeTypes(
		E.TypeOfPlaceRef(ref="monomodalStopPlace" if isMonoModal else "multimodalStopPlace")
	))

	# AccessibilityAssessment
	access = properties.wdmTagsToNetexAccess(combinedTags, objId)
	if access is not None:
		spNtx.append(access)
	
	properties.appendDirectMappings(spNtx, sp, [
		mappings.MAPPING_RULE_LIGHTING
	])

	properties.appendFacilities(wds, spNtx, sp, objId)

	# ParentSiteRef
	if isMonoModal and sp.getId() in wds.parentStopPlace:
		parentNtxId = netexMembersByWdsId["StopPlace"][wds.parentStopPlace[sp.getId()]]
		spNtx.append(E.ParentSiteRef(
			ref=parentNtxId["id"],
			version=parentNtxId["version"]
		))

	# entrances
	if sp.getId() in wds.stopPlacesEntrances and len(wds.stopPlacesEntrances[sp.getId()]) > 0:
		et = E.entrances()

		for eId in wds.stopPlacesEntrances[sp.getId()]:
			eNtxId = netexMembersByWdsId["Entrance"][eId]
			et.append(E.EntranceRef(
				ref=eNtxId["id"],
				version=eNtxId["version"]
			))

		spNtx.append(et)

	# equipmentPlaces, placeEquipments
	properties.findAndAppendAmenityEquipmentPlaces(wds, sp, spNtx, objId, equipmentPlacesByWdsId)
	pathLinks = properties.findAndAppendSplPlaceEquipments(wds, sp, spNtx, netexMembersByWdsId, placeEquipmentsByWdsSplId)

	# TransportMode
	otherTransportModes = None
	if not isMonoModal:
		# Look for child monomodal stop places
		associatedStopPlacesIds = [k for k,v in wds.parentStopPlace.items() if v == sp.getId()]
		associatedStopPlaces = [ wds.stopPlaces[id] for id in associatedStopPlacesIds ]
		otherTransportModes = set([
			mappings.MAPPING_STOPPLACE_TRANSPORT_MODE.get(t)
			for t in list(set([
				sp.tags.get("StopPlace")
				for sp in associatedStopPlaces
			]))
		])

		if transportMode is None:
			# Find transport mode according to quays
			prio = ["air","water","rail","metro","tram","cableway","bus","coach","trolleyBus"]

			for p in prio:
				if p in otherTransportModes:
					transportMode = p
					break
			
			if transportMode is None:
				transportMode = "bus"
		
		otherTransportModes.discard(transportMode)
		
	spNtx.append(E.TransportMode(transportMode))

	# OtherTransportModes
	if not isMonoModal and len(otherTransportModes) > 0:
		spNtx.append(E.OtherTransportModes(" ".join(list(otherTransportModes))))

	# StopPlaceType
	properties.appendDirectMappings(spNtx, sp, [
		{ "wdm": "StopPlace", "netex": "StopPlaceType", "mapping": {
			"OnstreetBus": "onstreetBus",
			"BusStation": "busStation",
			"CoachStation": "coachStation",
			"OnstreetTram": "onstreetTram",
			"TramStation": "tramStation",
			"RailStation": "railStation",
			"MetroStation": "metroStation",
			"Airport": "airport",
			"FerryPort": "ferryPort",
			"LiftStation": "liftStation",
			"*": "other",
			"": "other"
		}}
	])

	# Quays
	if quays is not None:
		spNtx.append(quays)

	# TODO : accessSpaces

	# pathLinks
	if len(pathLinks) > 0:
		spNtx.append(E.pathLinks(*pathLinks))

	return spNtx


def pointOfInterestToXml(wds, poi, objId, netexMembersByWdsId, equipmentPlacesByWdsId = {}, placeEquipmentsByWdsSplId = {}):
	"""Transforms a single PointOfInterest into Netex format"""

	poiNtx = E.PointOfInterest()
	properties.setNetexIdVersion(
		poiNtx, poi, "PointOfInterest",
		forceFallbackId = poi.tags.get("PointOfInterest") == "StopPlace"
	)

	# KeyList, Name, Description
	properties.appendKeyList(poiNtx, poi)
	properties.appendDirectMappings(poiNtx, poi, [
		{ "wdm": "Name", "netex": "Name" },
		{ "wdm": "Description", "netex": "Description" }
	])

	properties.appendGeometry(wds, poi, poiNtx)

	# Url, Image
	properties.appendDirectMappings(poiNtx, poi, [
		{ "wdm": "Website", "netex": "Url" },
		{ "wdm": "Image", "netex": "Image" }
	])

	properties.appendPostalAddress(poiNtx, poi.tags, objId)

	# AccessibilityAssessment
	access = properties.wdmTagsToNetexAccess(poi.tags, objId)
	if access is not None:
		poiNtx.append(access)
	
	# Covered, Gated, Lighting
	properties.appendDirectMappings(poiNtx, poi, [
		mappings.MAPPING_RULE_COVERED,
		mappings.MAPPING_RULE_GATED,
		mappings.MAPPING_RULE_LIGHTING
	])

	properties.appendFacilities(wds, poiNtx, poi, objId)
	
	# entrances
	if poi.getId() in wds.poiEntrances and len(wds.poiEntrances[poi.getId()]) > 0:
		et = E.entrances()

		for eId in wds.poiEntrances[poi.getId()]:
			eNtxId = netexMembersByWdsId["Entrance"][eId]
			et.append(E.EntranceRef(
				ref=eNtxId["id"],
				version=eNtxId["version"]
			))

		poiNtx.append(et)

	# equipmentPlaces, placeEquipments
	properties.findAndAppendAmenityEquipmentPlaces(wds, poi, poiNtx, objId, equipmentPlacesByWdsId)
	pathLinks = properties.findAndAppendSplPlaceEquipments(wds, poi, poiNtx, netexMembersByWdsId, placeEquipmentsByWdsSplId)
	
	# TODO: classifications, spaces

	# pathLinks
	if len(pathLinks) > 0:
		poiNtx.append(E.pathLinks(*pathLinks))

	return poiNtx


def pathJunctionToXml(wds, pj):
	"""Transforms a single PathJunction into Netex format"""

	pjNtx = E.PathJunction()
	properties.setNetexIdVersion(pjNtx, pj, "PathJunction", forceFallbackId=True)
	properties.appendKeyList(pjNtx, pj)
	properties.appendGeometry(wds, pj, pjNtx, directLocation=True)

	return pjNtx


def sitePathLinkToXml(wds, splId, objId, netexMembersByWdsId, placeEquipments, equipmentPlaces = [], nodesPos = None, levels = None):
	"""Transforms a single SitePathLink into Netex format"""

	spl = wds.sitePathLinks[splId]
	ntxSplId = None
	cumulatedDist = 0
	splNodes = []
	splNodesTags = []

	# Geometry
	if spl.tags.get("NonGeographicalLocation") == "Yes":
		pass

	elif isinstance(spl, model.Way):
		fromNodeId = spl.nodes[nodesPos[0]]
		toNodeId = spl.nodes[nodesPos[1]]
		splNodes = spl.nodes[nodesPos[0]:nodesPos[1]+1]
		splNodesTags = [ wds.nodes[n].tags for n in splNodes ]
		ntxSplId = f"{splId}_n{fromNodeId}_n{toNodeId}"
		if levels is not None and len(levels) == 1 and levels[0] is not None:
			ntxSplId += f"_level{levels[0]}"

		# LineString Geometry
		linestring = etree.Element("{"+mappings.NS_GML["xmlns"]+"}LineString", nsmap = mappings.NS_GML)
		linestring.attrib["{"+mappings.NS_GML["xmlns"]+"}id"] = ntxSplId

		prevCoords = None
		for c in wds.getNodesCoordinates(splNodes):
			if prevCoords is not None:
				cumulatedDist += properties.distance(prevCoords, c)
			pos = etree.SubElement(linestring, "{"+mappings.NS_GML["xmlns"]+"}pos")
			pos.text = f"{c['lon']} {c['lat']}"
			prevCoords = c

	elif isinstance(spl, model.Node):
		fromNodeId = spl.attrs["id"]
		toNodeId = spl.attrs["id"]
		minLvl = int(levels[0]) if round(levels[0]) == levels[0] else levels[0]
		maxLvl = int(levels[1]) if round(levels[1]) == levels[1] else levels[1]
		ntxSplId = f"{splId}_level{minLvl}_level{maxLvl}"


	splTree = E.SitePathLink()
	properties.setNetexIdVersion(splTree, spl, "SitePathLink", ntxSplId)

	properties.appendKeyList(splTree, spl)

	properties.appendDirectMappings(splTree, spl,
		[{ "wdm": "Name", "netex": "Name" }]
	)

	splTree.append(E.Distance("%.2f" % cumulatedDist))

	if isinstance(spl, model.Way):
		splTree.append(linestring)

	# From|To / PlaceRef|EntranceRef
	properties.appendPlaceRef(wds, f"n{fromNodeId}", splTree, "From", netexMembersByWdsId)
	properties.appendPlaceRef(wds, f"n{toNodeId}", splTree, "To", netexMembersByWdsId)

	# Description
	properties.appendDirectMappings(splTree, spl, [
		{ "wdm": "Description", "netex": "Description" },
	])

	# AccessibilityAssessment
	access = properties.wdmTagsToNetexAccess(spl.tags, objId, splNodesTags)
	if access is not None:
		splTree.append(access)

	splTree.append(E.PublicUse("all"))

	properties.appendDirectMappings(splTree, spl, [
		mappings.MAPPING_RULE_COVERED,
		mappings.MAPPING_RULE_GATED,
		mappings.MAPPING_RULE_LIGHTING,
		{ "wdm": "WheelchairAccess", "netex": "AllAreasWheelchairAccessible", "mapping": mappings.MAPPING_YESNO_TRUEFALSE }
	])

	# Number of steps
	if spl.tags["SitePathLink"] == "Stairs":
		properties.appendPositiveInteger(splTree, spl, "StepCount", "NumberOfSteps")

	elif isinstance(spl, model.Way):
		# Look for potential nodes with Obstacle=Kerb, that count as steps
		stepCount = 0

		for nodeId in splNodes:
			node = wds.nodes[nodeId]
			if node.tags.get("Obstacle") == "Kerb":
				stepCount += 1

		splTree.append(E.NumberOfSteps(str(stepCount)))

	# MinimumWidth
	minWidth = properties.getPathMinimumWidth(wds, spl, splNodes)
	if minWidth is not None:
		splTree.append(E.MinimumWidth(minWidth))

	# AllowedUse
	if spl.tags["SitePathLink"] in ["Escalator", "Travelator"]:
		properties.appendDirectMappings(splTree, spl, [
			{ "wdm": "Conveying", "netex": "AllowedUse", "mapping": { "Reversible": "twoWay", "Forward": "oneWay", "Backward": "oneWay" } }
		])

	properties.appendDirectMappings(splTree, spl, [
		{ "wdm": "Incline", "netex": "Transition", "mapping": { "Up": "up", "Down": "down", "No": "level" } }
	])
	properties.appendGradient(splTree, spl.tags.get("Slope"))

	# TiltAngle & TiltType
	if spl.tags.get("Tilt") is not None:
		try:
			tiltPct = float(spl.tags.get("Tilt"))
			tiltDeg = properties.anglePctToDegrees(tiltPct)
			tiltSide = spl.tags.get("TiltSide")

			if tiltDeg >= 0:
				splTree.append(E.TiltAngle(str(tiltDeg)))

				if tiltPct < 2:
					splTree.append(E.TiltType("nearlyFlat"))
				elif tiltSide == "Left" and tiltPct >= 3:
					splTree.append(E.TiltType("strongLeftTilt"))
				elif tiltSide == "Left" and tiltPct < 3:
					splTree.append(E.TiltType("mediumLeftTilt"))
				elif tiltSide == "Right" and tiltPct >= 3:
					splTree.append(E.TiltType("strongRightTilt"))
				elif tiltSide == "Right" and tiltPct < 3:
					splTree.append(E.TiltType("mediumRightTilt"))
		except ValueError:
			pass

	properties.appendDirectMappings(splTree, spl, [
		{ "wdm": "SitePathLink", "netex": "AccessFeatureType", "mapping": {
			"Lift": "lift", "Stairs": "stairs", "SeriesOfStairs": "seriesOfStairs", "Escalator": "escalator",
			"Travelator": "travelator", "Ramp": "ramp", "Shuttle": "shuttle", "Crossing": "crossing", "Footpath": "footpath",
			"Barrier": "barrier", "NarrowEntrance": "narrowEntrance", "Hall": "hall", "OpenSpace": "openSpace",
			"Street": "street", "Pavement": "pavement", "Concourse": "concourse", "ConfinedSpace": "confinedSpace",
			"QueueManagement": "queueManagement", "Quay": "pavement", "Elevator": "lift", "Corridor": "hall"
		} }
	])

	# PassageType
	if spl.tags.get("StructureType") in ["Pathway", "Corridor", "Overpass", "Underpass", "Tunnel"]:
		properties.appendDirectMappings(splTree, spl, [
			{ "wdm": "StructureType", "netex": "PassageType", "mapping": {
				"Pathway": "pathway",
				"Corridor": "corridor",
				"Overpass": "overpass",
				"Underpass": "underpass",
				"Tunnel": "tunnel"
			} }
		])
	elif spl.tags["SitePathLink"] in ["Pavement", "Footpath", "Street", "OpenSpace", "Concourse", "Passage"]:
		splTree.append(E.PassageType("pathway"))

	# FlooringType
	validFloorings = ["Carpet", "Concrete", "Asphalt", "Cork", "FibreglassGrating", "GlazedCeramicTiles", "PlasticMatting", "CeramicTiles", "Rubber", "SteelPlate", "Vinyl", "Wood", "Stone", "Grass", "Earth", "Gravel", "Uneven", "PavingStones"]

	if spl.tags.get("FlooringMaterial") in validFloorings:
		val = spl.tags["FlooringMaterial"]

		if val == "PavingStones":
			val = "stone"

		val = val[0].lower() + val[1:]
		splTree.append(E.FlooringType(val))

	elif "FlooringMaterial" in spl.tags:
		splTree.append(E.FlooringType("unknown"))

	# TactileWarningStrip
	if isinstance(spl, model.Way):
		startTWS = splNodesTags[0].get("TactileWarningStrip")
		endTWS = splNodesTags[-1].get("TactileWarningStrip")
		tws = "unknown"

		if startTWS == "None" and endTWS == "None":
			tws = "noTactileStrip"
		if startTWS not in ["None", None] and endTWS not in ["None", None]:
			tws = "tactileStripAtBothEnds"
		elif startTWS not in ["None", None] and endTWS in ["None", None]:
			tws = "tactileStripAtBeginning"
		elif startTWS in ["None", None] and endTWS not in ["None", None]:
			tws = "tactileStripAtEnd"
		
		splTree.append(E.TactileWarningStrip(tws))

	properties.appendDirectMappings(splTree, spl, [
		{ "wdm": "TactileGuidingStrip", "netex": "TactileGuidingStrip", "mapping": mappings.MAPPING_YESNO_TRUEFALSE },
	])

	# SiteRef
	splPlaceId = wds.sitePathLinksSiteRefs.get(spl.getId())
	if splPlaceId is not None:
		splTree.append(E.SiteRef(
			wdsref = splPlaceId
		))

	properties.appendEquipmentPlaces(splTree, equipmentPlaces, objId)
	properties.appendPlaceEquipments(splTree, placeEquipments)

	return splTree
