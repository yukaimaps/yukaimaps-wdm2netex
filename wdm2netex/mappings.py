#
# Various tag/feature mappings
#

# XML Namespaces
NS_NETEX = {
	None: "http://www.netex.org.uk/netex",
	"xsi": "http://www.w3.org/2001/XMLSchema-instance",
	"core": "http://www.govtalk.gov.uk/core",
	"gml": "http://www.opengis.net/gml/3.2",
	"ifopt": "http://www.ifopt.org.uk/ifopt",
	"siri": "http://www.siri.org.uk/siri",
	"xlink": "http://www.w3.org/1999/xlink"
}
NS_GML = { "xmlns": "http://www.opengis.net/gml/3.2" }


# WDM Tags
WDM_ACCESS_MAIN_TAGS = ["WheelchairAccess", "AudibleSignals", "VisualSigns" ]
WDM_ACCESS_DESC_TAGS = [ f"{k}:Description" for k in WDM_ACCESS_MAIN_TAGS ]
WDM_ACCESS_CROSSING_TAGS = ["PedestrianLights", "ZebraCrossing", "VisualGuidanceBands"]
WDM_ACCESS_PARKING_TAGS = [ "ParkingVisibility", "StepFreeAccess" ]
WDM_ACCESS_TAGS = WDM_ACCESS_MAIN_TAGS + WDM_ACCESS_DESC_TAGS + WDM_ACCESS_CROSSING_TAGS + WDM_ACCESS_PARKING_TAGS


# WDM to Netex tags mappings
MAPPING_YESNO_TRUEFALSE = { "Yes": "true", "No": "false" }
MAPPING_YESBADNO_TRUEFALSE = { "Yes": "true", "Bad": "true", "No": "false" }
MAPPING_NONESTAR_FALSETRUE = { "None": "false", "*": "true" }
MAPPING_NONESTAREMPTY_FALSETRUE = { "None": "false", "*": "true", "": "false" }
MAPPING_YESNONE_TRUEFALSE = { "Yes": "true", "*": "false", "": "false" }
MAPPING_YESNOSTAREMPTY_TRUEFALSEEMPTYTRUE = { "Yes": "true", "No": "false", "*": "", "": "true"}
MAPPING_STATUS_TRUEFALSE = {
      "Good": "true", "Worn": "true", "Discomfortable": "true", "Hazardous": "true",
      "None": "false", "*": "false", "": "false"
}
MAPPING_RLCBN_SIDES = { "Right": "oneSide", "Left": "oneSide", "Center": "oneSide", "Both": "bothSides", "None": "none" }
MAPPING_SIDES_TRUEFALSE = { "BothSides": "true", "OneSide": "true", "Yes": "true", "No": "false" }
MAPPING_QUAY_TRANSPORT_MODE = {
	"Bus": "bus", "Coach": "coach", "Water": "water", "Funicular": "funicular",
	"TrolleyBus": "trolleyBus", "CableWay": "cableway", "Rail": "rail",
	"Metro": "metro", "Tram": "tram", "*": "bus", "": "bus"
}
MAPPING_STOPPLACE_TRANSPORT_MODE = {
	"RailStation": "rail", "MetroStation": "metro", "BusStation": "bus",
	"CoachStation": "coach", "TramStation": "tram", "FerryPort": "water",
	"LiftStation": "cableway", "Airport": "air", "OnstreetBus": "bus",
	"OnstreetTram": "tram"
}
MAPPING_DOORHANDLE = {
      "None": "none", "Lever": "lever", "Button": "button", "Knob": "knob",
      "CrashBar": "crashBar", "GrabRail": "grabRail", "WindowLever": "windowLever",
      "Vertical": "vertical", "Other": "other", "*": "other"
}
MAPPING_RULE_COVERED = { "wdm": "Outdoor", "netex": "Covered", "mapping": { "Yes": "outdoors", "No": "indoors", "Covered": "covered" } }
MAPPING_RULE_GATED = { "wdm": "Gated", "netex": "Gated", "mapping": { "Yes": "gatedArea", "No": "openArea" } }
MAPPING_RULE_LIGHTING = { "wdm": "Lighting", "netex": "Lighting", "mapping": { "Yes": "wellLit", "No": "unlit", "Bad": "poorlyLit" } }
MAPPING_RULE_STAFFING = { "wdm": "Staffing", "netex": "Staffing", "mapping": { "No": "unmanned", "PartTime": "partTime", "FullTime": "fullTime" }}
MAPPING_RULE_SURFACETYPE = { "wdm": "FlooringMaterial", "netex": "SurfaceType", "mapping": {
      "Asphalt": "asphalt", "Bricks": "bricks", "Cobbles": "cobbles", "Earth": "earth", "Grass": "grass",
      "LooseSurface": "looseSurface", "PavingStones": "pavingStones", "RoughSurface": "roughSurface",
      "Smooth": "smooth", "*": "other","": "other"
} }


# KeyList tags to ignore
KEYLIST_TAGS_IMPORT = ["Ref:Import:Id", "Ref:Import:Version"]
KEYLIST_TAGS = {
      "PathJunction": [ "Altitude", "PathJunction" ],
      "ParkingBay": [
            "ParkingBay", "Altitude", "Description", "BayGeometry",
            "ParkingVisibility", "Length", "Width", "VehicleRecharging",
            "Lighting", "StepFreeAccess", "WheelchairAccess", "WheelchairAccess:Description",
      ],
      "AssistanceService": [ "Staffing", "AccessibilityTrainedStaff" ],
      "SeatingEquipment": [ "SeatCount", "Width", "BackRest" ],
      "GeneralSign": [ "Width" ],
      "RoughSurface": [ "Width", "FlooringMaterial" ],
      "SanitaryEquipment": [
            "WheelchairAccess", "WheelchairAccess:Description", "Toilets:Shower", "Toilets:BabyChange",
            "FreeToUse", "ManoeuvringDiameter", "Staffing", "Altitude", "Toilets:HandWashing",
            "Toilets:DrinkingWater", "Toilets:Position",
      ],
      "ShelterEquipment": [ "SeatCount", "Width" ],
      "SitePathLink": [
            "SitePathLink", "TactileGuidingStrip", "Incline", "FlooringMaterial", "StructureType",
            "TiltSide", "Tilt", "Slope", "Lighting", "StepCount", "WheelchairAccess", "WheelchairAccess:Description",
            "Gated", "Outdoor", "AudibleSignals", "VisualSigns", "Description", "Name", "StairFlightCount",
            "ContinuousHandrail", "Handrail", "StepColourContrast", "StepHeight", "StepCount", "PublicCode",
            "Depth", "MaxWeight", "ManoeuvringDiameter", "InternalWidth", "HandrailHeight", "RaisedButtons",
            "MirrorOnOppositeSide", "Attendant", "AlarmButton", "AutomaticDoor", "AudioAnnouncements",
            "MagneticInductionLoop", "StoppedIfUnused", "PedestrianLights", "CrossingIsland", "Crossing",
            "ZebraCrossing", "BumpCrossing", "VibratingCrossingAids", "StepLength", "StepCondition",
            "Handrail:TactileWriting", "StairRamp", "RestStopDistance", "SafetyEdge", "TurningSpacePosition",
            "MonitoringRemoteControl",
      ],
      "CrossingEquipment": [
            "SitePathLink", "VisualGuidanceBands", "TactileGuidingStrip", "PedestrianLights", "CrossingIsland",
            "Crossing", "Width", "Incline", "FlooringMaterial", "StructureType", "TiltSide", "Tilt",
            "Slope", "Lighting", "WheelchairAccess", "WheelchairAccess:Description", "Gated", "Outdoor",
            "Description", "ZebraCrossing", "BumpCrossing", "VibratingCrossingAids",
      ],
      "RampEquipment": [
            "SitePathLink", "VisualGuidanceBands", "TactileGuidingStrip", "Handrail", "Slope", "Width", "FlooringMaterial",
            "StructureType", "TiltSide", "Tilt", "Lighting", "WheelchairAccess", "WheelchairAccess:Description",
            "Gated", "Outdoor", "Description", "MaxWeight", "RestStopDistance", "SafetyEdge", "TurningSpacePosition"
      ],
      "TravelatorEquipment": [
            "SitePathLink", "PublicCode", "Width", "StoppedIfUnused", "Slope", "FlooringMaterial", "StructureType",
            "TiltSide", "Tilt", "Lighting", "Gated", "Outdoor", "Description", "MonitoringRemoteControl",
      ],
      "LiftEquipment": [
            "SitePathLink", "PublicCode", "Depth", "MaxWeight", "Gated", "Outdoor", "Description",
            "ManoeuvringDiameter", "InternalWidth", "Handrail", "HandrailHeight", "RaisedButtons", "MirrorOnOppositeSide",
            "Attendant", "AlarmButton", "AutomaticDoor", "AudioAnnouncements", "MagneticInductionLoop", "FlooringMaterial",
            "StructureType", "TiltSide", "Tilt", "Slope", "Lighting", "WheelchairAccess", "WheelchairAccess:Description",
      ],
      "QueueingEquipment": [
            "SitePathLink", "Width", "FlooringMaterial", "StructureType", "TiltSide",
            "Lighting", "WheelchairAccess", "WheelchairAccess:Description", "Gated", "Outdoor",
		"AudibleSignals", "Description", "Tilt", "Slope", "VisualSigns"
      ],
      "StaircaseEquipment": [
            "SitePathLink", "StairFlightCount", "ContinuousHandrail", "Handrail", "StepColourContrast",
            "StepHeight", "StepCount", "Width", "FlooringMaterial", "TiltSide", "Tilt", "Slope",
            "Lighting", "Gated", "Outdoor", "AudibleSignals", "VisualSigns", "Description", "StepLength",
            "StepCondition", "Handrail:TactileWriting", "StairRamp",
      ],
      "Entrance": [
            "Entrance", "Height", "Width", "IsExit", "IsEntry", "Name", "PublicCode", "WheelchairAccess",
            "WheelchairAccess:Description", "Width", "EntranceAttention", "AddressLine", "PostCode",
            "DoorHandle:Outside", "DoorHandle:Inside", "OutsideTurningSpace:Width", "OutsideTurningSpace:Length",
            "InsideTurningSpace:Width", "InsideTurningSpace:Length", "RampDoorbell", "Recognizable", "TurningSpacePosition",
      ],
      "EntranceEquipment": [
            "AutomaticDoor", "Height", "Width", "IsExit", "IsEntry", "Name", "PublicCode", "WheelchairAccess",
            "WheelchairAccess:Description", "Width", "EntranceAttention", "AddressLine", "PostCode", "Altitude",
            "DoorHandle:Outside", "DoorHandle:Inside", "OutsideTurningSpace:Width", "OutsideTurningSpace:Length",
            "InsideTurningSpace:Width", "InsideTurningSpace:Length", "RampDoorbell", "Recognizable", "TurningSpacePosition",
      ],
      "PointOfInterest": [
		"StopPlace", "Lighting", "AudioInformation", "VisualSigns", "VisualSigns:Description",
            "WheelchairAccess", "WheelchairAccess:Description", "AudibleSignals", "AudibleSignals:Description",
		"StepFreeAccess", "EscalatorFreeAccess", "LiftFreeAccess", "Description", "Name", "VisualDisplays",
            "LargePrintTimetable", "Assistance", "AccessibilityToolLending", "FamilyServices", "WiFi", "RealTimeDepartures",
            "Toilets", "Toilets:Shower", "Toilets:BabyChange", "Tickets", "Emergency", "NearbyCarPark", "NearbyCyclePark",
            "Staffing", "Website", "Image", "AddressLine", "PostCode", "Outdoor", "Gated", "Altitude"
      ],
      "Quay": [
            "Quay", "Name", "Description", "Outdoor", "Gated", "Lighting", "PublicCode",
            "VisualSigns", "VisualSigns:Description", "WheelchairAccess", "WheelchairAccess:Description",
		"AudibleSignals", "AudibleSignals:Description", "Ref:Export:Id", "Altitude"
      ],
}

KEYLIST_TAGS["EscalatorEquipment"] = KEYLIST_TAGS["TravelatorEquipment"]
KEYLIST_TAGS["PointOfInterestEntrance"] = KEYLIST_TAGS["Entrance"]
KEYLIST_TAGS["StopPlace"] = KEYLIST_TAGS["PointOfInterest"] + ["PointOfInterest", "Ref:Export:Id"]
