#
# Tests for converter.py
#

import os
from lxml.doctestcompare import LXMLOutputChecker, PARSE_XML
from lxml import etree
from doctest import Example
from wdm2netex import converter, model
import datetime
import pytest


DATA_DIR = os.path.join(os.path.dirname(__file__), "./data")
xsdPath = os.path.join(DATA_DIR, "./netex_xsd/xsd/NeTEx_publication.xsd")
NETEX_XSD = None


def assertEqualsXml(validXmlName, toCheckXmlPath):
	"""Compares two given XML to check if they are identical

	Parameters
	----------
	validXmlName : str
		Name of the valid/reference XML stored in /tests/data folder
	toCheckXmlPath : str
		Full path to XML file to test
	"""
	validXmlPath = os.path.join(DATA_DIR, validXmlName)

	with open(validXmlPath, 'rb') as validXmlFile:
		with open(toCheckXmlPath, 'rb') as toCheckXmlFile:
			validXmlStr = validXmlFile.read()
			toCheckXmlStr = toCheckXmlFile.read()
			checker = LXMLOutputChecker()
			if not checker.check_output(validXmlStr, toCheckXmlStr, PARSE_XML):
				message = checker.output_difference(Example("", validXmlStr.decode("utf-8")), toCheckXmlStr, PARSE_XML)
				raise AssertionError(message)


def assertValidNetex(toCheckXmlPath):
	"""Checks if given file is valid regarding Netex XSD schema

	Parameters
	----------
	toCheckXmlPath : str
		Full path to XML file to test
	"""

	global NETEX_XSD

	# Load schema if not available yet
	if NETEX_XSD is None:
		with open(xsdPath) as f:
			doc = etree.parse(f)
			NETEX_XSD = etree.XMLSchema(doc)

	# Load XML file
	with open(toCheckXmlPath) as f:
		doc = etree.parse(f)

	try:
		NETEX_XSD.assertValid(doc)
	except etree.DocumentInvalid as e:
		raise AssertionError(e)


def test_readWdmFromXML_empty():
	inputPath = os.path.join(DATA_DIR, "wdm_empty.xml")
	res = converter.readWdmFromXML(inputPath)
	assert type(res) == type(model.WalkDataSet())
	assert len(res.sitePathLinks) == 0


def test_readWdmFromXML_SitePathLink_single():
	inputPath = os.path.join(DATA_DIR, "wdm_spl_simple.xml")
	res = converter.readWdmFromXML(inputPath)
	assert type(res) == type(model.WalkDataSet())
	assert len(res.sitePathLinks) == 1
	spl = res.sitePathLinks["w10"]
	assert spl is not None
	assert isinstance(spl, model.Way)
	assert spl.attrs["version"] == "1"
	assert spl.tags == {
		"SitePathLink": "Crossing",
		"Crossing": "Road",
		"WheelchairAccess": "Yes",
		"TactileGuidingStrip": "No",
		"Lighting": "Yes",
		"ZebraCrossing": "Good",
		"PedestrianLights": "No",
		"Width": "2",
		"Name": "Passage piéton des Halles",
		"FlooringMaterial": "Asphalt",
		"WheelchairAccess:Description": "PMR compatible",
		"AudibleSignals:Description": "Pas de signal audio",
		"Ref:Import:Source": "OpenStreetMap",
		"Slope": "1.2",
		"Tilt": "5",
		"TiltSide": "Right"
	}
	assert spl.nodes == ["26", "27"]


@pytest.mark.parametrize(("testName", "validateXsd"), (
	("amenities", True),
	("empty", True),
	("pathjunction_ref", True),
	("poi", True),
	("quay_polygons", True),
	("spl_elevators", True),
	("spl_kerbs", True),
	("spl_length", True),
	("spl_obstacles", True),
	("spl_place_equipments", True),
	("spl_simple", True),
	("spl_siteref", True),
	("spl_stairs", True),
	("spl_crossing", True),
	("transport", True),
	("example_tramStation", True),
	("example_railStation", True),
	("example_onstreetbus", True),
))
def test_writeWdmIntoNetex(tmpdir, testName, validateXsd):
	output = tmpdir.join("netex.xml")
	wds = model.WalkDataSet()

	if testName != "empty":
		inputPath = os.path.join(DATA_DIR, f"wdm_{testName}.xml")
		wds = converter.readWdmFromXML(inputPath)

	converter.writeWdmIntoNetex(wds, output, pubts=datetime.datetime(2022, 12, 7, 12, 0, 0))
	if validateXsd:
		assertValidNetex(output)
	assertEqualsXml(f"netex_{testName}.xml", output)
