import os
from lxml.doctestcompare import LXMLOutputChecker
from lxml import etree
from doctest import Example
from wdm2netex import properties, model
import datetime
import pytest


def test_appendKeyList_basic_case():
	elem = etree.Element("CrossingEquipment")
	feature = model.Feature({}, {})
	properties.appendKeyList(elem, feature)
	assert elem.find("keyList") is None


def test_appendKeyList_with_specific_tags():
	elem = etree.Element("CrossingEquipment")
	feature = model.Feature({"Name": "value1", "Ref:Import:Source": "OSM"}, {})
	properties.appendKeyList(elem, feature)
	keyList = elem.find("keyList")
	assert keyList is not None
	assert len(keyList) == 2
	assert keyList[0].find("Key").text == "Name"
	assert keyList[0].find("Value").text == "value1"
	assert keyList[0].attrib.get("typeOfKey") == "WDM_tag"
	assert keyList[1].find("Key").text == "Ref:Import:Source"
	assert keyList[1].find("Value").text == "OSM"
	assert keyList[1].attrib.get("typeOfKey") == "WDM_tag"


def test_appendKeyList_exclusion_based_on_mappings():
	elem = etree.Element("CrossingEquipment")
	feature = model.Feature({"VisualGuidanceBands": "Yes"}, {})
	properties.appendKeyList(elem, feature)
	keyList = elem.find("keyList")
	assert keyList is None


def test_appendKeyList_inclusion_of_wdm_id_and_version():
	elem = etree.Element("CrossingEquipment", id="OSM:f123")
	feature = model.Feature({}, {"version": "2", "id": "123"})
	properties.appendKeyList(elem, feature)
	keyList = elem.find("keyList")
	assert keyList is not None
	assert len(keyList) == 2
	assert keyList[0].find("Key").text == "Id"
	assert keyList[0].find("Value").text == "f123"
	assert keyList[0].attrib.get("typeOfKey") == "WDM_id"
	assert keyList[1].find("Key").text == "Version"
	assert keyList[1].find("Value").text == "2"
	assert keyList[1].attrib.get("typeOfKey") == "WDM_id"


def test_appendKeyList_no_wdm_id_when_yukaimaps():
	elem = etree.Element("CrossingEquipment", id="Yukaimaps:1234")
	feature = model.Feature({}, {"version": "2", "id": "123"})
	properties.appendKeyList(elem, feature)
	keyList = elem.find("keyList")
	assert keyList is None


@pytest.mark.parametrize(("startVal", "endVal"), (
	("1", "1"),
	("1.0", None),
	("-1", None),
	("0", None),
	("blabla", None)
))
def test_appendPositiveInteger(startVal, endVal):
	t = etree.Element("test")
	w = model.Feature({ "StepCount": startVal }, {})
	properties.appendPositiveInteger(t, w, "StepCount", "NumberOfSteps")

	if endVal is not None:
		assert len(t) == 1
		assert t[0].tag == "NumberOfSteps"
		assert t[0].text == endVal
	else:
		assert len(t) == 0


@pytest.mark.parametrize(("val", "res"), (
	("1.0", True),
	("-1.0", False),
	("bla", False),
	("0.0", False),
	(None, False)
))
def test_isValidPositiveFloat(val, res):
	assert properties.isValidPositiveFloat(val) == res


@pytest.mark.parametrize(("startVal", "precision", "endVal"), (
	("1.0", 0, "1"),
	("15.123456", 2, "15.12"),
	("1", 2, "1.00"),
	("-1.0", 2, None),
	("0.0", 2, None),
	("blabla", 2, None)
))
def test_appendPositiveFloat(startVal, precision, endVal):
	t = etree.Element("test")
	w = model.Feature({ "StepCount": startVal }, {})
	properties.appendPositiveFloat(t, w, "StepCount", "NumberOfSteps", precision)

	if endVal is not None:
		assert len(t) == 1
		assert t[0].tag == "NumberOfSteps"
		assert t[0].text == endVal
	else:
		assert len(t) == 0


@pytest.mark.parametrize(("addr", "postcode"), (
	(None, None),
	("1 Rue du Bois", "35750"),
	("1 Rue du Bois", None),
	(None, "35750")
))
def test_appendPostalAddress(addr, postcode):
	tags = {}

	if addr is not None:
		tags["AddressLine"] = addr
	
	if postcode is not None:
		tags["PostCode"] = postcode
	

	t = etree.Element("test")
	autoIncObjId = 0
	properties.appendPostalAddress(t, tags, autoIncObjId)

	if addr is None and postcode is None:
		assert len(t) == 0
	else:
		assert len(t) == 1
		assert t[0].tag == "PostalAddress"
		assert t[0].attrib["id"] == "Yukaimaps:PostalAddress:0"
		assert t[0].attrib["version"] == "any"

		if addr is not None:
			assert t[0][0].tag == "AddressLine1"
			assert t[0][0].text == addr
			if postcode is not None:
				assert t[0][1].tag == "PostCode"
				assert t[0][1].text == postcode
		elif postcode is not None:
			assert t[0][0].tag == "PostCode"
			assert t[0][0].text == postcode


def test_appendDirectMappings_basic():
	t = etree.Element("test")
	m = [
		{ "wdm": "Name", "netex": "Name" },
		{ "wdm": "Description", "netex": "Description" }
	]
	w = model.Feature({ "Name": "1" }, {})

	properties.appendDirectMappings(t, w, m)

	assert len(t) == 1
	assert t[0].tag == "Name"
	assert t[0].text == "1"


def test_appendDirectMappings_transforms():
	t = etree.Element("test")
	m = [
		{ "wdm": "Name", "netex": "Name" },
		{ "wdm": "Lighting", "netex": "Lights", "mapping": { "Yes": "wellLit", "No": "unlit", "Bad": "poorlyLit" } },
	]
	w = model.Feature({ "Name": "1", "Lighting": "Yes" }, {})

	properties.appendDirectMappings(t, w, m)

	assert len(t) == 2
	assert t[0].tag == "Name"
	assert t[0].text == "1"
	assert t[1].tag == "Lights"
	assert t[1].text == "wellLit"


def test_appendDirectMappings_fallback():
	t = etree.Element("test")
	m = [
		{ "wdm": "Gated", "netex": "Gated", "mapping": { "": "openArea", "Yes": "gatedArea" } },
	]
	w = model.Feature({}, {})

	properties.appendDirectMappings(t, w, m)

	assert len(t) == 1
	assert t[0].tag == "Gated"
	assert t[0].text == "openArea"


def test_appendDirectMappings_fallback_star():
	t = etree.Element("test")
	m = [
		{ "wdm": "Gated", "netex": "Gated", "mapping": { "*": "openArea", "Yes": "gatedArea" } },
	]
	w = model.Feature({"Gated": "Whatever"}, {})

	properties.appendDirectMappings(t, w, m)

	assert len(t) == 1
	assert t[0].tag == "Gated"
	assert t[0].text == "openArea"


@pytest.mark.parametrize(("v", "e"), (
	("Yes", "true"),
	("No", "false"),
	("Blabla", None),
	(None, "true"),
))
def test_appendDirectMappings_fallback_star_emptyval(v, e):
	t = etree.Element("test")
	m = [
		{ "wdm": "IsEntry", "netex": "IsEntry", "mapping": { "Yes": "true", "No": "false", "*": "", "": "true"} },
	]
	w = model.Feature({} if v is None else {"IsEntry": v}, {})

	properties.appendDirectMappings(t, w, m)

	if e is not None:
		assert len(t) == 1
		assert t[0].tag == "IsEntry"
		assert t[0].text == e
	else:
		assert len(t) == 0


@pytest.mark.parametrize(("wtype", "n1tags", "n2tags", "expected"), (
	("Footpath", {"Obstacle": "PostBox", "Width": "0.5"}, {}, "2.00"),
	("Footpath", {"Obstacle": "PostBox", "RemainingWidth": "1.5"}, {}, "1.50"),
	("Footpath", {"Obstacle": "PostBox", "RemainingWidth": "0.5"}, {"Obstacle": "Bench", "RemainingWidth": "1.8"}, "0.50"),
	("Footpath", {"Obstacle": "Vegetation", "ObstacleType": "Overhanging", "RemainingWidth": "0.5", "Height": "2.4"}, {}, "2.00"),
	("Footpath", {"Obstacle": "Vegetation", "ObstacleType": "Overhanging", "RemainingWidth": "0.5", "Height": "1.5"}, {}, "0.50"),
	("Footpath", {"Obstacle": "Vegetation", "ObstacleType": "Overhanging", "RemainingWidth": "0.5"}, {}, "0.50"),
	("Crossing", {"KerbCut:Width": "0.751"}, {"KerbCut:Width": "1.25"}, "0.75")
))
def test_getPathMinimumWidth(wtype, n1tags, n2tags, expected):
	wds = model.WalkDataSet()
	n1 = model.Node(n1tags, {"id": "1"})
	n2 = model.Node(n2tags, {"id": "2"})
	path = model.Way({"SitePathLink": wtype, "Width": "2"}, {"id":"1"}, ["1", "2"])
	wds.addNode(n1)
	wds.addNode(n2)
	wds.addSitePathLink(path)
	res = properties.getPathMinimumWidth(wds, path, path.nodes)
	assert res == expected


@pytest.mark.parametrize(("wdm", "netex", "nodes"), (
	( # no_access_info
		{},
		None,
		[]
	),
	( # validity_all
		{ "WheelchairAccess:Description": "1",
			"AudibleSignals:Description": "2",
			"VisualSigns:Description": "3" },
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>1 - 2 - 3</Description>
				</ValidityCondition>
			</validityConditions>""",
		[]
	),
	( # validity_subset
		{ "WheelchairAccess:Description": "1",
			"VisualSigns:Description": "3" },
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>1 - 3</Description>
				</ValidityCondition>
			</validityConditions>""",
		[]
	),
	( # validity missing with limited access tags
		{ "WheelchairAccess": "Limited" },
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Accessibilité partielle non détaillée</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>partial</WheelchairAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # access_true
		{ "WheelchairAccess": "Yes",
			"AudibleSignals": "Yes",
			"VisualSigns": "Yes" },
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<AudibleSignalsAvailable>true</AudibleSignalsAvailable>
					<VisualSignsAvailable>true</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # access_true_subset
		{ "WheelchairAccess": "Yes",
			"VisualSigns": "Yes" },
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<VisualSignsAvailable>true</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # access_partial
		{ "WheelchairAccess": "Yes",
			"AudibleSignals": "No",
			"VisualSigns": "Yes" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<AudibleSignalsAvailable>false</AudibleSignalsAvailable>
					<VisualSignsAvailable>true</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # access_no
		{ "WheelchairAccess": "No",
			"AudibleSignals": "No",
			"VisualSigns": "No" },
		"""<MobilityImpairedAccess>false</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<AudibleSignalsAvailable>false</AudibleSignalsAvailable>
					<VisualSignsAvailable>false</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # access_no_subset
		{ "AudibleSignals": "No",
			"VisualSigns": "No" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<AudibleSignalsAvailable>false</AudibleSignalsAvailable>
					<VisualSignsAvailable>false</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # limitations wheelchair true, audible false, visual partial
		{ "WheelchairAccess": "Yes",
			"AudibleSignals": "No",
			"VisualSigns": "Limited"},
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Accessibilité partielle non détaillée</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<AudibleSignalsAvailable>false</AudibleSignalsAvailable>
					<VisualSignsAvailable>partial</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # limitations wheelchair false
		{ "WheelchairAccess": "No" },
		"""<MobilityImpairedAccess>false</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # limitations wheelchair limited
		{ "WheelchairAccess": "Limited" },
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Accessibilité partielle non détaillée</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>partial</WheelchairAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # StepFreeAccess stairs false
		{ "SitePathLink": "Stairs" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>false</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # StepFreeAccess obstacle kerb
		{ "SitePathLink": "Footpath" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>false</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[{}, {"Obstacle":"Kerb"}, {}]
	),
	( # EscalatorFreeAccess escalator false
		{ "SitePathLink": "Escalator" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>false</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # LiftFreeAccess elevator false
		{ "SitePathLink": "Elevator" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>false</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>unknown</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Wheelchair access elevator few depth
		{ "SitePathLink": "Elevator", "Depth": "1.0" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>false</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>unknown</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Wheelchair access elevator auto door
		{ "SitePathLink": "Elevator", "AutomaticDoor": "No" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>false</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>unknown</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Wheelchair access elevator few width
		{ "SitePathLink": "Elevator", "Width": "0.7" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>false</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>unknown</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Wheelchair access elevator with technical details
		{ "SitePathLink": "Elevator", "AutomaticDoor": "Yes", "Depth": "2", "Width": "1.5" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>false</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>unknown</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # LiftFreeAccess elevator node
		{ "SitePathLink": "Footpath" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[{"SitePathLink": "Elevator"}, {}, {}]
	),
	( # Path with tilt
		{ "SitePathLink": "Footpath", "Tilt": "10" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Path with slope
		{ "SitePathLink": "Footpath", "Slope": "10" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Path with narrow width
		{ "SitePathLink": "Footpath", "Width": "0.5" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Path with sand
		{ "SitePathLink": "Footpath", "FlooringMaterial": "Sand" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Path with flooring hazardous
		{ "SitePathLink": "Footpath", "Flooring": "Hazardous" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Path with obstacle
		{ "SitePathLink": "Footpath" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[{"Obstacle": "Toilets"}, {}]
	),
	( # Path with node & kerb height
		{ "SitePathLink": "Footpath" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>false</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[{"PathJunction": "Crossing", "KerbHeight": "0.1"}, {}]
	),
	( # Path with node & kerb raised
		{ "SitePathLink": "Footpath" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>false</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[{"PathJunction": "Crossing", "KerbDesign": "Raised"}, {}]
	),
	( # Path with node & no kerb info
		{ "SitePathLink": "Footpath" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>false</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[{"PathJunction": "Crossing"}, {}]
	),
	( # Path with OK technical details
		{ "SitePathLink": "Footpath", "Tilt": "1", "Slope": "3", "Width": "2", "FlooringMaterial": "Asphalt", "Flooring": "Good" },
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[{"PathJunction": "Crossing", "KerbHeight": "0.01"}, {}]
	),
	( # Path with OK technical details and limited access
		{ "SitePathLink": "Footpath", "Tilt": "4", "Slope": "7", "Width": "1", "FlooringMaterial": "Asphalt", "Flooring": "Discomfortable" },
		"""<validityConditions>
				<ValidityCondition id="Yukaimaps:ValidityCondition:0:" version="any">
					<Description>Présence de marches de moins de 4 cm</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>partial</WheelchairAccess>
					<StepFreeAccess>partial</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[{"PathJunction": "Crossing", "KerbHeight": "0.03"}, {}]
	),
	( # Ramp with slope
		{ "SitePathLink": "Ramp", "Slope": "10" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Ramp with narrow width
		{ "SitePathLink": "Ramp", "Width": "0.9" },
		"""<MobilityImpairedAccess>partial</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>false</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Ramp with enough width and few slope
		{ "SitePathLink": "Ramp", "Width": "2", "Slope": "3" },
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # VisualSigns true
		{ "VisualSigns": "Yes" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<VisualSignsAvailable>true</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # VisualSigns partial with desc
		{ "VisualSigns": "Limited", "ZebraCrossing": "Hazardous", "VisualGuidanceBands": "No", "PedestrianLights": "No" },
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Accessibilité partielle non détaillée</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<VisualSignsAvailable>partial</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Crossing + VisualSigns partial with desc
		{ "SitePathLink": "Crossing", "VisualSigns": "Limited", "ZebraCrossing": "Hazardous", "VisualGuidanceBands": "No", "PedestrianLights": "No" },
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Marquage au sol avec dégradation entraînant un problème de sécurité immédiat</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>partial</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # VisualSigns empty + crossing + no help
		{ "SitePathLink": "Crossing", "PedestrianLights": "No", "ZebraCrossing": "None", "VisualGuidanceBands": "No" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>false</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # VisualSigns empty + crossing + help
		{ "SitePathLink": "Crossing", "PedestrianLights": "Yes", "ZebraCrossing": "Good", "VisualGuidanceBands": "Yes" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>true</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # VisualSigns empty + crossing + partial help
		{ "SitePathLink": "Crossing", "PedestrianLights": "No", "ZebraCrossing": "Good", "VisualGuidanceBands": "Yes" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>true</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # VisualSigns empty + crossing + not all helps sets
		{ "SitePathLink": "Crossing", "ZebraCrossing": "Good", "VisualGuidanceBands": "Yes" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>true</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # VisualSigns on footpath
		{ "SitePathLink": "Footpath", "VisualSigns": "Limited" },
		"""<validityConditions>
				<ValidityCondition id="Yukaimaps:ValidityCondition:0:" version="any">
					<Description>Accessibilité partielle non détaillée</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # VisualSigns on hall
		{ "SitePathLink": "Hall", "WheelchairAccess": "Yes", "VisualSigns": "Yes" },
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<VisualSignsAvailable>true</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # VisualSigns on elevator
		{ "SitePathLink": "Elevator", "WheelchairAccess": "Yes", "VisualDisplays": "Yes" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>false</LiftFreeAccess>
					<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
					<VisualSignsAvailable>true</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # AudibleSignals true
		{ "AudibleSignals": "Yes" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<AudibleSignalsAvailable>true</AudibleSignalsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # AudibleSignals empty crossing acoustic aids none
		{ "SitePathLink": "Crossing", "AcousticCrossingAids": "None" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<AudibleSignalsAvailable>false</AudibleSignalsAvailable>
					<VisualSignsAvailable>unknown</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # AudibleSignals empty crossing acoustic aids good
		{ "SitePathLink": "Crossing", "AcousticCrossingAids": "Good" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<AudibleSignalsAvailable>true</AudibleSignalsAvailable>
					<VisualSignsAvailable>unknown</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # AudibleSignals empty crossing acoustic aids discomfortable
		{ "SitePathLink": "Crossing", "AcousticCrossingAids": "Discomfortable" },
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Répétiteurs sonores avec dégradatation entrainant une difficulté d'usage ou un inconfort</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<AudibleSignalsAvailable>partial</AudibleSignalsAvailable>
					<VisualSignsAvailable>unknown</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # AudibleSignals empty crossing acoustic aids hazardous
		{ "SitePathLink": "Crossing", "AcousticCrossingAids": "Hazardous" },
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Répétiteurs sonores avec dégradatation entrainant un problème de sécurité immédiat</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>unknown</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<AudibleSignalsAvailable>partial</AudibleSignalsAvailable>
					<VisualSignsAvailable>unknown</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # AudibleSignals on footpath
		{ "SitePathLink": "Footpath", "WheelchairAccess": "Yes", "AudibleSignals": "Yes" },
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # AudibleSignals on hall
		{ "SitePathLink": "Hall", "WheelchairAccess": "Yes", "AudibleSignals": "Yes" },
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>true</LiftFreeAccess>
					<AudibleSignalsAvailable>true</AudibleSignalsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # AudibleSignals on elevator
		{ "SitePathLink": "Elevator", "WheelchairAccess": "Yes", "AudioAnnouncements": "Yes" },
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
				<AccessibilityLimitation>
					<WheelchairAccess>true</WheelchairAccess>
					<StepFreeAccess>true</StepFreeAccess>
					<EscalatorFreeAccess>true</EscalatorFreeAccess>
					<LiftFreeAccess>false</LiftFreeAccess>
					<AudibleSignalsAvailable>true</AudibleSignalsAvailable>
					<VisualSignsAvailable>unknown</VisualSignsAvailable>
				</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # StopPlace with unknown wheelchair access to quay
		{"StopPlace": "RailStation", "WheelchairAccess": {"Yes", "No", None}},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<EscalatorFreeAccess>unknown</EscalatorFreeAccess>
				<LiftFreeAccess>unknown</LiftFreeAccess>
				<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
				<VisualSignsAvailable>unknown</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # StopPlace with partial wheelchair access to quay
		{"StopPlace": "RailStation", "WheelchairAccess": {"Yes", "No"}},
		"""<validityConditions>
				<ValidityCondition id="Yukaimaps:ValidityCondition:0:" version="any">
					<Description>Présence inconnue d&#x27;un cheminement pratiquable en fauteuil roulant entre les quais</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>partial</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<EscalatorFreeAccess>unknown</EscalatorFreeAccess>
				<LiftFreeAccess>unknown</LiftFreeAccess>
				<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
				<VisualSignsAvailable>unknown</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # StopPlace with unknown visual signs on quay
		{"StopPlace": "RailStation", "VisualSigns": {"Yes", "No", None}},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<EscalatorFreeAccess>unknown</EscalatorFreeAccess>
				<LiftFreeAccess>unknown</LiftFreeAccess>
				<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
				<VisualSignsAvailable>unknown</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # StopPlace with partial visual signs on quay
		{"StopPlace": "RailStation", "VisualSigns": {"Yes", "No"}},
		"""<validityConditions>
				<ValidityCondition id="Yukaimaps:ValidityCondition:0:" version="any">
					<Description>Signalétique visuelle dépendante du quai</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<EscalatorFreeAccess>unknown</EscalatorFreeAccess>
				<LiftFreeAccess>unknown</LiftFreeAccess>
				<AudibleSignalsAvailable>unknown</AudibleSignalsAvailable>
				<VisualSignsAvailable>partial</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # StopPlace with partial audible signals on quay
		{"StopPlace": "RailStation", "AudibleSignals": {"Yes", "No"}},
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Signalétique auditive dépendante du quai</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<EscalatorFreeAccess>unknown</EscalatorFreeAccess>
				<LiftFreeAccess>unknown</LiftFreeAccess>
				<AudibleSignalsAvailable>partial</AudibleSignalsAvailable>
				<VisualSignsAvailable>unknown</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # StopPlace with audioinformation + audiblesignals
		{"StopPlace": "RailStation", "AudioInformation": "Bad", "AudibleSignals": "No"},
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Signalétique auditive non accessible malentendants</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<EscalatorFreeAccess>unknown</EscalatorFreeAccess>
				<LiftFreeAccess>unknown</LiftFreeAccess>
				<AudibleSignalsAvailable>partial</AudibleSignalsAvailable>
				<VisualSignsAvailable>unknown</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with no access tags
		{"Entrance": "PointOfInterest"},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with kerb height
		{"Entrance": "PointOfInterest", "KerbHeight": "0.03"},
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Présence d'une marche de moins de 4 cm</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>partial</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with kerb height = 0
		{"Entrance": "PointOfInterest", "KerbHeight": "0"},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>true</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with kerb height = 0.1
		{"Entrance": "PointOfInterest", "KerbHeight": "0.1"},
		"""<MobilityImpairedAccess>false</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>false</WheelchairAccess>
				<StepFreeAccess>false</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with kerb height
		{"Entrance": "PointOfInterest", "KerbCut:Width": "5"},
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Présence d'une abaissé de trottoir de taille inconnue</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>partial</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with width 0.7
		{"Entrance": "PointOfInterest", "Width": "0.7"},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>false</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with NecessaryForceToOpen 52
		{"Entrance": "PointOfInterest", "NecessaryForceToOpen": "52"},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>false</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with KerbHeight 0.1
		{"Entrance": "PointOfInterest", "KerbHeight": "0.1"},
		"""<MobilityImpairedAccess>false</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>false</WheelchairAccess>
				<StepFreeAccess>false</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with good width/kerbheight/necessaryforce
		{"Entrance": "PointOfInterest", "Width": "1", "KerbHeight": "0.01", "NecessaryForceToOpen": "20"},
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>true</WheelchairAccess>
				<StepFreeAccess>true</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with good width/kerbheight/door
		{"Entrance": "PointOfInterest", "Width": "1", "KerbHeight": "0.01", "Door": "None"},
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>true</WheelchairAccess>
				<StepFreeAccess>true</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Entrance with good width/kerbheight/entrancepassage
		{"Entrance": "PointOfInterest", "Width": "1", "KerbHeight": "0.01", "EntrancePassage": "Opening"},
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>true</WheelchairAccess>
				<StepFreeAccess>true</StepFreeAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Parking bay without details
		{"ParkingBay": "Disabled"},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<VisualSignsAvailable>unknown</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Parking bay without wheelchair access but with technical detail
		{
			"ParkingBay": "Disabled",
			"Tilt": "1",
			"Slope": "1",
			"Width": "4",
			"FlooringMaterial": "Asphalt",
			"Flooring": "Good",
			"StepFreeAccess": "Yes"
		},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>true</WheelchairAccess>
				<StepFreeAccess>true</StepFreeAccess>
				<VisualSignsAvailable>unknown</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Parking bay without wheelchair access but with partial technical detail
		{
			"ParkingBay": "Disabled",
			"Tilt": "1",
			"FlooringMaterial": "Asphalt",
			"Flooring": "Good",
			"StepFreeAccess": "Yes"
		},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>true</StepFreeAccess>
				<VisualSignsAvailable>unknown</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Parking bay without wheelchair access but with technical detail (not accessible)
		{
			"ParkingBay": "Disabled",
			"Tilt": "3",
			"Slope": "3",
			"Width": "4",
			"FlooringMaterial": "Asphalt",
			"Flooring": "Good",
			"StepFreeAccess": "Yes"
		},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>false</WheelchairAccess>
				<StepFreeAccess>true</StepFreeAccess>
				<VisualSignsAvailable>unknown</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Parking bay unmarked
		{"ParkingBay": "Disabled", "ParkingVisibility": "Unmarked"},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<VisualSignsAvailable>false</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Parking bay demarcated
		{"ParkingBay": "Disabled", "ParkingVisibility": "Demarcated"},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<VisualSignsAvailable>true</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Parking bay signage
		{"ParkingBay": "Disabled", "ParkingVisibility": "SignageOnly"},
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Signalétique présente mais pas de marquage au sol</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<VisualSignsAvailable>partial</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Parking bay marking
		{"ParkingBay": "Disabled", "ParkingVisibility": "MarkingOnly"},
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Marquage au sol présent mais pas de signalétique</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<VisualSignsAvailable>partial</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Parking bay docks
		{"ParkingBay": "Disabled", "ParkingVisibility": "Docks"},
		"""<validityConditions>
				<ValidityCondition version="any" id="Yukaimaps:ValidityCondition:0:">
					<Description>Démarquation physique uniquement</Description>
				</ValidityCondition>
			</validityConditions>
			<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
				<StepFreeAccess>unknown</StepFreeAccess>
				<VisualSignsAvailable>partial</VisualSignsAvailable>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Toilets with no info
		{"Amenity": "Toilets"},
		"""<MobilityImpairedAccess>unknown</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>unknown</WheelchairAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
	( # Toilets wheelchair
		{"Amenity": "Toilets", "WheelchairAccess": "Yes"},
		"""<MobilityImpairedAccess>true</MobilityImpairedAccess>
			<limitations>
			<AccessibilityLimitation>
				<WheelchairAccess>true</WheelchairAccess>
			</AccessibilityLimitation>
			</limitations>""",
		[]
	),
))
def test_wdmTagsToNetexAccess(wdm, netex, nodes):
	res = properties.wdmTagsToNetexAccess(wdm, 0, nodes)

	if netex is None:
		assert res is None
	else:
		assert type(res) == type(etree.Element("AccessibilityAssessment"))

		netexstr = f"""<AccessibilityAssessment id="Yukaimaps:AccessibilityAssessment:0:" version="any">{netex}</AccessibilityAssessment>"""
		resstr = etree.tostring(res, encoding='unicode')
		checker = LXMLOutputChecker()

		if not checker.check_output(netexstr, resstr, 0):
			message = checker.output_difference(Example("", netexstr), resstr, 0)
			raise AssertionError(message)


@pytest.mark.parametrize(("pct", "deg"), (
	(1, 1),
	(45, 25),
	(90, 42),
))
def test_anglePctToDegrees(pct, deg):
	assert properties.anglePctToDegrees(pct) == deg


@pytest.mark.parametrize(("t1", "t2", "mk", "out"), (
	({"a": "1"}, {"b": "2"}, None, {"a": "1", "b": "2"}),
	({}, {"a": "1"}, None, {"a": "1"}),
	({"a": "1"}, {"a": "2"}, None, {"a": {"1", "2"}}),
	({"a": "1"}, {"a": "1"}, None, {"a": "1"}),
	({"a": {"1", "2"}}, {"a": "3"}, None, {"a": {"1", "2", "3"}}),
	({"a": "1"}, {}, ["a"], {"a": {"1", None}}),
	({}, {"a": "1"}, ["a"], {"a": {None, "1"}}),
	({}, {}, ["a"], {}),
))
def test_combineTags(t1, t2, mk, out):
	assert properties.combineTags(t1, t2, mk) == out


@pytest.mark.parametrize(("val", "res"), (
	("Yes", "true"),
	("No", "false"),
	("Limited", "partial"),
	("Bla", "false"),
	({"Yes", "No", "Maybe", "I don't know"}, "partial"),
	(None, "unknown"),
	({None}, "unknown"),
	("Bad", "partial"),
	({"Yes", "No", None}, "unknown"),
))
def test_wdm3StatesToNetex(val, res):
	assert properties.wdm3StatesToNetex(val) == res


@pytest.mark.parametrize(("tags", "key", "res"), (
	({"a": "1"}, "a", 1),
	({}, "a", None),
	({"a": "bla"}, "a", None),
	({"a": "1.0"}, "a", 1),
	({"a": "1.12"}, "a", None),
))
def test_getIntegerTag(tags, key, res):
	assert properties.getIntegerTag(tags, key) == res


@pytest.mark.parametrize(("tags", "key", "res"), (
	({"a": "1"}, "a", 1.0),
	({}, "a", None),
	({"a": "bla"}, "a", None),
	({"a": "1.0"}, "a", 1.0),
	({"a": "1.12"}, "a", 1.12),
))
def test_getFloatTag(tags, key, res):
	assert properties.getFloatTag(tags, key) == res
